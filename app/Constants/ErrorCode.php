<?php

declare(strict_types=1);


namespace App\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 * 自定义业务代码规范如下：
 * 错误异常，5001……
 * 业务相关，3001……
 * 验证相关，4001……
 */
class ErrorCode extends AbstractConstants
{
    /**
     * @Message("ok")
     */
    const SUCCESS = 0;
    /**
     * @Message("入参验证出错")
     */
    const ERR_VALIDATION = 4000;
    /**
     * @Message("Internal Server Error!")
     */
    const ERR_SERVER = 5000;
    /**
     * @Message("业务逻辑错误")
     */
    const ERR_BUESSUS = 3000;

    /**
     * @Message("签名错误")
     */
    const ERR_GRPC = 403;

    /**
     * @Message("绑定抖音账号")
     */
    const AUTH_ERROR = 1;
}
