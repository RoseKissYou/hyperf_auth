<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Dao;

use App\Model\Module;

class ModuleDao
{
    //添加数据
    public function add(array $data)
    {
        return Module::query()->insertGetId($data);
    }

    //添加数据多条
    public function insert(array $data)
    {
        return Module::query()->insert($data);
    }

    //删除
    public function delete($where, $whereIn = [])
    {
        return Module::query()->where($where)->when($whereIn, function ($query, $whereIn) {
            return $query->whereIn('id', $whereIn);
        })->delete();
    }

    //更新
    public function update($where, $data)
    {
        return Module::query()->where($where)->update($data);
    }


    //通过条件搜索单一条
    public function searchFirst($where, $columns = ['*'])
    {
        return Module::query()->where($where)->first($columns);
    }

    //找出主键id
    public function searchAllId($where = [])
    {
        return Module::query()->where($where)->pluck('id')->toArray();
    }

    public function searchAll($where = [], $whereIn = [], $columns = ['*'])
    {
        return Module::query()->where($where)->when($whereIn, function ($query, $whereIn) {
            return $query->whereIn('id', $whereIn);
        })->orderBy('id', 'desc')->get($columns)->toArray();
    }
   
}
