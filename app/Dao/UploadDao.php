<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Dao;

use App\Model\Upload;

class UploadDao
{
    //添加数据
    public function add(array $data)
    {
        $data['add_time'] = time();
        return Upload::query()->insertGetId($data);
    }


    //通过条件搜索单一条
    public function searchFirst($where, $columns = ['*'])
    {
        return Upload::query()->where($where)->first($columns);
    }

    public function searchAll($where, $whereIn = [], $columns = ['*'])
    {
        return Upload::query()->where($where)->when($whereIn, function ($query, $whereIn) {
            return $query->whereIn('id', $whereIn);
        })->get($columns)->toArray();
    }
}
