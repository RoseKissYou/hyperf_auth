<?php

declare (strict_types = 1);

namespace App\Model;

use Hyperf\Database\Model\Builder;
use Hyperf\DbConnection\Model\Model;

class AuthRole extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'auth_role';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'company_id', 'add_time', 'update_time', 'is_del'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'add_time' => 'datetime:Y-m-d H:i:s',
        'update_time' => 'datetime:Y-m-d H:i:s',
    ];
    protected $hidden = ['is_del'];

    protected function boot(): void
    {
        $this->bootTraits();
        //model 软删除过滤 避免每次查询都添加is_del条件 去除条件添加withoutGlobalScope
        $this->addGlobalScope('is_del', function (Builder $builder) {
            $builder->where($this->table . '.is_del', 1);
        });
    }
    const CREATED_AT = 'add_time';
    const UPDATED_AT = 'update_time';
    protected $dateFormat = 'U';
}
