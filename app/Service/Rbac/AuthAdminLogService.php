<?php

declare(strict_types=1);

namespace App\Service\Rbac;

use App\Dao\{AuthAdminLogDao};
use Hyperf\Di\Annotation\Inject;

class AuthAdminLogService
{
    /**
     * @Inject
     * @var AuthAdminLogDao
     */
    protected $authAdminLogDao;

    public function listLog(int $page,int $pageSize)
    {
        return $this->authAdminLogDao->getListPage([],[],$page,$pageSize);
    }

    public function addLog(string $operate,int $adminId)
    {
        return $this->authAdminLogDao->addLog($operate,$adminId);
    }

}