<?php

declare (strict_types = 1);

namespace App\Controller\Organization;

use App\Controller\AbstractController;
use App\Service\Organziation\RoleService;
use App\Validate\RoleValidation;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Context;

class RoleController extends AbstractController
{

    /**
     * @Inject
     * @var RoleService
     */
    protected $service;

    public function index()
    {
        $params = $this->request->all();
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $result = $this->service->list($params);
        return $this->success($result['msg'], $result['data'], $result['totalItem']);
    }

    public function roleComponent()
    {
        $params = make(RoleValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $result = $this->service->roleComponent($params);
        return $this->success($result['msg'], $result['data'], $result['totalItem']);
    }

    public function add()
    {
        $params = make(RoleValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'] ?? 1;
        $result = $this->service->add($params);
        return $this->success($result['msg'], $result['data'], $result['totalItem']);
    }

    public function edit()
    {
        $params = make(RoleValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $result = $this->service->edit($params);
        return $this->success($result['msg']);
    }

    public function del()
    {
        $params = make(RoleValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        $result = $this->service->del($params);
        return $this->success($result['msg']);
    }

    public function set()
    {
        $params = make(RoleValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $result = $this->service->set($params);
        return $this->success($result['msg']);
    }
}
