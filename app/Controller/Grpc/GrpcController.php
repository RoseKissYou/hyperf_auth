<?php

declare(strict_types = 1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Controller\Grpc;

use App\Controller\AbstractController;
use Hyperf\Utils\Codec\Json;
use Pb\Params;
use Pb\Reply;
use App\Utils\ServerUtil;

class GrpcController extends AbstractController
{
    public function curdClient(Params $params)
    {
        $className = $params->getController();
        $method = $params->getMethod();
        $client = $this->container->get($className);
        $map = $params->getRequest() ? Json::decode(ServerUtil::opensslDecrypt($params->getRequest())) : '';
        //$map = $params->getRequest() ? Json::decode($params->getRequest()) : '';
        $res = $client->{$method}($map);
        $message = new Reply();
        $message->setErrCode($res['errCode']);
        $message->setMsg($res['msg']);
        
        $res['data'] = $res['data'] ? ServerUtil::opensslEncrypt(Json::encode($res['data'])) : '';
        //$message->setData($res['data'] ? Json::encode($res['data']) : '');
        $message->setData($res['data']);
        $message->setTotal($res['totalItem']);
        return $message;
    }
}
