<?php

declare (strict_types = 1);

namespace App\Controller\Organization;

use App\Controller\AbstractController;
use App\Service\Organziation\CompanyService;
use App\Validate\CompanyValidation;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Context;

class CompanyController extends AbstractController
{

    /**
     * @Inject
     * @var CompanyService
     */
    protected $service;

    public function index()
    {
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $params['employee_id'] = Context::get('jwt_token')['uid'];
        $result = $this->service->info($params);
        return $this->success($result['msg'], $result['data']);
    }

    public function editComany()
    {
        $params = make(CompanyValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $result = $this->service->editComany($params);
        return $this->success($result['msg'], $result['data']);
    }
}
