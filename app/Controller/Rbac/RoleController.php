<?php

declare (strict_types = 1);

namespace App\Controller\Rbac;

use App\Constants\ErrorCode;
use App\Controller\AbstractController;
use App\Exception\BusinessException;
use Hyperf\Di\Annotation\Inject;
use App\Validate\RoleValidation;
// 使用rbac
use Wuxian\Rbac\Rbac;
use Hyperf\DbConnection\Db;

class RoleController extends AbstractController
{
    /**
     * @Inject
     * @var Rbac
     */
    protected $rbacObj;

    public function __construct()
    {
        $this->rbacObj = new Rbac(config('rbac.defualt'));
    }

    public function roleList()
    {
        $pageSize = (int)$this->request->input('pageSize',15);
        $result = $this->rbacObj->roleList($pageSize);
        // 角色列表信息查询对应的权限数据
        foreach ($result['data'] as &$datum) {
            $permissins = $this->rbacObj->getPermissionIdsByRoleId($datum['id']);
            $datum['permission_id'] = $permissins;
        }
        return $this->success('ok',$result['data'],$result['total']);
    }

    public function roleAdd()
    {
        $data = $this->request->inputs(['name','permission_id']);
        make(RoleValidation::class)->check($data, __FUNCTION__)->response();
        // 添加角色
        try {
            Db::beginTransaction();
            $role_id = $this->rbacObj->addRole(['name'=>$data['name']]);
            // 添加角色绑定权限
            $result = $this->rbacObj->addPermissionIdsRoleId($role_id,$data['permission_id']);
            Db::commit();
        }catch (\Throwable $t) {
            Db::rollBack();
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '角色名称重复,或者信息错误@#新增角色失败');
        }
        //$this->roleService->addRole($data)
        return $this->success('ok',$result);
    }

    public function roleEdit()
    {
        $id = (int)$this->request->input('id');
        if ($this->request->isMethod('POST')){
            $data = $this->request->inputs(['id','name','permission_id']);
            make(RoleValidation::class)->check($data, __FUNCTION__)->response();
            unset($data['id']);
            try {
                Db::beginTransaction();
                $result = $this->rbacObj->editRole($id,['name'=>$data['name']]);
                // 更新权限
                $this->rbacObj->addPermissionIdsRoleId($id,$data['permission_id']);
                Db::commit();
            }catch (\Throwable $t) {
                Db::rollBack();
                throw new BusinessException(ErrorCode::ERR_BUESSUS, '角色名称重复,或者信息错误@#编辑角色失败');
            }
            if (!$result) {
                throw new BusinessException(ErrorCode::ERR_BUESSUS, '角色不存在,或者信息错误@#编辑角色失败');
            }
            return $this->success('修改成功',$result);
        }else{
            make(RoleValidation::class)->check($this->request->all(), 'roleShow')->response();
            $result = $this->rbacObj->getRoleInfo($id);
            //$this->roleService->editRoleGet((int)$this->request->input('id'))
            return $this->success('获取资料',$result);
        }
    }

    public function roleDel()
    {
        $data = $this->request->inputs(['id']);
        make(RoleValidation::class)->check($data, 'roleShow')->response();
        // 判断id是否包含 ,
        $data_ids = [$data['id']];
        try {
            Db::beginTransaction();
            $result = $this->rbacObj->delRole($data_ids);
            $this->rbacObj->delPermissionIdsRoleId('role_id',$data_ids);
            Db::commit();
        }catch (\Throwable $t) {
            Db::rollBack();
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '角色删除失败,或者信息错误@#删除角色失败');
        }
        if (!$result) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '角色不存在,或者信息错误@#编辑角色失败');
        }
        //$this->roleService->delRole((int)$this->request->input('id'))
        return $this->success('删除成功',$result);
    }

    public function roleAll()
    {
        $result = $this->rbacObj->roleAll();
        //$this->roleService->allRole(config('web.super')[0])
        return $this->success('ok',$result);
    }
}