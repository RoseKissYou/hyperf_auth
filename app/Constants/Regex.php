<?php

namespace App\Constants;

/**
 * 正则
 * Class Regex
 * @package App\Constants
 */
class Regex
{
    /**
     * 简单版，手机号码
     */
    const PHONE = '/^1\d{10}$/';
    const PHONE_MESSAGE = '手机号码格式不正确';

    const QQ = '/^\d{5,30}$/';
    const QQ_MESSAGE = 'QQ号不正确';

    const MONEY = '/^[-]?(0|[1-9]\d{0,})([.][0-9]{1,2})?$/';
    const MONEY_MESSAGE = '金额不正确';
    /**
     * 只允许中文
     */
    const CHS = '/^[\x{4e00}-\x{9fa5}]+$/u';
    const CHS_MESSAGE = '只允许中文';
    /**
     * 身份校验
     */
    const IDENTIFICATIONI = '/^[1-9]\d{5}(19|20)\d{2}[01]\d[0123]\d\d{3}[xX\d]$/';
    const IDENTIFICATIONI_MESSAGE = '身份证格式错误';

}
