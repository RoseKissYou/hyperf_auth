<?php

declare (strict_types = 1);

namespace App\Controller\Organization;

use App\Controller\AbstractController;
use App\Service\Organziation\EmployeeService;
use App\Validate\EmployeeValidation;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Context;

class EmployeeController extends AbstractController
{

    /**
     * @Inject
     * @var EmployeeService
     */
    protected $service;

    public function index()
    {
        $params = $this->request->all();
        $params['page'] = $this->request->input('page', 1);
        $params['pageSize'] = $this->request->input('pageSize', 15);
        $params['company_id'] = Context::get('jwt_token')['company_id'] ?? 1;
        $result = $this->service->list($params);
        return $this->success($result['msg'], $result['data'], $result['totalItem']);
    }

    function list() {
        $params = $this->request->all();
        $params['company_id'] = Context::get('jwt_token')['company_id'] ?? 1;
        $params['list_type'] = $params['list_type'] ?? 1;
        $params['entry_status'] = $params['entry_status'] ?? 1;
        $result = $this->service->list($params);
        return $this->success($result['msg'], $result['data']);
    }

    public function addEmployee()
    {
        $params = make(EmployeeValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'] ?? 1;
        //参与项目的字符串id
        // $params['join_project_id'] = $this->request->input('join_project_id', "");
        // //查看项目字符串id
        // $params['show_project_id'] = $this->request->input('show_project_id', "");
        $params['admin_role_id'] = $this->request->input('admin_role_id', 0);
        $result = $this->service->addEmployee($params);
        return $this->success($result['msg'], $result['data']);
    }

    public function editEmployee()
    {
        $params = make(EmployeeValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'] ?? 1;
        //参与项目的字符串id
        // $params['join_project_id'] = $this->request->input('join_project_id', "");
        // //查看项目字符串id
        // $params['show_project_id'] = $this->request->input('show_project_id', "");
        $params['admin_role_id'] = $this->request->input('admin_role_id', 0);
        $result = $this->service->editEmployee($params);
        return $this->success($result['msg'], $result['data']);
    }

    public function editPassword()
    {
        $params = make(EmployeeValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        $params['id'] = Context::get('jwt_token')['uid'];
        $result = $this->service->editPassword($params);
        return $this->success($result['msg']);
    }

    public function login()
    {
        $params = make(EmployeeValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        $result = $this->service->login($params);
        return $this->success($result['msg'], $result['data']);
    }

    public function editEmployeeStauts()
    {

        make(EmployeeValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        $result = $this->service->editEmployeeStauts($this->request->all());
        return $this->success($result['msg'], $result['data']);
    }

    public function refreshToken()
    {
        $result = $this->service->refreshToken($this->request->all());
        return $this->success($result['msg'], $result['data']);
    }
}
