<?php
declare(strict_types=1);

namespace App\Utils;

/**
 * @Description 处理一些常用的数据信息
 * @author abell
 * @since 2020-08-17
 */
class DataUtils
{
    /**
     * 筛选数组字段形成新的数组
     * @param $select array 需要筛选的字段数组
     * @param $data array 目标数组
     * @return array
     */
    public static function selectKeyFromArray($select, $data)
    {
        $new_data = array();
        foreach ($select as $sel_key) {
            $new_data[$sel_key] = array_key_exists($sel_key, $data) ?  $data[$sel_key] : '';
        }
        return $new_data;
    }

    /**
     * 金额校验
     */
    public static function checkMoney($moneys, $minNum=0, $isInt=false)
    {
        $error = null;
        foreach ($moneys as $money) {
            $money = (integer)$money;
            if ($money <=0) {
                $error = '金额不能为小于等于0';
            }
            if ($isInt && (!is_integer($money))) {
                $error = '金额必须为整数';
            }
            if ($money < $minNum) {
                $error = '不能小于最低金额:'.(string)$minNum;
            }
        }
        return $error;
    }
}
