<?php

declare (strict_types = 1);

namespace App\Dao;


class AuthCompanyDao
{
    use DaoTrait;
    public function __construct()
    {
        $this->driver = __CLASS__;
    }

    public function changeDriver(\Hyperf\Database\Model\Model $model)
    {
        $this->driver = $model;
        return $this;
    }

    public function resetDriver()
    {
        $this->driver = __CLASS__;
        return $this;
    }

}
