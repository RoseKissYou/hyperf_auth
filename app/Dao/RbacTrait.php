<?php

declare (strict_types = 1);

namespace App\Dao;

use App\Model\AuthPermission as Permission;
use App\Model\AuthRole as Role;
use App\Model\AuthEmployee as RoleAdmin; // 待定和账号绑定的角色管理
use App\Model\AuthAdminRolePermission as RolePermission;

trait RbacTrait
{
    /**
     * 获取某个用户的权限列表
     * @param int $manage_id 用户id
     * @return array
     */
    public static function permissionAll(int $manage_id) : array
    {
        //从配置文件中取出超级管理员
        $super = config('web.super');
        if (in_array($manage_id, $super)) { //超级管理员的权限
            $permission_arr = static::superPermission();
        } else {
            $permission_arr = static::permissionListByUserid($manage_id);
        }
        return $permission_arr;
    }

    /**
     * 通过某个用户获取相应的权限
     * @param int $manage_id 用户id
     * @return array
     */
    public static function permissionListByUserid(int $manage_id) : array
    {
        $role_arr = static::roleIdByUserid($manage_id);
        if (empty($role_arr)) {
            return [];
        }
        $permission_ids = RolePermission::query()->whereIn('role_id', $role_arr)->pluck('permission_id')->toArray();
        if (! empty($permission_ids)) {
            $data = Permission::query()->whereIn('id', $permission_ids)->orderBy('sort_order', 'desc')->get()->toArray();
            return $data;
        }
        return [];
    }

    //通过某角色获取相应的权限
    public static function permissionListByRole(int $role_id) : array
    {
        $permission_arr = static::permissionIdByRoleid($role_id);
        $data = [];
        //通过权限id找到相关的权限
        if (! empty($permission_arr)) {
            $data = Permission::query()->whereIn('id', $permission_arr)->get()->toArray();
        }
        return $data;
    }

    //通过某角色获取相应用户id
    public static function manageIdByRole (int $role_id) : array
    {
        return RoleAdmin::query()->where('role_id', $role_id)->get()->toArray();
    }

    //获取权限列表
    public static function getPermissionList() : array
    {
        return Permission::query()->orderBy('sort_order', 'desc')->get()->toArray();
    }

    //超级管理员权限
    public static function superPermission()
    {
        return static::getPermissionList();
    }

    //所有角色列表
    public static function roleAll() : array
    {
        return Role::query()->get()->toArray();
    }

    /**
     * 获取某个用户的角色id
     * @param int $manage_id 用户id
     * @return array
     */
    public static function roleIdByUserid(int $manage_id) : array
    {
        $role_arr = RoleAdmin::query()->where('id', $manage_id)->get()->toArray();
        if (empty($role_arr)) {
            return [];
        }
        return array_column($role_arr, 'admin_role_id');
    }

    /**
     * 获取某个角色的权限id
     * @param int $role_id 角色id
     * @return array
     */
    public static function permissionIdByRoleid(int $role_id) : array
    {
        $permission_arr = RolePermission::query()->where('role_id', $role_id)->get()->toArray();
        if (empty($permission_arr)) {
            return [];
        }
        return array_column($permission_arr, 'permission_id');
    }

    /**
     * 通过某个用户获取相应的角色
     * @param int $manage_id 用户id
     * @return array
     */
    public static function roleListByUserid(int $manage_id) : array
    {
        //通过角色找到权限id
        $role_arr = static::roleIdByUserid($manage_id);
        $data = [];
        //通过权限id找到相关的权限
        if (! empty($role_arr)) {
            $data = Role::query()->whereIn('id', $role_arr)->get()->toArray();
        } else {
            if (in_array($manage_id, config('web.super'))) {
                $data[0]['name'] = '超级管理员';
            }
        }
        return $data;
    }

    //递归实现侧边栏
    public static function diG(array $arr, $parent_id = 0, $level = 1) : array
    {
        static $tree;
        foreach ($arr as $k => $v) {
            if ($v['parent_id'] == $parent_id) {
                $v['level'] = $level;
                $tree[] = $v;
                static::diG($arr, $v['id'], $level + 1);
            }
        }
        return $tree;
    }

    //递归实现侧边栏(层级从属关系)
    public static function getTree(array $data, $pid = 0, $level = 1) : array
    {
        $list = [];
        foreach ($data as $k => $v) {
            if ($v['parent_id'] == $pid) {
                $v['level'] = $level;
                $v['son'] = static::getTree($data, $v['id'], $level + 1);
                $list[] = $v;
            }
        }
        return $list;
    }

    /**
     * 判断当前用户是否拥有当前控制器函数访问权限.
     * @param $arr
     * @return array|bool
     */
    public static function permissionIsOk(array $arr)
    {
        //通过用户找到权限
        $permission_role_arr = static::permissionAll($arr['admin_id']);
        if (empty($permission_role_arr)) {
            return false;
        }
        $permission_ids = array_column($permission_role_arr, 'id');
        //通过当前操作找到权限id
        $permission_id = Permission::query()->where('url', $arr['url'])->value('id');
        if (empty($permission_id)) {
            return false;
        }
        return in_array($permission_id, $permission_ids);
    }
}