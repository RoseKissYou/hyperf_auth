<?php

declare (strict_types = 1);

namespace App\Validate;

use App\Constants\Regex;

class EmployeeValidation extends BaseValidation
{
    // 调用案例 check支持自定义规则

    // make(TaskValidation::class)->check($this->request->all(),__FUNCTION__)

    //返回响应值的  ->response()

    //返回boole   ->get()

    //返回报错信息  ->getError()

    //验证规则
    protected $rule = [
        'account' => 'required|regex:/^\w{3,32}$/',
        'password' => 'required|regex:/^\w{6,32}$/',
        'duty_id' => 'required|integer',
        'role_id' => 'required|integer',
        'organization_id' => 'required|integer',
        'employee_id' => 'required|integer',
        'job_number' => 'required',
        'name' => 'required',
        'phone' => 'required|regex:' . Regex::PHONE,
        'identification' => ['required', 'regex:' . Regex::IDENTIFICATIONI],
        'admin_status' => 'required|in:1,2',
        'status' => 'required|in:1,2',
        // 'partake_project' => 'required|in:1,2',
        // 'check_project' => 'required|in:1,2',
        'entry_time' => 'required',
        'entry_status' => 'required|in:1,2',
        'sex' => 'required|in:1,2',
        'list_type' => 'required|in:1,2,3',
        'password_confirmation' => 'required|regex:/^\w{6,32}$/',
        'password_confirmation_re' => 'required|same:password_confirmation',
    ];

    //自定义验证信息
    protected $message = [
        'account.required' => '账号不能为空',
        'account.regex' => '账号长度3,32位之间的数字与字母组合',
        'password.required' => '密码不能为空',
        'password.regex' => '密码长度6,32位之间',
        'name.required' => '名称不能为空',
        'job_number.required' => '工号不能为空',
        'phone.required' => '电话不能为空',
        'phone.regex' => Regex::PHONE_MESSAGE,
        'identification.required' => '身份证不能为空',
        'identification.regex' => Regex::IDENTIFICATIONI_MESSAGE,
        'duty_id.required' => '岗位不能为空',
        'role_id.required' => '角色不能为空',
        'organization_id.required' => '部门不能为空',
        'employee_id.required' => '员工id不能为空',
        'admin_status.required' => '后端登陆权限不能为空',
        'admin_status.in' => '后端登陆权限范围错误',
        'status.required' => '登陆状态不能为空',
        'status.in' => '登陆状态范围错误',
        // 'partake_project.required' => '参与项目配置不能为空',
        // 'partake_project.in' => '参与项目配置范围错误',
        // 'check_project.required' => '查看项目配置不能为空',
        // 'check_project.in' => '查看项目配置范围错误',
        'entry_status.required' => '在职状态不能为空',
        'entry_time.required' => '入职时间不能为空',
        'sex.required' => '性别不能为空',
        'sex.in' => '性别范围错误',
        'list_type.required' => '人员范围设置不能为空',
        'list_type.in' => '人员范围错误',
        'password_confirmation.required' => "新密码 必须",
        'password_confirmation.regex' => '新密码长度6,32位之间',
        'password_confirmation_re.required' => "确认密码 必须",
        'password_confirmation_re.same' => "确认密码与新密码 必须相同",
    ];

    //自定义场景
    protected $scene = [
        'addEmployee' => [
            'account', 'job_number', 'password', 'name', 'phone', 'identification', 'duty_id', 'role_id', 'organization_id',
            'admin_status', 'status',
            // 'partake_project', 'check_project',
            'entry_time', 'entry_status', 'sex',
        ],
        'editEmployee' => [
            'employee_id',
            'account', 'password', 'job_number', 'name', 'phone', 'identification', 'duty_id', 'role_id', 'organization_id',
            'admin_status', 'status',
            // 'partake_project', 'check_project',
            'entry_time', 'entry_status', 'sex',
        ],
        'delEmployee' => ['employee_id', 'operation_confirm'],
        'editPassword' => ['password', 'password_confirmation', 'password_confirmation_re'],
        'login' => ['account', 'password'],
        'editEmployeeStauts' => ['employee_id'],
    ];
}
