<?php

declare (strict_types=1);

namespace App\Dao;

use App\Model\AuthPermission;

class AuthPermissionDao
{
    use RbacTrait;
    use DaoTrait;
    public function __construct()
    {
        $this->driver = __CLASS__;
    }

    // 通过用户id查找权限
    public function searchPerByWhereId(int $user_id)
    {
        $data = static::permissionAll($user_id);
        return static::getTree($data);
    }

    // 通过ids查找权限
    public function serchaByIds(array $where)
    {
        return AuthPermission::query()->whereIn('parent_id',$where)->pluck('id')->toArray();
    }

    // 批量删除权限
    public function delPermissionByIds(array $where)
    {
        return AuthPermission::query()->whereIn('id',$where)->delete();
    }
}