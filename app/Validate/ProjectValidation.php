<?php

declare (strict_types = 1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

namespace App\Validate;

class ProjectValidation extends BaseValidation
{
    // 调用案例 check支持自定义规则

    // make(TaskValidation::class)->check($this->request->all(),__FUNCTION__)

    //返回响应值的  ->response()

    //返回boole   ->get()

    //返回报错信息  ->getError()

    //验证规则
    protected $rule = [
        'id' => 'required|integer',
        'name' => 'required',
        'organization_ids' => 'required',
        'employee_id' => 'required|integer',
        'employee_list' => 'required',
        'type' => 'required|in:1,2,3',
        'period_type' => 'required|in:1,2',
    ];

    //自定义验证信息
    protected $message = [
        'organization_ids.required' => '部门id不能为空',
        'employee_id.required' => '员工id不能为空',
        'employee_ids.required' => '员工id不能为空',
        'employee_id.integer' => '员工id错误',
        'id.required' => 'id不能为空',
        'id.integer' => 'id错误',
        'name.required' => '名称不能为空',
        'type.required' => '编辑状态不能为空',
        'type.in' => '编辑状态错误',
        'period_type.required' => '周期类型不能为空',
        'period_type.in' => '周期类型错误',

    ];

    //自定义场景
    protected $scene = [
        'add' => ['organization_ids', 'name', 'period_type'],
        'edit' => ['id', 'organization_ids', 'name', 'period_type'],
        'info' => ['id'],
        'organization' => ['id'],
        'editType' => ['id', 'type'],
        'employeeList' => ['id'],
        'employeeAdd' => ['id', 'employee_id'],
        'employeeDel' => ['id'],
        'employeeEdit' => ['id', 'organization_ids', 'employee_list'],
    ];
}
