<?php

declare (strict_types = 1);

namespace App\Dao;

use App\Model\AuthRole;

class AuthRoleDao
{
    use DaoTrait;

    public function __construct()
    {
        $this->driver = __CLASS__;
    }

    public function getIndexList($params)
    {

        $sql = AuthRole::query()->where('company_id', $params['company_id']);
        if (isset($params['page']) && $params['page'] != '' && isset($params['pageSize']) && $params['pageSize'] != '') {
            $count = $sql->count();
            $data = $sql->paginate(intval($params['pageSize']), ['*'], 'page', (int) $params['page'])->toArray()['data'];
            return compact('data', 'count');
        }
        return $sql->get();

    }
}
