<?php

declare (strict_types = 1);

namespace App\Controller\Auth;

use App\Controller\AbstractController;
use App\Service\Auth\ProjectService;
use App\Validate\ProjectValidation;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Context;

class ProjectController extends AbstractController
{
    /**
     * @Inject
     * @var ProjectService
     */
    protected $service;

    /**
     * 添加项目
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function add()
    {
        $params = $this->request->all();
        make(ProjectValidation::class)->check($params, 'add')->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $result = $this->service->add($params);
        return $this->success($result['msg'], $result['data']);

    }

    /**
     * 编辑项目
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function edit()
    {
        $params = $this->request->all();
        make(ProjectValidation::class)->check($params, 'edit')->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $result = $this->service->edit($params);
        return $this->success($result['msg'], $result['data']);

    }


    /**
     * 项目列表
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function list()
    {

        $params = $this->request->all();
        $params['page'] = (int)$this->request->input('page', 1);
        $params['pageSize'] = (int)$this->request->input('pageSize', 15);
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $result = $this->service->list($params);
        return $this->success($result['msg'], $result['data'], $result['totalItem']);

    }

    /**
     * 项目详情
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function info()
    {
        $params = $this->request->all();
        make(ProjectValidation::class)->check($params, 'info')->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $result = $this->service->info($params);
        return $this->success($result['msg'], $result['data']);

    }

    /**
     * 编辑项目状态
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function editType()
    {
        $params = $this->request->all();
        make(ProjectValidation::class)->check($params, 'editType')->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $result = $this->service->editType($params);
        return $this->success($result['msg'], $result['data']);

    }

    /**
     * 项目员工列表
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function employeeList()
    {
        $params = $this->request->all();
        $params['page'] = (int)$this->request->input('page', 1);
        $params['pageSize'] = (int)$this->request->input('pageSize', 15);
        make(ProjectValidation::class)->check($params, 'employeeList')->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $result = $this->service->employeeList($params);
        return $this->success($result['msg'], $result['data'], $result['totalItem']);

    }


    /**
     * 获取项目下级组织架构
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function organization()
    {
        $params = $this->request->all();
        make(ProjectValidation::class)->check($params, 'organization')->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $result = $this->service->organization($params);
        return $this->success($result['msg'], $result['data']);

    }

    /**
     * 编辑项目员工
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function employeeEdit()
    {
        $params = $this->request->all();
        make(ProjectValidation::class)->check($params, 'employeeEdit')->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $result = $this->service->employeeEdit($params);
        return $this->success($result['msg'], $result['data']);

    }

    /**
     * 获取项目全部员工ID
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function employeeAll()
    {
        $params = $this->request->all();
        make(ProjectValidation::class)->check($params, 'info')->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $result = $this->service->employeeAll($params);
        return $this->success($result['msg'], $result['data'], $result['totalItem']);

    }

    /**
     * 获取项目全部员工ID
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function employeePermission()
    {
        $params['id'] = (int)$this->request->input('id');
        $result = $this->service->employeePermission($params);
        return $this->success($result['msg'], $result['data'], $result['totalItem']);

    }

    /**
     * 编辑项目员工（部门负责人）
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function employeeEditPrincipal()
    {
        $params = $this->request->all();
        $result = $this->service->employeeEditPrincipal($params);
        return $this->success($result['msg'], $result['data'], $result['totalItem']);

    }

}
