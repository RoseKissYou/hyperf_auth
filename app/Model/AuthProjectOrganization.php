<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id 
 * @property int $company_id 公司id
 * @property int $project_id 项目id
 * @property int $organization_id 部门id
 * @property datetime:Y-m-d H:i:s $add_time 
 */
class AuthProjectOrganization extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'auth_project_organization';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'company_id', 'project_id', 'organization_id', 'add_time'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'int', 'project_id' => 'integer', 'organization_id' => 'integer', 'add_time' => 'datetime:Y-m-d H:i:s', 'company_id' => 'integer'];
}