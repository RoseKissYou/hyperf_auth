<?php

declare(strict_types=1);

namespace App\Model;

use Hyperf\DbConnection\Model\Model;

class Task extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'auth_task';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['to_module_name','from_module_name','task_no','content','status','add_time','name','reason'];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'add_time' => 'datetime:Y-m-d H:i:s',
    ];

    public $timestamps = false;

    const FAIL = 21; //失败
    const SUCESS = 11; //成功
    const GOING = 1; //执行中

   
}
