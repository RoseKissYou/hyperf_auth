<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */
namespace App\Service\Auth;

use App\Constants\ErrorCode;
use App\Dao\AuthAdminLogDao;
use App\Dao\AuthCompanyDao;
use App\Dao\AuthDutyDao;
use App\Dao\AuthEmployeeDao;
use App\Dao\AuthOrganizationDao;
use App\Dao\AuthProjectDao;
use App\Dao\AuthProjectEmployeeDao;
use App\Dao\AuthProjectOrganizationDao;
use App\Exception\BusinessException;
use App\Model\AuthEmployee;
use App\Model\AuthProject;
use App\Model\AuthProjectEmployee;
use App\Utils\ApiUtils;
use Hyperf\Di\Annotation\Inject;
use Hyperf\DbConnection\Db;


class ProjectService
{

    /**
     * @Inject
     * @var AuthProjectDao
     */
    protected $dao;

    /**
     * @Inject
     * @var AuthProjectEmployeeDao
     */
    protected $projectEmployeeDao;

    /**
     * @Inject
     * @var AuthProjectOrganizationDao
     */
    protected $projectOrganizationDao;


    /**
     * 添加项目
     * @param $params
     * @return array
     */
    public function add($params)
    {


        if ($params['period_type'] == AuthProject::SHORT_PERIOD_TYPE && (empty($params['start_time']) || empty($params['end_time']))) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '项目周期错误');
        }

        $organizationIds = \explode(',', $params['organization_ids']);
        $principalList = make(AuthOrganizationDao::class)->searchByWhereIn(['company_id'=>$params['company_id']], 'id', $organizationIds, ['id', 'employee_id', 'pid']);
        if (count($organizationIds) != count($principalList)) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '部门错误');
        }

        if (count($organizationIds) > 1) {
            //校验负责部门中有无包含关系
            foreach ($principalList as $vo) {
                if ($vo['pid']) {
                    $parentList = make(AuthOrganizationDao::class)->findParentIds($vo['pid']);
                    if (!empty(array_intersect($organizationIds, $parentList))) {
                        throw new BusinessException(ErrorCode::ERR_BUESSUS, '负责部门中不能存在包含关系');
                    }
                }

            }
        }

        //参与全选员工，加入到项目中，并自动补齐中间的项目负责人
        $joinEmployeeList = make(AuthEmployeeDao::class)->getByWhereTrait(['company_id'=>$params['company_id'], 'entry_status'=>AuthEmployee::ENTRY_STATUS_ING, 'partake_project'=>AuthEmployee::PARTAKE_PROJECT_ALL], ['id', 'organization_id'])->toArray();
        $relevanceOrganization = [];//需要关联部门负责人的部门id数组
        if (!empty($joinEmployeeList)) {
            foreach ($joinEmployeeList as $value) {
                $relevanceOrganization = array_merge($relevanceOrganization, $this->relevanceOrganizationList($organizationIds, $value['organization_id']));
            }
            $relevanceOrganization = array_merge($relevanceOrganization, $organizationIds);
            $principalList = make(AuthOrganizationDao::class)->searchByWhereIn(['company_id'=>$params['company_id']], 'id', array_unique($relevanceOrganization), ['id', 'employee_id']);
            $organizationIds = array_unique($organizationIds);
        }

        //查看全选员工
        $checkEmployeeList = make(AuthEmployeeDao::class)->pluckByWhereTrait(['company_id'=>$params['company_id'], 'entry_status'=>AuthEmployee::ENTRY_STATUS_ING, 'check_project'=>AuthEmployee::CHECK_PROJECT_ALL], 'id')->toArray();

        $projectInfo = [
            'company_id' => $params['company_id'],
            'name' => $params['name'],
            'period_type' => $params['period_type'],
        ];
        if ($params['period_type'] == AuthProject::SHORT_PERIOD_TYPE) {
            $projectInfo['start_time'] = intval($params['start_time']);
            $projectInfo['end_time'] = intval($params['end_time']);
        }
        if (!empty($params['detail'])) {
            if (strlen($params['detail']) > config('web.project_detail_max_size')) {
                throw new BusinessException(ErrorCode::ERR_BUESSUS, '项目描述超过最大字符数');
            }
            $projectInfo['detail'] = $params['detail'];
        }

        Db::beginTransaction();
        try {
            $projectId = $this->dao->add($projectInfo);

            //部门
            $projectOrganization = [
                'company_id' => $params['company_id'],
                'project_id' => $projectId,
            ];
            foreach ($organizationIds as $vo) {
                if($vo){
                    $projectOrganization['organization_id'] = $vo;
                    $this->projectOrganizationDao->add($projectOrganization);
                }

            }

            $projectEmployee = [
                'company_id' => $params['company_id'],
                'project_id' => $projectId,
                'type' => AuthProjectEmployee::JOIN_TYPE,
            ];
            //部门负责人
            foreach ($principalList as $vo) {
                if ($vo['employee_id']) {
                    $projectEmployee['organization_id'] = $vo['id'];
                    $projectEmployee['employee_id'] = $vo['employee_id'];
                    $this->projectEmployeeDao->add($projectEmployee);
                }
            }
            //全选参与员工
            foreach ($joinEmployeeList as $vo) {
                $projectEmployee['organization_id'] = 0;
                $projectEmployee['employee_id'] = $vo['id'];
                $this->projectEmployeeDao->add($projectEmployee);
            }
            //全选查看员工
            $checkEmployee = [
                'company_id' => $params['company_id'],
                'project_id' => $projectId,
                'type' => AuthProjectEmployee::NOT_DEL,
            ];
            foreach ($checkEmployeeList as $vo) {
                $checkEmployee['employee_id'] = $vo;
                $this->projectEmployeeDao->add($checkEmployee);
            }
            make(AuthAdminLogDao::class)->addLog('添加项目' . json_encode($params));
            Db::commit();
        } catch (\Throwable $e) {
            Db::rollBack();
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '添加项目失败'.config('web.err_msg_splite').$e->getMessage());
        }


        return ApiUtils::send(ErrorCode::SUCCESS, '添加成功' , $projectId);

    }


    /**
     * 获取关联的部门id（向上或者向下）
     * @param $organizationList
     * @param $organizationId
     * @return array
     */
    public function relevanceOrganizationList(&$organizationList, $organizationId)
    {
        $list = [];//目标数组
        $organizationIds = make(AuthOrganizationDao::class)->findParentIds($organizationId);
        $intersect = array_intersect($organizationList, $organizationIds);

        if (empty($intersect)) {//下拉
            $organizationIds = make(AuthOrganizationDao::class)->findBranchIds($organizationId);
            $intersect = array_intersect($organizationList, $organizationIds);
            if (!empty($intersect)) {
                $organizationList = array_diff($organizationList, $intersect);//负责部门中，去除下级部门
                $organizationList[] = $organizationId;
                foreach ($intersect as $vo) {//下拉有多条时，每条都拉
                    $organizationBranchIds = make(AuthOrganizationDao::class)->findParentIds($vo);//只能由下往上推
                    foreach ($organizationBranchIds as $v) {
                        if($organizationId == $v){
                            break;
                        }
                        $list[] = $vo;
                    }
                }
            }

        } else {//上推
            $extremeOrganization = reset($intersect);
            foreach ($organizationIds as $vo) {
                if($extremeOrganization == $vo){
                    break;
                }
                $list[] = $vo;
            }
        }

        if (empty($list)) {//都不存在
            $organizationList[] = $organizationId;
        }

        return $list;
    }

    /**
     * 编辑项目
     * @param $params
     * @return array
     */
    public function edit($params)
    {
        if ($params['period_type'] == AuthProject::SHORT_PERIOD_TYPE && (empty($params['start_time']) || empty($params['end_time']))) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '项目周期错误');
        }

        $organizationIds = empty($params['organization_ids']) ? [] : \explode(',', $params['organization_ids']);
        $principalList = [];//需要添加的部门负责人
        $delEmployeeIds = [];//需要移除的参与人员
        $allDelOrganization = [];

        $projectOrganization = $this->projectOrganizationDao->searchAll(['company_id'=>$params['company_id'], 'project_id'=>$params['id']], ['organization_id']);
        $projectOrganization = array_column($projectOrganization, 'organization_id');
        $addOrganization = array_diff($organizationIds, $projectOrganization);
        $delOrganization = array_diff($projectOrganization, $organizationIds);

        if(!empty($addOrganization)){
            $principalList = make(AuthOrganizationDao::class)->searchByWhereIn(['company_id'=>$params['company_id']], 'id', $addOrganization, ['id', 'employee_id']);
            if (!empty(array_diff($addOrganization, array_column($principalList, 'id')))) {
                throw new BusinessException(ErrorCode::ERR_BUESSUS, '部门错误');
            }
        }

        if(!empty($delOrganization)){
            $delOrganizationList = make(AuthOrganizationDao::class)->searchRecursionOrganization($delOrganization, ['id']);
            $allDelOrganization = array_merge($delOrganization, array_column($delOrganizationList, 'id'));
            $delEmployeeIds = make(AuthEmployeeDao::class)->searchByOrganizationIds(['company_id' => $params['company_id']], $allDelOrganization, ['id']);
            $delEmployeeIds = array_column($delEmployeeIds, 'id');

        }

        $updateInfo = [
            'name' => $params['name'],
            'period_type' => $params['period_type'],
        ];
        if ($params['period_type'] == AuthProject::SHORT_PERIOD_TYPE) {
            $updateInfo['start_time'] = intval($params['start_time']);
            $updateInfo['end_time'] = intval($params['end_time']);
        }
        $updateInfo['detail'] = '';
        if (!empty($params['detail'])) {
            if (strlen($params['detail']) > config('web.project_detail_max_size')) {
                throw new BusinessException(ErrorCode::ERR_BUESSUS, '项目描述超过最大字符数');
            }
            $updateInfo['detail'] = $params['detail'];
        }
        Db::beginTransaction();
        try {

            $this->dao->updateByWhere(['company_id'=>$params['company_id'], 'id'=>$params['id']], $updateInfo);

            $projectOrganization = [
                'company_id' => $params['company_id'],
                'project_id' => $params['id'],
            ];
            foreach ($addOrganization as $vo) {
                $projectOrganization['organization_id'] = $vo;
                $this->projectOrganizationDao->add($projectOrganization);
            }
            if (!empty($delOrganization)) {
                $this->projectOrganizationDao->delByWhereIn(['company_id'=>$params['company_id'], 'project_id'=>$params['id']], $delOrganization);
                $this->projectEmployeeDao->updateByWhereInEmployee(['company_id'=>$params['company_id'], 'project_id'=>$params['id'], 'organization_id'=>0, 'type'=>AuthProjectEmployee::JOIN_TYPE], $delEmployeeIds, ['update_time'=>time(), 'is_del'=>AuthProjectEmployee::DEL]);
                $this->projectEmployeeDao->updateByWhereInOrganization(['company_id'=>$params['company_id'], 'project_id'=>$params['id'], 'type'=>AuthProjectEmployee::JOIN_TYPE], $allDelOrganization, ['update_time'=>time(), 'is_del'=>AuthProjectEmployee::DEL]);
            }


            //部门负责人
            $projectEmployee = [
                'company_id' => $params['company_id'],
                'project_id' => $params['id'],
                'type' => AuthProjectEmployee::JOIN_TYPE,
            ];
            foreach ($principalList as $vo) {
                if ($vo['employee_id']) {
                    $projectEmployee['organization_id'] = $vo['id'];
                    $projectEmployee['employee_id'] = $vo['employee_id'];
                    $this->projectEmployeeDao->add($projectEmployee);
                }

            }
            make(AuthAdminLogDao::class)->addLog('编辑项目' . json_encode($params));
            Db::commit();
        } catch (\Throwable $e) {
            Db::rollBack();
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '编辑项目失败'.config('web.err_msg_splite').$e->getMessage());
        }


        return ApiUtils::send(ErrorCode::SUCCESS, '编辑成功');

    }

    /**
     * 项目列表
     * @param $params
     * @return array
     */
    public function list($params)
    {
        $where = [
            ['company_id', '=', $params['company_id']],
            ['is_del', '=', AuthProject::NOT_DEL],
        ];

        if (!empty($params['name'])) {
            $where[] = ['name', 'like', "%{$params['name']}%"];
        }

        if (!empty($params['start_time'])) {
            $where[] = ['add_time', '>=', strtotime($params['start_time'])];
        }

        if (!empty($params['end_time'])) {
            $where[] = ['add_time', '<', strtotime($params['end_time'])];
        }

        if (!empty($params['status'])) {
            $where[] = ['status', '=', $params['status']];
        }

        $list = $this->dao->getListPage($where, $params['page'], $params['pageSize'], ['id', 'name', 'status', 'add_time', 'period_type', 'start_time', 'end_time']);
        $ids = array_column($list['data'], 'id');

        $whereEmployee = [
            'company_id' => $params['company_id'],
            'is_del' => AuthProject::NOT_DEL,
            'type' => AuthProjectEmployee::JOIN_TYPE,
        ];
        $employeeCountArray = $this->projectEmployeeDao->searchEmployeeCountAll($whereEmployee, $ids, ['project_id', Db::raw('COUNT(DISTINCT employee_id) AS total'),]);
        $employeeCountArray = array_column($employeeCountArray, 'total', 'project_id');


        $organizationArray = $this->projectOrganizationDao->searchProjectsOrganizationAll(['auth_project_organization.company_id' => $params['company_id']], $ids, ['auth_project_organization.project_id', 'auth_organization.name', 'auth_organization.id']);
        $data = [];
        foreach ($organizationArray as $v) {
            $data[$v['project_id']][] = ['id' => $v['id'], 'name' => $v['name']];
        }

        foreach ($list['data'] as $k => $v) {
            $list['data'][$k]['employee'] = $employeeCountArray[$v['id']];
            $list['data'][$k]['organization'] = $data[$v['id']];
        }

        return ApiUtils::send(ErrorCode::SUCCESS, '查询成功', $list['data'], $list['total']);
    }

    /**
     * 项目详情
     * @param $params
     * @return array
     */
    public function info($params)
    {
        $info = $this->dao->searchFirst(['id'=>$params['id'], 'is_del' => AuthProject::NOT_DEL], ['id', 'name', 'add_time', 'period_type', 'start_time', 'end_time', 'detail']);
        if (empty($info)) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '项目不存在');
        }

        $info['employee'] = $this->projectEmployeeDao->searchEmployeeCount(['project_id'=>$params['id'], 'type'=>AuthProjectEmployee::JOIN_TYPE, 'is_del'=>AuthProjectEmployee::NOT_DEL]);
        $info['organization'] = $this->projectOrganizationDao->searchProjectsOrganizationAll(['auth_project_organization.company_id' => $params['company_id']], [$params['id']], ['auth_organization.id', 'auth_organization.name']);

        return ApiUtils::send(ErrorCode::SUCCESS, '查询成功', $info);
    }

    /**
     * 编辑项目状态
     * @param $params
     * @return array
     */
    public function editType($params)
    {
        switch ($params['type']){
            case 1:
                $data = ['status'=>AuthProject::IS_STATUS];
                break;
            case 2:
                $data = ['status'=>AuthProject::NOT_STATUS];
                break;
            case 3:
                $data = ['is_del'=>AuthProject::DEL];
                /*if($this->projectEmployeeDao->searchEmployeeCount(['project_id'=>$params['id'], 'type'=>AuthProjectEmployee::JOIN_TYPE, 'is_del'=>AuthProjectEmployee::NOT_DEL])){
                    throw new BusinessException(ErrorCode::ERR_BUESSUS, '该项目存在参与人员，请先移除');
                }*/
                break;
            default:
                throw new BusinessException(ErrorCode::ERR_BUESSUS, '编辑项目状态不正确');
        }
        $this->dao->updateByWhere(['id'=>$params['id']], $data);
        make(AuthAdminLogDao::class)->addLog('编辑项目状态'.json_encode($params));

        return ApiUtils::send(ErrorCode::SUCCESS, '操作成功');
    }

    /**
     * 项目员工列表
     * @param $params
     * @return array
     */
    public function employeeList($params)
    {
        if (!$this->dao->searchFirst(['id'=>$params['id'], 'company_id'=>$params['company_id'], 'is_del' => AuthProject::NOT_DEL])) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '项目不存在');
        }

        $where = [
            ['auth_project_employee.project_id', '=', $params['id']],
            ['auth_project_employee.type', '=', AuthProjectEmployee::JOIN_TYPE],
            ['auth_project_employee.is_del', '=', AuthProjectEmployee::NOT_DEL],
        ];

        if (!empty($params['name'])) {
            $where[] = ['auth_employee.name', 'like', "%{$params['name']}%"];
        }

        if (!empty($params['start_time'])) {
            $where[] = ['auth_employee.entry_time', '>=', strtotime($params['start_time'])];
        }

        if (!empty($params['end_time'])) {
            $where[] = ['auth_employee.entry_time', '<', strtotime($params['end_time'])];
        }

        if (!empty($params['job_number'])) {
            $where[] = ['auth_employee.job_number', '=', $params['job_number']];
        }

        if (!empty($params['phone'])) {
            $where[] = ['auth_employee.phone', '=', $params['phone']];
        }

        if (!empty($params['duty_id'])) {
            $where[] = ['auth_employee.duty_id', '=', $params['duty_id']];
        }

        if (!empty($params['organization_id'])) {
            $where[] = ['auth_employee.organization_id', '=', $params['organization_id']];
        }

        if (!empty($params['entry_status'])) {
            $where[] = ['auth_employee.entry_status', '=', $params['entry_status']];
        }

        $columns = [
            'auth_project_employee.employee_id',
            'auth_employee.job_number',
            'auth_employee.duty_id',
            'auth_employee.organization_id',
            'auth_employee.name',
            'auth_employee.phone',
            'auth_employee.entry_status',
            'auth_employee.entry_time',
        ];
        $list = $this->projectEmployeeDao->getEmployeeListPage($where, $params['page'], $params['pageSize'], $columns);

        $dutyArray = make(AuthDutyDao::class)->getArrayWhereInKey('id', array_column($list['data'], 'duty_id'), ['id', 'name']);
        $organizationArray = make(AuthOrganizationDao::class)->getArrayWhereInKey('id', array_column($list['data'], 'organization_id'), ['id', 'prefix_name']);
        $dutyArray = array_column($dutyArray, 'name', 'id');
        $organizationArray = array_column($organizationArray, 'prefix_name', 'id');

        foreach ($list['data'] as $k => $v) {
            $list['data'][$k]['duty'] = $dutyArray[$v['duty_id']];
            $list['data'][$k]['organization'] = empty($organizationArray[$v['organization_id']]) ? '' :rtrim($organizationArray[$v['organization_id']], '-');
        }

        return ApiUtils::send(ErrorCode::SUCCESS, '查询成功', $list['data'], $list['total']);
    }


    /**
     * 获取项目下级组织架构
     * @param $params
     * @return array
     */
    public function organization($params) {

        if (!$this->dao->searchFirst(['id'=>$params['id'], 'company_id'=>$params['company_id'], 'is_del' => AuthProject::NOT_DEL])) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '项目不存在');
        }


        $list = [
            'organization' => [],
            'employee' => [],
        ];
        if (empty($params['organization_id'])) {//获取负责部门
            $where = [
                'auth_project_organization.company_id' => $params['company_id'],
                'auth_project_organization.project_id' => $params['id'],
            ];
            $columns = [
                'auth_organization.id',
                'auth_organization.employee_id',
                'auth_organization.name'
            ];
            $list['organization'] = $this->projectOrganizationDao->searchProjectsOrganization($where, $columns);
        } else {//获取部门下级
            $where = [
                'auth_project_employee.company_id' => $params['company_id'],
                'auth_project_employee.project_id' => $params['id'],
                'auth_project_employee.type' => AuthProjectEmployee::JOIN_TYPE,
                'auth_project_employee.is_del' => AuthProjectEmployee::NOT_DEL,
            ];
            $employeeAll = $this->projectEmployeeDao->searchAll($where, ['organization_id']);
            //获取部门
            $organizationWhere = [
                'auth_organization.company_id' => $params['company_id'],
                'auth_organization.pid' => $params['organization_id'],

            ];
            $list['organization'] = make(AuthOrganizationDao::class)->searchByWhereIn($organizationWhere, 'id', array_unique(array_column($employeeAll, 'organization_id')), ['id', 'name', 'employee_id']);

            //获取员工
            $where['auth_employee.organization_id'] = $params['organization_id'];
            $where['auth_project_employee.organization_id'] = 0;
            $columns = [
                'auth_employee.id',
                'auth_employee.duty_id',
                'auth_employee.name'
            ];
            $list['employee'] = $this->projectEmployeeDao->getEmployeeAll($where, $columns);
            if(!empty($list['employee'])){
                $dutyArray = make(AuthDutyDao::class)->getArrayWhereInKey('id', array_column($list['employee'], 'duty_id'), ['id', 'name']);
                $dutyArray = array_column($dutyArray, 'name', 'id');
                foreach ($list['employee'] as $k => $v) {
                    $list['employee'][$k]['duty_name'] = $dutyArray[$v['duty_id']];
                }
            }
        }

        if(!empty($list['organization'])){
            $employeeList = make(AuthEmployeeDao::class)->getArrayWhereInKey('id', array_column($list['organization'], 'employee_id'), ['id', 'name']);
            $employeeList = array_column($employeeList, 'name', 'id');
            foreach ($list['organization'] as $k => $v) {
                $list['organization'][$k]['employee_name'] = $employeeList[$v['employee_id']];
            }
        }

        return ApiUtils::send(ErrorCode::SUCCESS, '查询成功', $list);

    }

    /**
     * 获取项目全部员工ID
     */
    public function employeeAll($params)
    {
        if (!$this->dao->searchFirst(['id'=>$params['id'], 'company_id'=>$params['company_id'], 'is_del' => AuthProject::NOT_DEL])) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '项目不存在');
        }

        $where = [
            'project_id' => $params['id'],
            'type' => AuthProjectEmployee::JOIN_TYPE,
            'is_del' => AuthProjectEmployee::NOT_DEL,
        ];

        $list = $this->projectEmployeeDao->searchAll($where, ['employee_id', 'organization_id']);
        //$list = array_column($list, 'employee_id');

        return ApiUtils::send(ErrorCode::SUCCESS, '查询成功', $list, count($list));
    }

    /**
     * 编辑项目员工(管理员)
     * @param $params
     * @return array
     */
    public function employeeEdit($params) {

        if (!$this->dao->searchFirst(['id'=>$params['id'], 'company_id'=>$params['company_id'], 'is_del' => AuthProject::NOT_DEL])) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '项目不存在');
        }

        $employeeIds = array_column($params['employee_list'], 'employee_id');//empty($params['employee_list']) ? [] : \explode(',', $params['employee_ids']);
        $organizationIds = empty($params['organization_ids']) ? [] : \explode(',', $params['organization_ids']);

        $allOrganization = make(AuthOrganizationDao::class)->getByWhereTrait(['company_id'=>$params['company_id']], ['id', Db::raw("CONCAT(id, '_', employee_id) AS id_employee")])->toArray();
        if (!empty(array_diff($organizationIds, array_column($allOrganization, 'id')))) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '部门错误');
        }

        $allEmployeeIds = make(AuthEmployeeDao::class)->searchByWhereIn(['company_id'=>$params['company_id']], 'id', $employeeIds, ['id']);
        if (!empty(array_diff($employeeIds, array_column($allEmployeeIds, 'id')))) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '员工错误');
        }
        $allOrganizationEmployee = array_column($allOrganization, 'id_employee');
        $organizationEmployee = [];

        foreach ($params['employee_list'] as $v) {
            $tmp = $v['organization_id'] . '_' . $v['employee_id'];
            if ($v['organization_id'] && !in_array($tmp, $allOrganizationEmployee)) {
                throw new BusinessException(ErrorCode::ERR_BUESSUS, '部门负责人错误，部门id:'.$v['organization_id'].'，负责人id:'.$v['employee_id']);
            }
            $organizationEmployee[$tmp] = $v['employee_id'];
        }

        //部门
        $existOrganizationIds = $this->projectOrganizationDao->searchAll(['company_id'=>$params['company_id'], 'project_id'=>$params['id']], ['organization_id']);
        $existOrganizationIds = array_column($existOrganizationIds, 'organization_id');
        $delOrganization = array_diff($existOrganizationIds, $organizationIds);//需要移除的部门
        $addOrganization = array_diff($organizationIds, $existOrganizationIds);//需要添加的部门

        //员工
        $where = [
            'company_id'=>$params['company_id'],
            'project_id' => $params['id'],
            'type' => AuthProjectEmployee::JOIN_TYPE,
            'is_del' => AuthProjectEmployee::NOT_DEL,
        ];
        $existEmployeeIds = $this->projectEmployeeDao->searchAll($where, ['employee_id', Db::raw("CONCAT(id, '_', employee_id) AS id_employee")]);
        $existEmployeeIds = array_column($existEmployeeIds, 'employee_id','id_employee');
        $delEmployeeIds = array_diff_key($existEmployeeIds, $organizationEmployee);//需要移除的参与人员
        $addEmployeeIds = array_diff_key($organizationEmployee, $existEmployeeIds);//需要新增的参与人员

        Db::beginTransaction();
        try {

            if ($delOrganization) {
                $this->projectOrganizationDao->delByWhereIn(['company_id'=>$params['company_id'], 'project_id'=>$params['id']], $delOrganization);
            }

            if ($addOrganization) {
                $projectOrganization = [
                    'company_id' => $params['company_id'],
                    'project_id' => $params['id'],
                ];
                foreach ($addOrganization as $vo) {
                    $projectOrganization['organization_id'] = $vo;
                    $this->projectOrganizationDao->add($projectOrganization);
                }
            }

            if ($delEmployeeIds) {
                $this->projectEmployeeDao->updateByWhereInEmployee(['company_id'=>$params['company_id'], 'project_id'=>$params['id'], 'type'=>AuthProjectEmployee::JOIN_TYPE], array_values($delEmployeeIds), ['update_time'=>time(), 'is_del'=>AuthProjectEmployee::DEL]);
            }

            if ($addEmployeeIds) {
                $projectEmployee = [
                    'company_id' => $params['company_id'],
                    'project_id' => $params['id'],
                    'type' => AuthProjectEmployee::JOIN_TYPE,
                ];

                foreach ($addEmployeeIds as $key => $vo) {
                    $tmp = \explode('_', $key);
                    $projectEmployee['organization_id'] = $tmp[0];
                    $projectEmployee['employee_id'] = $vo;
                    $this->projectEmployeeDao->add($projectEmployee);
                }
            }
            make(AuthAdminLogDao::class)->addLog('编辑项目员工'.json_encode($params));
            Db::commit();
        } catch (\Throwable $e) {
            Db::rollBack();
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '编辑项目员工失败'.config('web.err_msg_splite').$e->getMessage());
        }


        return ApiUtils::send(ErrorCode::SUCCESS, '操作成功');
    }

    /**
     * 员工项目权限列表
     * @param $params
     * @return array
     */
    public function employeePermission($params)
    {
        $root = ['join_list' => [],'check_list' => []];
        $where = [
            'auth_project_employee.employee_id' => $params['id'],
            'auth_project_employee.is_del' => AuthProjectEmployee::NOT_DEL,
            'auth_project.is_del' => AuthProject::NOT_DEL,
        ];

        $columns = [
            'auth_project.id',
            'auth_project.name',
            'auth_project.status',
            'auth_project_employee.type',
        ];
        $list = $this->projectEmployeeDao->getProjectAll($where, $columns);

        foreach ($list as $v) {
            $root['check_list'][$v['id']] = $v;
            if($v['type'] == AuthProjectEmployee::JOIN_TYPE){
                $root['join_list'][$v['id']] = $v;
            }
        }

        $root['check_list'] = array_values($root['check_list']);
        $root['join_list'] = array_values($root['join_list']);
        return ApiUtils::send(ErrorCode::SUCCESS, '查询成功', $root);
    }

    /**
     * 编辑项目员工（部门负责人）
     * @param $params
     * @return array
     */
    public function employeeEditPrincipal($params) {

        if (!$this->dao->searchFirst(['id'=>$params['id'], 'company_id'=>$params['company_id'], 'is_del' => AuthProject::NOT_DEL])) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '项目不存在');
        }

        if (!$this->projectEmployeeDao->searchFirst([
            'project_id' => $params['id'],
            'employee_id' => $params['principal_id'],
            'type' => AuthProjectEmployee::JOIN_TYPE,
            'is_del' => AuthProjectEmployee::NOT_DEL
        ])){
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '没有参与到项目');
        }

        $employeeIds = empty($params['employee_ids']) ? [] : \explode(',', $params['employee_ids']);
        $organizationIds =  make(AuthOrganizationDao::class)->pluckByWhereTrait(['employee_id'=>$params['principal_id']], 'id')->toArray();
        if(empty($organizationIds)){
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '不是部门负责人，没有权限编辑');
        }


        $organizationList = make(AuthOrganizationDao::class)->searchRecursionOrganization($organizationIds, ['id']);
        $organizationEmployeeIds = make(AuthEmployeeDao::class)->getArrayWhereInKey('organization_id', $organizationIds, ['id']);
        $principalEmployeeIds = make(AuthOrganizationDao::class)->getArrayWhereInKey('pid', $organizationIds, ['employee_id']);
        $organizationEmployeeIds = array_column($organizationEmployeeIds, 'id');
        $principalEmployeeIds = array_column($principalEmployeeIds, 'employee_id');
        $allEmployeeIds = make(AuthEmployeeDao::class)->searchByOrganizationIds(['company_id' => $params['company_id']], array_column($organizationList, 'id'), ['id']);
        $allEmployeeIds = array_diff(array_merge(array_column($allEmployeeIds, 'id'), $principalEmployeeIds), [$params['principal_id']]);

        if (array_diff($employeeIds, array_merge($organizationEmployeeIds, $principalEmployeeIds))) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '员工错误, 只能编辑负责部门下的员工');
        }

        $where = [
            'project_id' => $params['id'],
            'type' => AuthProjectEmployee::JOIN_TYPE,
            'is_del' => AuthProjectEmployee::NOT_DEL
        ];

        $existEmployeeIds = $this->projectEmployeeDao->searchByWhereIn($where, 'employee_id', $allEmployeeIds, ['employee_id']);
        $existEmployeeIds = array_column($existEmployeeIds, 'employee_id');
        $delEmployeeIds = array_diff($existEmployeeIds, $employeeIds);//需要移除的参与人员
        $addEmployeeIds = array_diff($employeeIds, $existEmployeeIds);//需要新增的参与人员

        Db::beginTransaction();
        try {

            if ($delEmployeeIds) {
                $this->projectEmployeeDao->updateByWhereInEmployee(['company_id'=>$params['company_id'], 'project_id'=>$params['id'], 'type'=>AuthProjectEmployee::JOIN_TYPE], $delEmployeeIds, ['update_time'=>time(), 'is_del'=>AuthProjectEmployee::DEL]);
            }

            if ($addEmployeeIds) {
                $projectEmployee = [
                    'company_id' => $params['company_id'],
                    'project_id' => $params['id'],
                    'type' => AuthProjectEmployee::JOIN_TYPE,
                ];
                foreach ($addEmployeeIds as $vo) {
                    $projectEmployee['employee_id'] = $vo;
                    $this->projectEmployeeDao->add($projectEmployee);
                }
            }

            Db::commit();
        } catch (\Throwable $e) {
            Db::rollBack();
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '编辑项目员工失败'.config('web.err_msg_splite').$e->getMessage());
        }

        return ApiUtils::send(ErrorCode::SUCCESS, '操作成功');
    }
}
