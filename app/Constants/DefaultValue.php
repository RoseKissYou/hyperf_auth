<?php


namespace App\Constants;

/**
 * 默认值
 * Class DefaultValue
 * @package App\Constants
 */
class DefaultValue
{

    /*
     * 图形验证码过期时间
     */
    const IMAGE_CAPTCHA_TTL = 1800;

    /*
     * 授权码验证通过后设置账号过期时间
     */
    const AUTH_ACCOUNT_TTL = 7200;
}
