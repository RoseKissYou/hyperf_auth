<?php

declare (strict_types = 1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

namespace App\Validate;

class OrganizationValidation extends BaseValidation
{
    // 调用案例 check支持自定义规则

    // make(TaskValidation::class)->check($this->request->all(),__FUNCTION__)

    //返回响应值的  ->response()

    //返回boole   ->get()

    //返回报错信息  ->getError()

    //验证规则
    protected $rule = [
        'name' => 'required',
        'pid' => 'required|integer',
        'organization_id' => 'required|integer',
        'employee_id' => 'required|integer',
        'sort_type' => 'required|in:1,2',
        'operation_confirm' => 'required|in:1,2',
    ];

    //自定义验证信息
    protected $message = [
        'organization_id.required' => '组织id不能为空',
        'pid.required' => '上级id不能为空',
        'employee_id.required' => '员工不能为空',
        'name.required' => '名称不能为空',
        'sort_type.required' => '排序类型不能为空',
        'sort_type.in' => '排序类型范围错误',
        'operation_confirm.required' => '确认 必须',
        'operation_confirm.in' => '确认 取值错误',
    ];

    //自定义场景
    protected $scene = [
        'addOrganization' => ['pid', 'name', 'employee_id'],
        'editOrganization' => ['pid', 'name', 'organization_id', 'employee_id'],
        'delOrganization' => ['organization_id', 'operation_confirm'],
        // 'setLeader' => [],
        'sort' => ['sort_type', 'organization_id'],
        'project' => ['organization_id'],
    ];
}
