<?php

declare (strict_types = 1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Dao;

use App\Model\AuthProjectEmployee;
use Hyperf\DbConnection\Db;

class AuthProjectEmployeeDao
{
    //添加数据
    public function add(array $data)
    {
        $data['add_time'] = time();
        return AuthProjectEmployee::query()->insertGetId($data);

    }

    //通过条件搜索单一条
    public function searchFirst($where, $columns = ['*'])
    {
        return AuthProjectEmployee::query()->where($where)->first($columns);

    }

    public function searchAll($where, $columns = ['*'])
    {
        return AuthProjectEmployee::query()->where($where)->get($columns)->toArray();

    }

    //带条件的分页
    public function pageByWhere($where, $page, $pageSize, $columns = ['*'])
    {
        return AuthProjectEmployee::query()->where($where)->paginate(intval($pageSize), $columns, 'page', intval($page))->toArray();

    }

    public function updateByWhere($where, $data)
    {
        return AuthProjectEmployee::query()->where($where)->update($data);

    }

    public function updateByWhereInEmployee($where, $whereIn, $data)
    {
        return AuthProjectEmployee::query()->where($where)->whereIn('employee_id', $whereIn)->update($data);
    }

    public function updateByWhereInOrganization($where, $whereIn, $data)
    {
        return AuthProjectEmployee::query()->where($where)->whereIn('organization_id', $whereIn)->update($data);
    }

    public function delPrincipalByWhereIn(array $project_ids)
    {
        return AuthProjectEmployee::query()->whereIn('id', $project_ids)->update(['is_del' => 2, 'update_time' => time()]);
    }

    public function searchEmployeeCountAll($where, $projectIds, $columns = ['*'])
    {
        return AuthProjectEmployee::query()->where($where)->whereIn('project_id', $projectIds)->groupBy(['project_id'])->get($columns)->toArray();

    }

    public function searchEmployeeCount($where)
    {
        return AuthProjectEmployee::query()->where($where)->count(Db::raw('DISTINCT employee_id'));

    }

    public function getEmployeeListPage($where = [], $page = 1, $pageSize = 15, $columns = ['*'])
    {
        return AuthProjectEmployee::query()
            ->leftJoin('auth_employee', 'auth_project_employee.employee_id', '=', 'auth_employee.id')
            ->where($where)
            ->groupBy(['auth_project_employee.employee_id'])
            ->paginate(intval($pageSize), $columns, 'page', intval($page))
            ->toArray();

    }

    public function getEmployeeAll($where = [], $columns = ['*'])
    {
        return AuthProjectEmployee::query()
            ->leftJoin('auth_employee', 'auth_project_employee.employee_id', '=', 'auth_employee.id')
            ->where($where)
            ->groupBy(['auth_project_employee.employee_id'])
            ->get($columns)
            ->toArray();
    }

    public function firstOrCreate($data, $map = [])
    {
        if (empty($map)) {
            return AuthProjectEmployee::query()->firstOrCreate($data);
        } else {
            return AuthProjectEmployee::query()->firstOrCreate($map, $data);
        }
    }

    public function delNotInIds(array $project_ids, array $map)
    {
        return AuthProjectEmployee::query()->where($map)->whereNotIn('project_id', $project_ids)->update([
            'is_del' => 2,
            'update_time' => time(),
        ]);
    }

    public function getProjectAll($where = [], $columns = ['*'])
    {
        return AuthProjectEmployee::query()
            ->leftJoin('auth_project', 'auth_project_employee.project_id', '=', 'auth_project.id')
            ->where($where)
            ->get($columns)
            ->toArray();
    }

    public function searchByWhereIn($where, $key, $whereIn, $columns = ['*'])
    {
        return AuthProjectEmployee::query()->where($where)->whereIn($key, $whereIn)->get($columns)->toArray();

    }

}
