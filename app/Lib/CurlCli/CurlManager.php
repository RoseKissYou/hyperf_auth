<?php

declare(strict_types=1);

namespace App\Lib\CurlCli;

/**
 * @Description 网络请求类
 * @author wuxian
 * @since 2020-04-15
 */
class CurlManager
{
    protected $network;

    /**
     * @param array $config  基本配置
     * @return null
     */
    public function __construct($config = [], $driver = 'saber')
    {
        switch ($driver) {
            case 'guzzle':
                $this->network = make(GuzzleNetwork::class, ['config'=>$config]);
                break;
            default:
                $this->network = make(SaberNetwork::class, ['config'=>$config]);

        }
        return $this->network;
    }

    /**
     * @param string $url  get请求的地址
     * @return array
     */
    public function get($url = '', $options = [])
    {
        return $this->network->get($url, $options);
    }
    /**
     * @param string $url  post请求的地址
     * @param array $data  post请求数据
     * @return array
     */
    public function post($url = '', $data = [], $options = [])
    {
        return $this->network->post($url, $data, $options);
    }
}
