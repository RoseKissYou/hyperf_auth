<?php

declare (strict_types = 1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Utils\GrpcClient;

use App\Utils\ServerUtil;
use Hyperf\GrpcClient\BaseClient;
use Pb\Params;
use Pb\Reply;

class OAClient extends BaseClient
{
    public function __construct(string $hostname = '', array $options = [
        'credentials' => null,
    ]) {
        if (empty($hostname)) {
            $hostname = env('GRPC_OA_HOST', '192.168.0.41') . ':' . env('GRPC_OA_PORT', '9621');
        }
        parent::__construct($hostname, $options);
    }

    public function curdClient(Params $argument)
    {
        $req = $argument->getRequest();
        !empty($req) ? $argument->setRequest(ServerUtil::opensslEncrypt($req)) : '';
        list($reply, $status) = $this->_simpleRequest(
            '/pb.client/curdClient',
            $argument,
            [Reply::class, 'decode']
        );
        $data = $reply->getData();
        !empty($data) ? $reply->setData(ServerUtil::opensslDecrypt($data)) : '';
        return [$reply, $status];
    }
}
