<?php

declare(strict_types=1);

namespace App\Model;

use Hyperf\DbConnection\Model\Model;

class Component extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'auth_component';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['component_name','add_time','pid','icon','content','url','module_id','is_web','identity','sort'];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'add_time' => 'datetime:Y-m-d H:i:s',
    ];

    public $timestamps = false;

    public function module()
    {
        return $this->belongsTo(Module::class, 'module_id', 'id');
    }

    const IS_DIR = 3; //目录
    const BUTTON = 2; //按钮
    const IS_WEB = 1; //页面
   
}
