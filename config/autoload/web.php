<?php

declare (strict_types = 1);

return [
    'cli_jwt_key' => 'cli tasksystem code', //生成用户jwt的密匙
    'user_jwt_key' => 'authcentercode5facee14951cc', //y3用户jwt的密匙
    'admin_jwt_key' => 'admin tasksystem code', //生成后台jwt的密匙
    'slot' => 'jfdlsgjlk12152154gasdgjklgj', //密码加密的盐
    'super' => [1], //超级管理员
    'err_msg_splite' => '@#', //用户程序员错误分割符
    'active_capcha' => true, // 是否关闭验证码 true 关闭  false 启动
    'super_permission' => [1], //超级管理员特有权限,其他的不显示,也不会分配这个权限
    'permission_redis_key' => 'depart_permission_', //部门权限redis缓存key
    'user_cache_key' => 'auth_employee_', //用户信息权限redis缓存key
    'aes_key' => '3c6e0b8a9c15224a8228b9a98ca1531d', //AES密匙
    'aes_iv' => 'f0b53b2da041fca4', //AES向量
    'aes_key_ws' => '0550a59af93c4f13e659eab437e513be', //云聊web-socket-AES密匙
    'aes_iv_ws' => 'JchzrmJQMbhIwYIN', //云聊web-socket-AES向量
    'ws_jwt_key' => 'websocketJchzrmJQMbhIwYIN', //websocket连接验证字符串
    'jwt_token_time' => 28800, //jwt-token有效时间
    'project_detail_max_size' => 10000, //项目描述最大字符数

    'fail_num' => 3, //任务失败重试次数
];
