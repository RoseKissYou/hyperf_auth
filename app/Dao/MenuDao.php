<?php

declare (strict_types = 1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Dao;

use App\Model\Menu;

class MenuDao
{
    //添加数据
    public function add(array $data)
    {
        $data['add_time'] = time();
        return Menu::query()->insertGetId($data);
    }

    //添加数据多条
    public function insert(array $data)
    {
        return Menu::query()->insert($data);
    }

    //更新
    public function update($where, $data)
    {
        return Menu::query()->where($where)->update($data);
    }

    //删除
    public function delete($where, $whereIn = [])
    {
        return Menu::query()->where($where)->when($whereIn, function ($query, $whereIn) {
            return $query->whereIn('id', $whereIn);
        })->delete();
    }

    //通过条件搜索单一条
    public function searchFirst($where, $columns = ['*'])
    {
        return Menu::query()->where($where)->first($columns);
    }

    public function searchAll($where = [], $whereIn = [], $columns = ['*'])
    {
        return Menu::query()->with('components')->where($where)->when($whereIn, function ($query, $whereIn) {
            return $query->whereIn('id', $whereIn);
        })->orderBy('sort', 'desc')->orderBy('id', 'desc')->get($columns)->toArray();
    }

    //找出主键id
    public function searchAllId($where = [], $whereIn = [])
    {
        return Menu::query()->where($where)->when($whereIn, function ($query, $whereIn) {
            return $query->whereIn('pid', $whereIn);
        })->pluck('id')->toArray();
    }

    //满足条件的最大一条
    public function MaxFirst($where, $columns = ['*'])
    {
        return Menu::query()->where($where)->orderBy('sort', 'desc')->orderBy('id', 'desc')->first($columns);
    }
    //满足条件的最小一条
    public function MinFirst($where, $columns = ['*'])
    {
        return Menu::query()->where($where)->orderBy('sort', 'asc')->orderBy('id', 'desc')->first($columns);
    }

}
