<?php

declare (strict_types = 1);

namespace App\Dao;

use App\Model\AuthDuty;

class AuthDutyDao
{
    use DaoTrait;
    public function __construct()
    {
        $this->driver = __CLASS__;
    }

    public function changeDriver(\Hyperf\Database\Model\Model $model)
    {
        $this->driver = $model;
        return $this;
    }

    public function resetDriver()
    {
        $this->driver = __CLASS__;
        return $this;
    }

    public function getByOrderByPid(array $map, $page, $pageSize, $columns = ['*'])
    {
        $sql = AuthDuty::when(!empty($map), function ($query) use ($map) {
            $query->where($map);
        })->orderBy('pid');
        if ($pageSize == -1) {
            $data = $sql->get()->toArray();
        } else {
            $data = $sql->paginate(intval($pageSize), $columns, 'page', intval($page))->toArray()['data'];
        }

        return $data;
    }

}
