<?php

declare(strict_types=1);

namespace App\Controller\Rbac;

use App\Constants\ErrorCode;
use App\Controller\AbstractController;
use App\Exception\BusinessException;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Context;
use App\Validate\PermissionValidation;
// 使用rbac
use Wuxian\Rbac\{Rbac};

class PermissionController extends AbstractController
{
    /**
     * @Inject
     * @var Rbac
     */
    protected $rbacObj;

    public function __construct()
    {
        $this->rbacObj = new Rbac(config('rbac.defualt'));
    }

    // 获取当前登录用户的权限列表
    public function menu()
    {
        $user_id = Context::get('jwt_token')['uid'];
        $result = $this->rbacObj->permissionList([],$user_id);
        return $this->success('ok',$result);
    }

    // 获取所有的权限列表
    public function permissionList()
    {
//        $result = $this->permissionService->menu(config('web.super')[0]);
        $result = $this->rbacObj->menu(config('web.super')[0]);
        return $this->success('ok',$result);
    }

    public function permissionAdd()
    {
        $data = $this->request->inputs(['sort_order','parent_id','url','identity','name','is_web']);
        make(PermissionValidation::class)->check($data, __FUNCTION__)->response();
//        $result = $this->permissionService->addPermssion($data);
        try {
            $result = $this->rbacObj->addPermission($data);
        }catch (\Throwable $t) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '权限名称重复,或者信息错误@#新增权限失败');
        }
        return $this->success('ok',$result);
    }

    public function permissionEdit()
    {
        if ($this->request->isMethod('POST')){
            $data = $this->request->inputs(['id','identity','name','sort_order','parent_id','url','is_web']);
            make(PermissionValidation::class)->check($data, __FUNCTION__)->response();
//            $this->permissionService->editPermissionPost($data);
            $id = (int)$data['id'];unset($data['id']);
            try {
                $update = $this->rbacObj->editPermission($id,$data);
            }catch (\Exception $exception) {
                throw new BusinessException(ErrorCode::ERR_BUESSUS, '权限名称重复,或者信息错误@#新增权限失败');
            }
            return $this->success('修改成功',$update);
        }
        make(PermissionValidation::class)->check($this->request->all(), 'permissionGet')->response();
//        $result = $this->permissionService->editPermissinGet((int)$this->request->input('id'));
        $result = $this->rbacObj->getPermissionInfo((int)$this->request->input('id'));
        return $this->success('ok',$result);
    }

    public function permissionDel()
    {
        make(PermissionValidation::class)->check($this->request->all(), 'permissionGet')->response();
//        $this->permissionService->delPermission((int)$this->request->input('id'));
        $result = $this->rbacObj->delPermission($this->request->inputs(['id']));
        if (!$result) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '要删除的信息不存在@#新增权限失败');
        }
        return $this->success('删除成功',$result);
    }

}
