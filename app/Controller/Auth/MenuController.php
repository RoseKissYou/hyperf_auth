<?php

declare (strict_types = 1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Controller\Auth;

use App\Controller\AbstractController;
use App\Service\Auth\MenuService;
use App\Validate\MenuValidation;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Context;

class MenuController extends AbstractController
{
    /**
     * @Inject
     * @var MenuService
     */
    protected $service;

    public function add()
    {
        $params = $this->request->all();
        make(MenuValidation::class)->check($params, 'add')->response();
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->add($params);
        return $this->success($result['msg'], $result['data']);

    }

    public function edit()
    {
        $params['id'] = $this->request->input('id');
        $params['pid'] = $this->request->input('pid');
        $params['menu_name'] = $this->request->input('menu_name');
        make(MenuValidation::class)->check($params, 'edit')->response();
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->edit($params);
        return $this->success($result['msg'], $result['data']);

    }

    public function del()
    {
        $params['id'] = $this->request->input('id');
        make(MenuValidation::class)->check($params, 'del')->response();
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->del($params);
        return $this->success($result['msg'], $result['data']);

    }

    public function menuAddComponent()
    {
        $params = $this->request->all();
        make(MenuValidation::class)->check($params, 'menuAddComponent')->response();
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->menuAddComponent($params);
        return $this->success($result['msg'], $result['data']);

    }

    public function menuDelComponent()
    {
        $params['menu_id'] = $this->request->input('menu_id');
        $params['component_id'] = $this->request->input('component_id');
        make(MenuValidation::class)->check($params, 'menuAddComponent')->response();
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->menuDelComponent($params);
        return $this->success($result['msg'], $result['data']);

    }

    public function menuMoveComponent()
    {
        $params['old_menu_id'] = $this->request->input('old_menu_id');
        $params['new_menu_id'] = $this->request->input('new_menu_id');
        $params['component_id'] = $this->request->input('component_id');
        make(MenuValidation::class)->check($params, 'menuMoveComponent')->response();
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->menuMoveComponent($params);
        return $this->success($result['msg'], $result['data']);

    }

    public function moveMenuComponent()
    {
        $params['old_menu_id'] = $this->request->input('old_menu_id');
        $params['new_menu_id'] = $this->request->input('new_menu_id');
        make(MenuValidation::class)->check($params, 'moveMenuComponent')->response();
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->moveMenuComponent($params);
        return $this->success($result['msg'], $result['data']);

    }

    public function menuHasComponent()
    {
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->menuHasComponent($params);
        return $this->success($result['msg'], $result['data']);

    }

    public function move()
    {
        $params['id'] = $this->request->input('id');
        make(MenuValidation::class)->check($params, 'del')->response();
        $params['type'] = $this->request->input('type', 1);
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->move($params);
        return $this->success($result['msg'], $result['data']);

    }

    public function menuList()
    {
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->menuList($params);
        return $this->success($result['msg'], $result['data']);
    }

    public function menuAll()
    {
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->menuAll($params);
        return $this->success($result['msg'], $result['data']);
    }

}
