<?php

declare (strict_types = 1);

namespace App\Service\Organziation;

use App\Constants\ErrorCode;
use App\Dao\AuthAdminLogDao;
use App\Dao\AuthDutyDao;
use App\Dao\AuthEmployeeDao;
use App\Dao\AuthOrganizationDao;
use App\Exception\BusinessException;
use App\Lib\JwtToken;
use App\Model\AuthEmployee;
use App\Service\Auth\ModuleService;
use App\Utils\ApiUtils;
use App\Utils\GrpcClient\OAClient;
use App\Utils\Redis;
use App\Utils\ServerUtil;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\Codec\Json;
use Pb\Params;
use Wuxian\WebUtils\JwtUtils;

class EmployeeService
{
    /**
     * 查询用户列表信息
     *
     * @Author czm
     * @DateTime 2020-09-30
     * @param array $params
     * @return void
     */
    function list(array $params) {

        if (isset($params['list_type']) && $params['list_type'] == 1) {
            //返回公司下面所有员工
            if (isset($params['field']) && $params['field']) {
                $field = explode(",", $params['field']);
            } else {
                $field = ['id', 'name'];
            }

            $map = [
                ['company_id', "=", $params['company_id']],
            ];
            //是否在职
            if (isset($params['entry_status']) && $params['entry_status']) {
                $map[] = ['entry_status', "=", AuthEmployee::ENTRY_STATUS_ING];
            }
            $data = make(AuthEmployeeDao::class)->getByWhereTrait($map, $field);
            if (in_array('organization_id', $field)) {
                $organizations = make(AuthOrganizationDao::class)->getByWhereTrait([
                    ['company_id', "=", $params['company_id']],
                ], ['id', 'name'])->toArray();
                $organizations = collect($organizations)->keyBy('id')->toArray();
                foreach ($data as &$val) {
                    $val['organization_name'] = $organizations[$val['organization_id']]['name'] ?? '';
                }
            }

            if (in_array('duty_id', $field)) {
                $dutys = make(AuthDutyDao::class)->getByWhereTrait([
                    ['company_id', "=", $params['company_id']],
                ], ['id', 'name'])->toArray();
                $dutys = collect($dutys)->keyBy('id')->toArray();
                foreach ($data as &$val) {
                    $val['duty_name'] = $dutys[$val['duty_id']]['name'] ?? '';
                }
            }

        } else if (isset($params['list_type']) && $params['list_type'] == 2) {
            if (!isset($params['employee_ids']) || !$params['employee_ids']) {
                throw new BusinessException(ErrorCode::ERR_BUESSUS, '指定员工id不能为空');
            }
            $ids = explode(",", (string) $params['employee_ids']);
            if (isset($params['field']) && $params['field']) {
                $field = explode(",", $params['field']);
            } else {
                $field = ['id', 'name'];
            }

            $data = make(AuthEmployeeDao::class)->getWhereInKeyTrait('id', $ids, $field);
            if (in_array('organization_id', $field)) {
                $organizations_ids = array_column($data, 'organization_id');
                $organizations = make(AuthOrganizationDao::class)->getWhereInKeyTrait('id', $organizations_ids, ['id', 'name']);
                $organizations = collect($organizations)->keyBy('id')->toArray();
                foreach ($data as &$val) {
                    $val['organization_name'] = $organizations[$val['organization_id']]['name'] ?? '';
                }
            }

            if (in_array('duty_id', $field)) {
                $duty_ids = array_column($data, 'duty_id');
                $dutys = make(AuthDutyDao::class)->getWhereInKeyTrait('id', $duty_ids, ['id', 'name']);
                $dutys = collect($dutys)->keyBy('id')->toArray();
                foreach ($data as &$val) {
                    $val['duty_name'] = $dutys[$val['duty_id']]['name'] ?? '';
                }
            }

        } else if (isset($params['list_type']) && $params['list_type'] == 3) {
            if (!isset($params['organization_ids']) || !$params['organization_ids']) {
                throw new BusinessException(ErrorCode::ERR_BUESSUS, '指定部门id不能为空');
            }
            $ids = explode(",", (string) $params['organization_ids']);

            if (isset($params['field']) && $params['field']) {
                $field = explode(",", $params['field']);
            } else {
                $field = ['id', 'name', 'organization_id'];
            }

            $data = make(AuthEmployeeDao::class)->getWhereInKeyTrait('organization_id', $ids, $field);

            $organizations = make(AuthOrganizationDao::class)->getWhereInKeyTrait('id', $ids, ['id', 'name']);
            $organizations = collect($organizations)->keyBy('id')->toArray();
            foreach ($data as &$val) {
                $val['organization_name'] = $organizations[$val['organization_id']]['name'];
            }

            if (in_array('duty_id', $field)) {
                $duty_ids = array_column($data, 'duty_id');
                $dutys = make(AuthDutyDao::class)->getWhereInKeyTrait('id', $duty_ids, ['id', 'name']);
                $dutys = collect($dutys)->keyBy('id')->toArray();
                foreach ($data as &$val) {
                    $val['duty_name'] = $dutys[$val['duty_id']]['name'] ?? '';
                }
            }

        } else {
            list($data, $count) = make(AuthEmployeeDao::class)->getWithRselevance($params, $params['page'], $params['page_size']);
        }

        return ApiUtils::send(ErrorCode::SUCCESS, '操作成功', $data, $count ?? 1);
    }

    /**
     * 新建员工
     *
     * @Author czm
     * @DateTime 2020-10-01
     * @param array $params
     * @return void
     */
    public function addEmployee(array $params)
    {
        //验证员工名称是否存在
        $map = [
            ['name', '=', $params['name']],
            ['company_id', '=', $params['company_id']],
        ];
        if (make(AuthEmployeeDao::class)->existsByWhereTrait($map)) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '公司下存在相同名称');
        }
        //验证工号是存在
        $map = [
            ['job_number', '=', $params['job_number']],
            ['company_id', '=', $params['company_id']],
        ];
        if (make(AuthEmployeeDao::class)->existsByWhereTrait($map)) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '公司下存在相同工号');
        }

        //验证账号是否存在
        $map = [
            ['account', '=', $params['account']],
        ];
        if (make(AuthEmployeeDao::class)->existsByWhereTrait($map)) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '该账号系统已存在');
        }
        //密码加密存储
        $params['password'] = ApiUtils::aesEnPassword($params['password']);
        //如果是不是全部则记录项目关联
        Db::beginTransaction();
        try {
            $employee = make(AuthEmployeeDao::class)->createTrait($params);
            $params['employee_id'] = $employee['id'];
            // if ($params['partake_project'] == AuthEmployee::PARTAKE_PROJECT_PART && $params['join_project_id']) {
            //     $join_project_id = explode(",", $params['join_project_id']);
            //     $this->bindProject($join_project_id, $params);

            // } else if ($params['partake_project'] == AuthEmployee::PARTAKE_PROJECT_ALL) {
            //     $join_project_id = array_column(make(AuthProjectDao::class)->searchAll([['company_id', '=', $params['company_id']], ['is_del', '=', 1], ['status', '=', AuthProject::IS_STATUS]], ['id']), 'id');
            //     $this->bindProject($join_project_id, $params);
            // }
            // //查看项目
            // if ($params['check_project'] == AuthEmployee::CHECK_PROJECT_PART && $params['show_project_id']) {
            //     $show_project_id = explode(",", $params['show_project_id']);
            // } else if ($params['check_project'] == AuthEmployee::CHECK_PROJECT_ALL) {
            //     $show_project_id = array_column(make(AuthProjectDao::class)->searchAll([['company_id', '=', $params['company_id']], ['is_del', '=', 1], ['status', '=', AuthProject::IS_STATUS]], ['id']), 'id');
            // } else {
            //     $show_project_id = [];
            // }
            // foreach ($show_project_id as $val) {
            //     make(AuthProjectEmployeeDao::class)->add([
            //         'employee_id' => $employee->id,
            //         'project_id' => $val,
            //         'type' => AuthProjectEmployee::SHOW_TYPE,
            //         'company_id' => $params['company_id'],
            //         'add_time' => time(),
            //         'update_time' => time(),
            //     ]);
            // }
            make(AuthAdminLogDao::class)->addLog('新增员工' . json_encode($params, JSON_UNESCAPED_UNICODE));
            //Grpc标记员工入职
            if ($employee->entry_status == AuthEmployee::ENTRY_STATUS_ING) {
                $this->entryGrpc([
                    'company_id' => $employee->company_id,
                    'organization_id' => $employee->organization_id,
                    'employee_id' => $employee->id,
                ]);
            }
            Db::commit();
        } catch (\Throwable $t) {
            Db::rollBack();
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '网络异常 数据写入失败' . config('web.err_msg_splite') . $t->getMessage());
        }
        return ApiUtils::send(ErrorCode::SUCCESS, "操作成功");
    }

    /**
     * 编辑员工
     *
     * @Author czm
     * @DateTime 2020-10-01
     * @param array $params
     * @return void
     */
    public function editEmployee(array $params)
    {
        //验证员工名称是否存在
        $map = [
            ['name', '=', $params['name']],
            ['company_id', '=', $params['company_id']],
            ['id', '!=', $params['employee_id']],
        ];
        if (make(AuthEmployeeDao::class)->existsByWhereTrait($map)) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '公司下存在相同名称');
        }

        //验证工号是存在
        $map = [
            ['job_number', '=', $params['job_number']],
            ['company_id', '=', $params['company_id']],
            ['id', '!=', $params['employee_id']],

        ];
        if (make(AuthEmployeeDao::class)->existsByWhereTrait($map)) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '公司下存在相同工号');
        }

        //验证账号是否存在
        $map = [
            ['account', '=', $params['account']],
            ['id', '!=', $params['employee_id']],
        ];
        if (make(AuthEmployeeDao::class)->existsByWhereTrait($map)) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '该账号系统已存在');
        }
        //密码加密存储
        $params['password'] = ApiUtils::aesEnPassword($params['password']);
        //如果是不是全部则记录项目关联
        Db::beginTransaction();
        try {

            $params['id'] = $params['employee_id'];
            $logInfo = make(AuthEmployeeDao::class)->findTrait($params['id']);
            make(AuthEmployeeDao::class)->updateByIdTrait($params);
            if (isset($params['entry_status']) && $logInfo->entry_status != AuthEmployee::ENTRY_STATUS_ING && $params['entry_status'] == AuthEmployee::ENTRY_STATUS_ING) {
                $this->entryGrpc([
                    'company_id' => $logInfo->company_id,
                    'organization_id' => $logInfo->organization_id,
                    'employee_id' => $logInfo->id,
                ]);
            }
            //离职
            if (isset($params['entry_status']) && $logInfo->entry_status == AuthEmployee::ENTRY_STATUS_ING && $params['entry_status'] != AuthEmployee::ENTRY_STATUS_ING) {
                $this->dimissionGrpc([
                    'company_id' => $logInfo->company_id,
                    'employee_id' => $logInfo->id,
                ]);
            }
            //换岗
            if (isset($params['organization_id']) && $logInfo->organization_id != $params['organization_id']) {
                $this->relieveGuard([
                    'company_id' => $logInfo->company_id,
                    'employee_id' => $logInfo->id,
                    'old_organization_id' => $logInfo->organization_id,
                    'new_organization_id' => $params['organization_id'],
                ]);
            }
            make(AuthAdminLogDao::class)->addLog('编辑员工' . json_encode($params, JSON_UNESCAPED_UNICODE) . "原数据 " . $logInfo->toJson(JSON_UNESCAPED_UNICODE));
            Db::commit();
            //更新redis缓存信息
            $this->userInfo(['employee_id' => $params['employee_id']]);
            if ((isset($params['role_id']) && $params['role_id'] != $logInfo->role_id) || (isset($params['organization_id']) && $params['organization_id'] != $logInfo->organization_id)) {
                make(ModuleService::class)->clearRedisPermission(0, 0, $params['employee_id']);
            }

        } catch (\Throwable $t) {
            Db::rollBack();
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '网络异常 数据写入失败' . config('web.err_msg_splite') . $t->getMessage());
        }
        return ApiUtils::send(ErrorCode::SUCCESS, "操作成功");
    }

    /**
     * 修改密码
     *
     * @Author czm
     * @DateTime 2020-09-29
     * @param array $params
     * @return void
     */
    public function editPassword(array $params)
    {
        $employee = make(AuthEmployeeDao::class)->findTrait($params['id']);

        if (!$employee) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '员工信息错误');
        }

        if ($employee->password != ApiUtils::aesEnPassword($params['password'])) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '旧密码错误');
        }

        if (!preg_match("/^\w{6,32}$/", $params['password_confirmation'])) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '格式错误 请输入6-32位数字或字母组合');
        }

        //密码加密存储
        $params['password_confirmation'] = ApiUtils::aesEnPassword($params['password_confirmation']);
        $employee->password = $params['password_confirmation'];
        $employee->save();
        make(AuthAdminLogDao::class)->addLog('用户修改密码id: ' . $params['id']);
        return ApiUtils::send(ErrorCode::SUCCESS, "操作成功");
    }

    /**
     * 账号密码登陆
     *
     * @Author czm
     * @DateTime 2020-09-29
     * @param array $params
     * @return void
     */
    public function login(array $params)
    {
        $employee = make(AuthEmployeeDao::class)->firstByWhereTrait(['account' => $params['account']]);

        if (empty($employee)) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '账号不存在');
        }
        $key = AuthEmployee::REDIS_EMPLOYEE_LOGIN_LOCK . $params['account'];
        $lock = make(Redis::class)->get($key);

        if ($lock && $lock > AuthEmployee::REDIS_EMPLOYEE_LOGIN_LOCK_NUM) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '账号验证失败次数超过上限 解锁时间24小时内');
        }

        if ($employee->password != ApiUtils::aesEnPassword($params['password'])) {
            !$lock && $lock = 0;
            make(Redis::class)->set($key, $lock + 1, ['EX' => 86400]);
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '密码错误');
        }
        //所存在则删除锁
        $lock && make(Redis::class)->del($key);

        if ($employee->status != AuthEmployee::STATUS_NORMAL) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '账号停用 请联系管理员');
        }
        $last_login_time = time();
        $employee->last_login_time = $last_login_time;
        $employee->save();
        $userInfo = $this->userInfo(['employee_id' => $employee->id])['data'];
        $jwt_token = $this->jwtToken([
            'uid' => $userInfo['id'],
            'employee_id' => $userInfo['id'],
            'name' => $userInfo['name'],
            'company_id' => $userInfo['company_id'],
            'organization_id' => $userInfo['organization_id'],
            'duty_id' => $userInfo['duty_id'],
            'last_login_time' => $last_login_time,
        ] + ['secreteKey' => config('web.user_jwt_key')])['data'];
        $ws_token = urlencode(ServerUtil::opensslEncrypt(config('web.ws_jwt_key'), config('web.aes_key_ws'), config('web.aes_iv_ws')));
        $data = [
            'permission_cache_key' => make(ModuleService::class)->userHasModule(['employee_id' => $employee->id])['data']['cache_key'],
            'user_cache_key' => config('web.user_cache_key') . $employee->id,
            'token' => $jwt_token,
            'ws_token' => $ws_token,
            'db' => env('REDIS_DB', 0),
        ];

        return ApiUtils::send(ErrorCode::SUCCESS, "操作成功", $data);
    }

    /**
     * 根据用户信息获取用户信息
     *
     * @Author czm
     * @DateTime 2020-10-10
     * @param array $params
     * @return void
     */
    public function userInfo(array $params)
    {
        $employee_id = $params['employee_id'];
        $employee = make(AuthEmployeeDao::class)->firstByWhereTrait(['id' => $employee_id]);
        if (empty($employee)) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '员工不存在');
        }
        $hasOrganization = $employee->hasOrganization;
        $hasDuty = $employee->hasDuty;
        if ($hasOrganization) {
            $organization_name = $hasOrganization->name;
        }
        if ($hasDuty) {
            $duty_name = $hasDuty->name;
        }

        $data = [
            'id' => $employee->id,
            'company_id' => $employee->company_id,
            'job_number' => $employee->job_number,
            'name' => $employee->name,
            'account' => $employee->account,
            'duty_id' => $employee->duty_id,
            'organization_id' => $employee->organization_id,
            'phone' => $employee->phone,
            'duty_name' => $duty_name ?? '',
            'organization_name' => $organization_name ?? '',
            'sex' => $employee->sex,
            'entry_status' => $employee->entry_status,
            'entry_time' => $employee->entry_status,
            'last_login_time' => $employee->last_login_time,
        ];
        //前缀加id
        $key = config('web.user_cache_key') . $employee->id;
        make(redis::class)->set($key, Json::encode($data), ['EX' => 86400 * 2]);
        return ApiUtils::send(ErrorCode::SUCCESS, "操作成功", $data);
    }

    /**
     * 建立部门与负责人的项目关联
     *
     * @Author czm
     * @DateTime 2020-10-16
     * @param [type] $join_project_id
     * @param [type] $params
     * @return void
     */
    // public function bindProject($join_project_id, $params)
    // {
    //获取关联的部门
    // $findParentIdsWithPrincipal = make(AuthOrganizationDao::class)->findParentIdsWithPrincipal($params['organization_id']);
    // foreach ($join_project_id as $val) {
    //     //编辑或者新增员工关联项目
    //     make(AuthProjectEmployeeDao::class)->firstOrCreate(
    //         [
    //             'employee_id' => $params['employee_id'],
    //             'project_id' => $val,
    //             'type' => AuthProjectEmployee::JOIN_TYPE,
    //             'company_id' => $params['company_id'],
    //             'add_time' => time(),
    //             'update_time' => time(),
    //             'organization_id' => 0,
    //             'is_del' => 1,
    //         ], [
    //             'employee_id' => $params['employee_id'],
    //             'project_id' => $val,
    //             'type' => AuthProjectEmployee::JOIN_TYPE,
    //             'company_id' => $params['company_id'],
    //             'organization_id' => 0,
    //             'is_del' => 1,
    //         ]);
    //     //追最顶级部门与负责人的项目关联
    //     $lock = true;
    //     $principals = [];
    //     $organization_ids = make(AuthProjectOrganizationDao::class)->searchAll([
    //         ['company_id', '=', $params['company_id']],
    //         ['project_id', '=', $val],
    //     ], ['organization_id']);
    //     $organization_ids = array_column($organization_ids, 'organization_id');
    //     foreach ($findParentIdsWithPrincipal as $k => $v) {
    //         if (in_array($k, $organization_ids)) {
    //             $lock = false;
    //             break;
    //         } else {
    //             $principals[$k] = $v;
    //         }

    //     }
    //     //如果顶级公司不存在则记录绑定当前部门项目关联
    //     if ($lock) {
    //         make(AuthProjectOrganizationDao::class)->firstOrCreate([
    //             'organization_id' => $params['organization_id'],
    //             'project_id' => $val,
    //             'company_id' => $params['company_id'],
    //             'add_time' => time(),
    //         ], [
    //             'organization_id' => $params['organization_id'],
    //             'project_id' => $val,
    //             'company_id' => $params['company_id'],
    //         ]);
    //         //去除所有下级部门的项目部门关联
    //         make(AuthProjectOrganizationDao::class)->delChildProject($params['organization_id'], $val);
    //         //部门负责人绑定
    //         ($findParentIdsWithPrincipal[$params['organization_id']] ?? "") && make(AuthProjectEmployeeDao::class)->firstOrCreate([
    //             'employee_id' => $findParentIdsWithPrincipal[$params['organization_id']],
    //             'project_id' => $val,
    //             'company_id' => $params['company_id'],
    //             'type' => AuthProjectEmployee::JOIN_TYPE,
    //             'organization_id' => $params['organization_id'],
    //             'add_time' => time(),
    //             'update_time' => time(),
    //         ], [
    //             'employee_id' => $findParentIdsWithPrincipal[$params['organization_id']],
    //             'project_id' => $val,
    //             'company_id' => $params['company_id'],
    //             'type' => AuthProjectEmployee::JOIN_TYPE,
    //             'organization_id' => $params['organization_id'],
    //         ]);
    //     } else {
    //         foreach ($principals as $k => $v) {
    //             //链路上负责人绑定
    //             $v && make(AuthProjectEmployeeDao::class)->firstOrCreate([
    //                 'employee_id' => $v,
    //                 'project_id' => $val,
    //                 'company_id' => $params['company_id'],
    //                 'type' => AuthProjectEmployee::JOIN_TYPE,
    //                 'organization_id' => $k,
    //                 'add_time' => time(),
    //                 'update_time' => time(),
    //             ], [
    //                 'employee_id' => $v,
    //                 'project_id' => $val,
    //                 'company_id' => $params['company_id'],
    //                 'type' => AuthProjectEmployee::JOIN_TYPE,
    //                 'organization_id' => $k,
    //             ]);
    //         }

    //     }

    // }}

    /**
     * 修改员工状态
     *
     * @Author czm
     * @DateTime 2020-10-21
     * @param array $params
     * @return void
     */
    public function editEmployeeStauts(array $params)
    {
        $employee = make(AuthEmployeeDao::class)->findTrait($params['employee_id']);
        if (isset($params['status']) && $params['status']) {
            $employee->status = $params['status'];
        }
        if (isset($params['admin_status']) && $params['admin_status']) {
            $employee->admin_status = $params['admin_status'];
        }
        $employee->save();
        make(AuthAdminLogDao::class)->addLog('用户修改员工状态id: ' . $params['employee_id'] . "内容 " . json_encode($params, JSON_UNESCAPED_UNICODE));
        $this->userInfo(['employee_id' => $params['employee_id']]);
        return ApiUtils::send(ErrorCode::SUCCESS, "操作成功");
    }

    /**
     * 获取员工负责人信息
     *
     * @Author czm
     * @DateTime 2020-11-13
     * @param array $params
     * @return void
     */
    public function getEmployeePrincipal(array $params)
    {
        if (!isset($params['employee_id']) || !$params['employee_id']) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '员工id不能为空');
        }

        if (!isset($params['organization_id']) || !$params['organization_id']) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '部门id不能为空');
        }

        $data = make(AuthEmployeeDao::class)->findPrincipal((int) $params['employee_id'], (int) $params['organization_id']);
        return ApiUtils::send(ErrorCode::SUCCESS, "操作成功", $data);
    }

    /**
     * 生产对应数据的jwt
     *
     * @Author czm
     * @DateTime 2020-11-16
     * @param array $params
     * @return void
     */
    public function jwtToken(array $params)
    {
        $secreteKey = $params['secreteKey'];
        unset($params['secreteKey']);
        $data = JwtUtils::authorizations($params, $secreteKey);
        return ApiUtils::send(ErrorCode::SUCCESS, "操作成功", $data);

    }

    /**
     * 刷新token数据
     *
     * @Author czm
     * @DateTime 2020-11-16
     * @param array $params
     * @return void
     */
    public function refreshToken(array $params)
    {
        if (!isset($params['refresh_token']) || !$params['refresh_token']) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, 'refresh_token内容错误');
        }
        try {
            $checkJwtToken = JwtUtils::checkToken($params['refresh_token'], config('web.user_jwt_key'));
            $map = [
                'id' => $checkJwtToken['data']['employee_id'],
                'name' => $checkJwtToken['data']['name'],
                'company_id' => $checkJwtToken['data']['company_id'],
                'organization_id' => $checkJwtToken['data']['organization_id'],
                'duty_id' => $checkJwtToken['data']['duty_id'],
            ];
            $employee = make(AuthEmployeeDao::class)->firstByWhereTrait($map);
            if (!empty($employee)) {
                $last_login_time = time();
                $employee->last_login_time = $last_login_time;
                $employee->save();
                $data = JwtUtils::refreshToken($params['refresh_token'], config('web.user_jwt_key'), ['last_login_time' => $last_login_time]);
                //重置登陆缓存
                $this->userInfo(['employee_id' => $checkJwtToken['data']['employee_id']]);
                make(ModuleService::class)->userHasModule(['employee_id' => $checkJwtToken['data']['employee_id']]);
                return ApiUtils::send(ErrorCode::SUCCESS, "操作成功", $data);
            } else {
                $checkJwtToken['errCode'] = ErrorCode::ERR_BUESSUS;
                $checkJwtToken['msg'] = "用户过期 请重新登陆";
            }
            $errCode = $checkJwtToken['errCode'];
            $msg = $checkJwtToken['msg'];
        } catch (\Throwable $t) {
            $msg = $t->getMessage();
            $errCode = $t->getCode();
        }
        throw new BusinessException((int) $errCode, $msg);
    }

    public function getAllPrincipal(array $params)
    {
        if (!isset($params['employee_id']) || empty($params['employee_id'])) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '员工不存在');
        }
        $organization_ids = make(AuthOrganizationDao::class)->pluckByWhereTrait(['employee_id' => $params['employee_id']], 'id')->toArray();
        $ids = $organization_ids;
        foreach ($organization_ids as $val) {
            $ids = array_merge($ids, make(AuthOrganizationDao::class)->findBranchIds($val));
        }
        if (empty($ids)) {
            $data = [];
            goto end;
        }
        $data = make(AuthEmployeeDao::class)->pluckByWhereInTrait('organization_id', $ids, 'id')->toArray();
        end:
        return ApiUtils::send(ErrorCode::SUCCESS, "操作成功", $data);

    }

    /**
     * 员工入职
     *
     * @Author czm
     * @DateTime 2021-01-04
     * @param array $params
     * @return void
     */
    public function entryGrpc(array $params)
    {
        $requestData = [
            'company_id' => $params['company_id'],
            'organization_id' => $params['organization_id'],
            'employee_id' => $params['employee_id'],
        ];
        $client = new OAClient();
        $request = new Params();
        $request->setRequest(
            json_encode($requestData)
        );
        $request->setController("App\Grpc\Controller\EmployeeController");
        $request->setMethod("add");
        list($reply, $status) = $client->curdClient($request);
        if ($reply->getErrCode() != 0) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, "GRPC" . $reply->getMsg());
        }
    }

    /**
     * 员工离职
     *
     * @Author czm
     * @DateTime 2021-01-04
     * @param array $params
     * @return void
     */
    public function dimissionGrpc(array $params)
    {
        $requestData = [
            'company_id' => $params['company_id'],
            'employee_id' => $params['employee_id'],
        ];
        $client = new OAClient();
        $request = new Params();
        $request->setRequest(
            json_encode($requestData)
        );
        $request->setController("App\Grpc\Controller\EmployeeController");
        $request->setMethod("del");
        list($reply, $status) = $client->curdClient($request);
        if ($reply->getErrCode() != 0) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, "GRPC" . $reply->getMsg());
        }
    }
    /**
     * 员工换岗
     *
     * @Author czm
     * @DateTime 2021-01-04
     * @param array $params
     * @return void
     */
    public function relieveGuard(array $params)
    {
        $requestData = [
            'company_id' => $params['company_id'],
            'employee_id' => $params['employee_id'],
            'old_organization_id' => $params['old_organization_id'],
            'new_organization_id' => $params['new_organization_id'],
        ];

        $client = new OAClient();
        $request = new Params();
        $request->setRequest(
            json_encode($requestData)
        );
        $request->setController("App\Grpc\Controller\EmployeeController");
        $request->setMethod("change");
        list($reply, $status) = $client->curdClient($request);
        // var_dump($requestData, $reply->getMsg(), $reply->getErrCode());
        if ($reply->getErrCode() != 0) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, "GRPC" . $reply->getMsg());
        }
    }
}
