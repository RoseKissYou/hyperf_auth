<?php

declare (strict_types = 1);

namespace App\Validate;

class CompanyValidation extends BaseValidation
{
    // 调用案例 check支持自定义规则

    // make(TaskValidation::class)->check($this->request->all(),__FUNCTION__)

    //返回响应值的  ->response()

    //返回boole   ->get()

    //返回报错信息  ->getError()

    //验证规则
    protected $rule = [
        'name' => 'required',

    ];

    //自定义验证信息
    protected $message = [
        'name.required' => '名称不能为空',

    ];

    //自定义场景
    protected $scene = [
        'editComany' => ['name'],
    ];
}
