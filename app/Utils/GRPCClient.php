<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Utils;

use Hyperf\GrpcClient\BaseClient;
use Pb\Params;
use Pb\Reply;
use Hyperf\Utils\Codec\Json;
use App\Utils\ServerUtil;

class GRPCClient extends BaseClient
{
    public function __construct(string $hostname = '', array $options = [
        'credentials' => null,
        ])
    {
        if(empty($hostname)){
            $hostname = env('GRPC_HOST', '127.0.0.1').':'.env('GRPC_PORT', '9503');
        }
        parent::__construct($hostname, $options);
    }

    public function curdClient(Params $argument)
    {
        $req = $argument->getRequest();
        !empty($req) ? $argument->setRequest(ServerUtil::opensslEncrypt($req)) : '';
        list($reply, $status) = $this->_simpleRequest(
            '/pb.client/curdClient',
            $argument,
            [Reply::class, 'decode']
        );
        $data = $reply->getData();
        !empty($data) ? $reply->setData(ServerUtil::opensslDecrypt($data)) : '';
        
        return [$reply, $status];
    }
}
