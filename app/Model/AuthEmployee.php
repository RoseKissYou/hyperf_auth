<?php

declare (strict_types = 1);
namespace App\Model;

use Hyperf\Database\Model\Builder;
use Hyperf\DbConnection\Model\Model;

/**
 * 员工信息表
 *
 * @Author czm
 * @DateTime 2020-09-09
 */
class AuthEmployee extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'auth_employee';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['company_id', 'job_number', 'account', 'password', 'duty_id', 'role_id', 'admin_role_id', 'organization_id',
        'name', 'phone', 'identification', 'admin_status', 'status',
        // 'partake_project', 'check_project',
        'entry_status', 'entry_time', 'sex', 'add_time', 'update_time', 'is_del'];
    protected $guarded = ['id'];
    protected $hidden = ['is_del'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'add_time' => 'datetime:Y-m-d H:i:s',
        'update_time' => 'datetime:Y-m-d H:i:s',
    ];

    const CREATED_AT = 'add_time';
    const UPDATED_AT = 'update_time';
    protected $dateFormat = 'U';

    protected function boot(): void
    {
        $this->bootTraits();
        //model 软删除过滤 避免每次查询都添加is_del条件 去除条件添加withoutGlobalScope
        $this->addGlobalScope('is_del', function (Builder $builder) {
            $builder->where($this->table . '.is_del', 1);
        });
    }

    public function hasOrganization()
    {
        return $this->hasOne(AuthOrganization::class, 'id', 'organization_id');
    }

    public function hasRole()
    {
        return $this->hasOne(AuthRole::class, 'id', 'role_id');
    }

    public function hasDuty()
    {
        return $this->hasOne(AuthDuty::class, 'id', 'duty_id');
    }

    // public function belongsToProject()
    // {
    //     return $this->belongsToMany(AuthProject::class, 'auth_project_employee', 'employee_id', 'project_id');
    // }

    // public function HasManyProject()
    // {
    //     return $this->hasManyThrough(AuthProject::class, AuthProjectEmployee::class, 'employee_id', 'id', 'id', 'project_id');
    // }

    const EMPLOYEE_SEX_MAN = 1; //男
    const EMPLOYEE_SEX_WOMAN = 2; //女

    const PARTAKE_PROJECT_ALL = 2; //参与项目:  全部
    const PARTAKE_PROJECT_PART = 1; //参与项目: 选择

    const CHECK_PROJECT_ALL = 2; //查看项目:  全部
    const CHECK_PROJECT_PART = 1; //查看项目: 选择

    const STATUS_NORMAL = 1; //账号正常
    const STATUS_STOP = 2; //账号停用

    const ADMIN_STATUS_NORMAL = 1; //后台登陆权限正常
    const ADMIN_STATUS_STOP = 2; //后台登陆权停用

    const NO_IS_PRINCIPAL = 1; //不是负责人
    const YES_IS_PRINCIPAL = 2; //是负责人

    const ENTRY_STATUS_ING = 1; //在职
    const ENTRY_STATUS_ED = 2; //离职

    const REDIS_EMPLOYEE_LOGIN_LOCK = "auth_employee_login_logck_"; //登陆锁前缀

    const REDIS_EMPLOYEE_LOGIN_LOCK_NUM = 8; //密码验证登陆锁次数

    const REDIS_EMPLOYEE_INFO = "auth_employee_info_"; //登陆锁前缀

}
