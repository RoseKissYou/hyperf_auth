<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

namespace App\Validate;

class ModuleValidation extends BaseValidation
{
    // 调用案例 check支持自定义规则

    // make(ModuleValidation::class)->check($this->request->all(),__FUNCTION__)

    //返回响应值的  ->response()

    //返回boole   ->get()

    //返回报错信息  ->getError()

    //验证规则
    protected $rule = [
        'id' => 'required',
        'appkey' => 'required',
        'url' => 'required',
        'module_id' => 'required',
        'component_id' => 'required',
        'component_name' => 'required',
        'org_id' => 'required',
        'cover_next' => 'required',
        'appkey' => 'required',
        'icon' => 'required',
        'content' => 'required',
        'is_web' => 'required',
        'pid' => 'required',
        'identity' => 'required'
    ];

    //自定义验证信息
    protected $message = [
        'id.required' => '参数不对',
        'appkey.required' => '交互密匙必传',
        'url.required' => '前端地址必填',
        'module_id.required' => '模块必传',
        'component_id.required' => '功能必传',
        'component_name.required' => '功能名字必传',
        'org_id.required' => '部门必传',
        'cover_next.required' => '是否覆盖下级',
        'appkey.required' => '密匙必填',
        'icon.required' => '图标必填',
        'content.required' => '配置内容必填',
        'is_web.required' => '左侧边栏是否显示',
        'pid.required' => '上级必填',
        'identity.required' => '接口标识必填'
    ];

    //自定义场景
    protected $scene = [
        'departAddComponent' => 'component_id,org_id,cover_next',
        'departHasComponent' => 'org_id',
        'EnableModule' => 'id,appkey',
        'addComponent' => 'component_name,module_id,identity,is_web,pid',
        'editComponent' => 'id,component_name,module_id,identity,is_web,pid',
        'delComponent' => 'id',
        
    ];
}
