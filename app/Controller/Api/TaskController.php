<?php

declare (strict_types = 1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Controller\Api;

use App\Constants\ErrorCode;
use App\Controller\AbstractController;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Context;
use App\Service\Api\TaskService;
use Hyperf\Utils\Codec\Json;

class TaskController extends AbstractController
{
    /**
     * @Inject
     * @var TaskService
     */
    protected $service;

    public function list()
    {
        $params = $this->request->all();
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->list($params);
        return $this->success($result['msg'],$result['data']);
        
    }

   
    
}
