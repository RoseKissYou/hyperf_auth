<?php

declare (strict_types = 1);

namespace App\Validate;


class PermissionValidation extends BaseValidation
{
    // 调用案例 check支持自定义规则

    // make(TaskValidation::class)->check($this->request->all(),__FUNCTION__)

    //返回响应值的  ->response()

    //返回boole   ->get()

    //返回报错信息  ->getError()

    //验证规则
    protected $rule = [
        'identity'   => 'required',
        'name'       => 'required|max:15',
        'sort_order' => 'required|integer',
        'parent_id'  => 'required|integer',
        'url'        => 'required',
        'id'         => 'required|integer',
        'is_web'     => 'required|integer'
    ];

    //自定义验证信息
    protected $message = [
        'name.required'      => '名字 必须',
        'name.max'           => '权限名字不能超过15个字符',
        'identity.required'  => '权限标识 必须',
        'url.required'       => '权限地址 必须',
        'id.required'        => 'ID 必须',
        'sort_order.required'=> '排序值 必须',
        'parent_id.required' => '父类ID,顶级填0 必须',
        'is_web.required'    => '是否是web页面值 必须'
    ];

    //自定义场景
    protected $scene = [
        'permissionAdd' => ['name','identity','url','sort_order','parent_id','is_web'],
        'permissionEdit'=>['id','identity','name','sort_order','parent_id','url'.'is_web'],
        'permissionGet' => ['id'],
    ];
}
