<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Dao;

use App\Model\CompanyModule;

class CompanyModuleDao
{
    //添加数据
    public function add(array $data)
    {
        return CompanyModule::query()->insertGetId($data);
    }

    //添加数据多条
    public function insert(array $data)
    {
        return CompanyModule::query()->insert($data);
    }

    //更新
    public function update($where, $data)
    {
        return CompanyModule::query()->where($where)->update($data);
    }

    //删除
    public function delete($where, $whereIn = [])
    {
        return CompanyModule::query()->where($where)->when($whereIn, function ($query, $whereIn) {
            return $query->whereIn('module_id', $whereIn);
        })->delete();
    }


    //通过条件搜索单一条
    public function searchFirst($where, $columns = ['*'])
    {
        return CompanyModule::query()->where($where)->first($columns);
    }

    public function searchAll($where = [], $whereIn = [], $columns = ['*'])
    {
        return CompanyModule::query()->where($where)->when($whereIn, function ($query, $whereIn) {
            return $query->whereIn('module_id', $whereIn);
        })->get($columns)->toArray();
    }
    //找出模块id
    public function searchMoudleId($where = [], $whereIn = [])
    {
        return CompanyModule::query()->where($where)->when($whereIn, function ($query, $whereIn) {
            return $query->whereIn('module_id', $whereIn);
        })->pluck('module_id')->toArray();
    }

}
