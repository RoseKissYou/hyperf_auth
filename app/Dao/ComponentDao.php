<?php

declare (strict_types = 1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Dao;

use App\Model\Component;

class ComponentDao
{
    //添加数据
    public function add(array $data)
    {
        $data['add_time'] = time();
        return Component::query()->insertGetId($data);
    }

    //添加数据多条
    public function insert(array $data)
    {
        return Component::query()->insert($data);
    }

    //删除
    public function delete($where, $whereIn = [])
    {
        return Component::query()->where($where)->when($whereIn, function ($query, $whereIn) {
            return $query->whereIn('id', $whereIn);
        })->delete();
    }

    //更新
    public function update($where, $data)
    {
        return Component::query()->where($where)->update($data);
    }

    //通过条件搜索单一条
    public function searchFirst($where, $columns = ['*'])
    {
        return Component::query()->where($where)->first($columns);
    }

    //找出主键id
    public function searchAllId($where = [], $whereIn = [])
    {
        return Component::query()->where($where)->when($whereIn, function ($query, $whereIn) {
            return $query->whereIn('pid', $whereIn);
        })->pluck('id')->toArray();
    }

    public function searchAll($where = [], $whereIn = [], $columns = ['*'])
    {
        return Component::query()->where($where)->when($whereIn, function ($query, $whereIn) {
            return $query->whereIn('id', $whereIn);
        })->orderBy('sort', 'desc')->orderBy('id', 'desc')->get($columns)->toArray();
    }

    //分页
    public function pageByWhere($where = [], $pageSize)
    {
        return Component::query()->with('module')->where($where)->orderBy('sort', 'desc')->orderBy('id', 'desc')->paginate(intval($pageSize), ['*'], 'page')->toArray();
    }

    /**
     * 根据条件获取对应数据
     * @Author czm
     * @DateTime 2020-05-26
     * @param array $map
     * @return void
     */
    public function getByWhereTrait($map = [], $field = ["*"])
    {
        return Component::query()->select($field)->when(!empty($map), function ($query) use ($map) {
            $query->where($map);
        })->orderBy('sort', 'desc')->orderBy('id', 'desc')->get();
    }

    /**
     * 根据条件查询pluck数据
     * @Author czm
     * @DateTime 2020-05-28
     * @return void
     */
    public function getByWhereInGroup($map)
    {
        $data = Component::query()->select('module_id', 'identity')->whereIn("id", $map)->get();
        if (!$data) {
            return [];
        }
        $data = $data->groupBy('module_id')->toArray();

        foreach ($data as $key => $val) {
            $val = array_column($val, 'identity');
            $val = array_filter($val);
            if (!empty($val)) {
                $val = implode(",", $val);
                $val = explode(",", $val);
            }
            $data[$key] = $val;
        }
        return $data;
    }
}
