<?php

declare(strict_types=1);

namespace App\Model;

use Hyperf\DbConnection\Model\Model;

class Upload extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'auth_upload';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['md5','content_type','chunk_size','origin_name','add_time','file_path','user_name','user_id'];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'add_time' => 'datetime:Y-m-d H:i:s',
    ];

    protected $dateFormat = false;

    public function getFilePathAttribute($value)
    {
        return env('WEB_HOST', '127.0.0.1:9501').'/'.$value;
    }
}
