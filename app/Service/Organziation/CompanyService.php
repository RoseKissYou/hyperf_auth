<?php

declare (strict_types = 1);

namespace App\Service\Organziation;

use App\Constants\ErrorCode;
use App\Dao\AuthAdminLogDao;
use App\Dao\AuthCompanyDao;
use App\Dao\AuthEmployeeDao;
use App\Dao\AuthOrganizationDao;
use App\Exception\BusinessException;
use App\Utils\ApiUtils;
use Hyperf\DbConnection\Db;

class CompanyService
{
    /**
     * 根据公司id获取员工数量与公司名称
     *
     * @Author czm
     * @DateTime 2020-09-29
     * @param [type] $params
     * @return void
     */
    public function info($params)
    {
        $company = make(AuthCompanyDao::class)->findTrait($params['company_id']);
        $company_name = $company->name;
        $employee_num = make(AuthEmployeeDao::class)->countByWhereTrait(['company_id' => $params['company_id']]);
        $employee = make(AuthEmployeeDao::class)->findTrait($params['employee_id']);

        $employee_account = $employee->account;
        $employee_name = $employee->name;
        return ApiUtils::send(ErrorCode::SUCCESS, '操作成功', compact('employee_num', 'company_name', 'employee_account', 'employee_name'));

    }
    /**
     * 编辑公司信息
     *
     * @Author czm
     * @DateTime 2020-09-29
     * @param [type] $params
     * @return void
     */
    public function editComany($params)
    {
        $company = make(AuthCompanyDao::class)->findTrait($params['company_id']);
        //改名要同步顶级部门名称
        Db::beginTransaction();
        try {
            if ($company->name != $params['name']) {
                $company->name = $params['name'];
                $organiztion = make(AuthOrganizationDao::class)->firstByWhereTrait([
                    ['pid', '=', 0],
                    ['company_id', '=', $params['company_id']],
                ]);
                //修正后缀
                $map = [
                    ['prefix_name', 'like', "%{$organiztion->name}-%"],
                    ['company_id', '=', $organiztion->company_id],
                ];
                $update_organiztion = make(AuthOrganizationDao::class)->getByWhereTrait($map);
                foreach ($update_organiztion as $val) {
                    if ($organiztion->id != $val['id']) {
                        $val['prefix_name'] = str_replace($organiztion->name . "-", $params['name'] . "-", $val['prefix_name']);
                        make(AuthOrganizationDao::class)->updateByIdTrait(['id' => $val['id'], 'prefix_name' => $val['prefix_name']]);
                    }
                }
                $organiztion->name = $params['name'];
                $organiztion->prefix_name = $params['name'] . "-";
                $organiztion->save();
            }
            $company->save();
            Db::commit();
        } catch (\Throwable $t) {
            Db::rollBack();
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '网络异常 数据写入失败' . config('web.err_msg_splite') . $t->getMessage());
        }
        make(AuthAdminLogDao::class)->addLog('编辑公司信息' . json_encode($params, JSON_UNESCAPED_UNICODE));
        return ApiUtils::send(ErrorCode::SUCCESS, '操作成功');
    }
}
