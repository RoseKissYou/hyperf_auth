<?php

declare (strict_types = 1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Dao;

use App\Model\Task;

class TaskDao
{
    //添加数据
    public function add(array $data)
    {
        $data['add_time'] = time();
        return Task::query()->insertGetId($data);
    }

    //添加数据多条
    public function insert(array $data)
    {
        return Task::query()->insert($data);
    }

    //更新
    public function update($where, $data)
    {
        return Task::query()->where($where)->update($data);
    }

    //删除
    public function delete($where)
    {
        return Task::query()->where($where)->delete();
    }

    //通过条件搜索单一条
    public function searchFirst($where, $columns = ['*'])
    {
        return Task::query()->where($where)->first($columns);
    }

    public function searchAll($where = [], $columns = ['*'])
    {
        return Task::query()->where($where)->orderBy('id', 'asc')->get($columns)->toArray();
    }

    public function getListPage($where = [], $page = 1, $pageSize = 15, $columns = ['*'])
    {
        return AuthProject::query()->where($where)->orderBy('id', 'desc')->paginate(intval($pageSize), $columns, 'page', intval($page))->toArray();

    }

    

}
