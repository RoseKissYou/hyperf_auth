<?php

declare (strict_types = 1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Dao;

use App\Model\TaskFail;

class TaskFailDao
{
    //添加数据
    public function add(array $data)
    {
        $data['add_time'] = time();
        return TaskFail::query()->insertGetId($data);
    }

    //添加数据多条
    public function insert(array $data)
    {
        return TaskFail::query()->insert($data);
    }

    //更新
    public function update($where, $data)
    {
        return TaskFail::query()->where($where)->update($data);
    }

    //删除
    public function delete($where)
    {
        return TaskFail::query()->where($where)->delete();
    }

    //通过条件搜索单一条
    public function searchFirst($where, $columns = ['*'])
    {
        return TaskFail::query()->where($where)->first($columns);
    }

    public function searchAll($where = [], $columns = ['*'])
    {
        return TaskFail::query()->where($where)->orderBy('id', 'asc')->get($columns)->toArray();
    }

   

}
