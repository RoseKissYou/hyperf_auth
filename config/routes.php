<?php

declare (strict_types = 1);

/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

use Hyperf\HttpServer\Router\Router;

Router::addRoute(['GET', 'POST', 'HEAD'], '/', 'App\Controller\IndexController@index');

//健康检查路由
Router::addRoute(['GET', 'POST', 'HEAD'], '/health', 'App\Controller\HealthController@health');

Router::addServer('grpc', function () {
    Router::addGroup('/pb.client', function () {
        Router::post('/curdClient', 'App\Controller\Grpc\GrpcController@curdClient');
    });
});

Router::addGroup(
    '/auth',
    function () {
        Router::addRoute(['POST'], '/task_log', 'App\Controller\Api\TaskController@list'); //任务日志列表


        Router::addRoute(['POST'], '/upload', 'App\Controller\Api\UploadController@upload'); //文件上传

        Router::addRoute(['POST'], '/user_module', 'App\Controller\Auth\ModuleController@userHasModule'); //找出用户权限

        Router::addRoute(['POST'], '/acomponent_info', 'App\Controller\Auth\ModuleController@componentInfo'); //功能信息
        Router::addRoute(['POST'], '/add_component', 'App\Controller\Auth\ModuleController@addComponent'); //添加功能
        Router::addRoute(['POST'], '/edit_component', 'App\Controller\Auth\ModuleController@editComponent'); //编辑功能
        Router::addRoute(['POST'], '/del_component', 'App\Controller\Auth\ModuleController@delComponent'); //删除功能
        Router::addRoute(['POST'], '/component_cate', 'App\Controller\Auth\ModuleController@componentCate'); //功能类型
        Router::addRoute(['POST'], '/module_enable', 'App\Controller\Auth\ModuleController@EnableModule'); //开启模块功能
        Router::addRoute(['POST'], '/module_list', 'App\Controller\Auth\ModuleController@moduleList'); //功能列表
        Router::addRoute(['POST'], '/top_module_list', 'App\Controller\Auth\ModuleController@topModuleList'); //一层数据功能列表
        Router::addRoute(['POST'], '/company_module_list', 'App\Controller\Auth\ModuleController@companyModuleList'); //公司开启模块列表
        Router::addRoute(['POST'], '/depart_add_component', 'App\Controller\Auth\ModuleController@departAddComponent'); //部门添加功能
        Router::addRoute(['POST'], '/depart_has_component', 'App\Controller\Auth\ModuleController@departHasComponent'); //部门已选功能
        Router::addRoute(['POST'], '/menu_add', 'App\Controller\Auth\MenuController@add'); //添加栏目
        Router::addRoute(['POST'], '/menu_edit', 'App\Controller\Auth\MenuController@edit'); //编辑栏目
        Router::addRoute(['POST'], '/menu_del', 'App\Controller\Auth\MenuController@del'); //删除栏目
        Router::addRoute(['POST'], '/menu_list', 'App\Controller\Auth\MenuController@menuList'); //栏目列表
        Router::addRoute(['POST'], '/menu_all', 'App\Controller\Auth\MenuController@menuAll'); //所有栏目
        Router::addRoute(['POST'], '/move', 'App\Controller\Auth\MenuController@move'); //栏目移动
        Router::addRoute(['POST'], '/menu_add_component', 'App\Controller\Auth\MenuController@menuAddComponent'); //栏目添加功能
        Router::addRoute(['POST'], '/menu_has_component', 'App\Controller\Auth\MenuController@menuHasComponent'); //栏目已选的功能
        Router::addRoute(['POST'], '/menu_del_component', 'App\Controller\Auth\MenuController@menuDelComponent'); //删除栏目功能
        Router::addRoute(['POST'], '/menu_move_component', 'App\Controller\Auth\MenuController@menuMoveComponent'); //栏目移动功能
        Router::addRoute(['POST'], '/move_menu_component', 'App\Controller\Auth\MenuController@moveMenuComponent'); //移动栏目功能
        Router::addRoute(['POST'], '/component_config_list', 'App\Controller\Auth\ModuleController@componentConfigList'); //功能配置列表

        Router::addRoute(['POST'], '/menu', 'App\Controller\Rbac\PermissionController@menu'); //权限列表
        Router::addRoute(['POST'], '/permission_all', 'App\Controller\Rbac\PermissionController@permissionList'); //所有权限列表
        Router::addRoute(['GET', 'POST'], '/permission_add', 'App\Controller\Rbac\PermissionController@permissionAdd');
        Router::addRoute(['GET', 'POST'], '/permission_edit', 'App\Controller\Rbac\PermissionController@permissionEdit');
        Router::addRoute(['GET', 'POST'], '/permission_del', 'App\Controller\Rbac\PermissionController@permissionDel');
        Router::addRoute(['GET', 'POST'], '/role_list', 'App\Controller\Rbac\RoleController@roleList'); //角色列表分页显示
        Router::addRoute(['GET', 'POST'], '/role_all', 'App\Controller\Rbac\RoleController@roleAll'); //所有角色
        Router::addRoute(['GET', 'POST'], '/role_add', 'App\Controller\Rbac\RoleController@roleAdd'); // 添加角色
        Router::addRoute(['GET', 'POST'], '/role_edit', 'App\Controller\Rbac\RoleController@roleEdit'); //编辑角色
        Router::addRoute(['GET', 'POST'], '/role_del', 'App\Controller\Rbac\RoleController@roleDel'); //删除角色
        Router::addRoute(['GET', 'POST'], '/rpc_role', 'App\Controller\Rbac\RoleController@rpcRole'); //删除角色
        //项目相关
        //        Router::addRoute(['POST'], '/project_add', 'App\Controller\Auth\ProjectController@add'); //添加项目
        //        Router::addRoute(['POST'], '/project_edit', 'App\Controller\Auth\ProjectController@edit'); //编辑项目
        //        Router::addRoute(['GET'], '/project_list', 'App\Controller\Auth\ProjectController@list'); //项目列表
        //        Router::addRoute(['GET'], '/project_info', 'App\Controller\Auth\ProjectController@info'); //项目详情
        //        Router::addRoute(['POST'], '/project_editType', 'App\Controller\Auth\ProjectController@editType'); //编辑项目状态
        //        Router::addRoute(['GET'], '/project_employeeList', 'App\Controller\Auth\ProjectController@employeeList'); //项目员工列表
        //        Router::addRoute(['GET'], '/project_employeeAll', 'App\Controller\Auth\ProjectController@employeeAll'); //项目全部员工ID
        //        Router::addRoute(['GET'], '/project_organization', 'App\Controller\Auth\ProjectController@organization'); //获取项目组织架构
        //        Router::addRoute(['POST'], '/project_employeeEdit', 'App\Controller\Auth\ProjectController@employeeEdit'); //编辑项目员工
        //        Router::addRoute(['GET'], '/project_employeePermission', 'App\Controller\Auth\ProjectController@employeePermission'); //员工项目权限列表
        //        Router::addRoute(['GET'], '/project_employeeEditPrincipal', 'App\Controller\Auth\ProjectController@employeeEditPrincipal'); //编辑项目员工（部门负责人）-

        Router::addGroup('/organization', function () {
            Router::addRoute(['GET'], '/company', 'App\Controller\Organization\CompanyController@index'); //公司
            Router::addRoute(['POST'], '/company_edit', 'App\Controller\Organization\CompanyController@editComany'); //编辑公司

            Router::addRoute(['POST'], '/employee', 'App\Controller\Organization\EmployeeController@index'); //员工列表
            Router::addRoute(['GET'], '/employee_list', 'App\Controller\Organization\EmployeeController@list'); //员工列表
            Router::addRoute(['POST'], '/employee_add', 'App\Controller\Organization\EmployeeController@addEmployee'); //添加员工
            Router::addRoute(['POST'], '/employee_edit', 'App\Controller\Organization\EmployeeController@editEmployee'); //编辑员工
            Router::addRoute(['POST'], '/employee_password_edit', 'App\Controller\Organization\EmployeeController@editPassword'); //编辑员工
            Router::addRoute(['POST'], '/employee_edit_status', 'App\Controller\Organization\EmployeeController@editEmployeeStauts'); //编辑员工状态

            Router::addRoute(['GET'], '/index', 'App\Controller\Organization\IndexController@index'); //组织架构
            Router::addRoute(['GET'], '/tree_list', 'App\Controller\Organization\IndexController@treeList'); //组织架构树结构信息
            Router::addRoute(['GET'], '/employee_organization', 'App\Controller\Organization\IndexController@employee'); //组织架构与员工
            // Router::addRoute(['POST'], '/project', 'App\Controller\Organization\IndexController@project'); //组织项目
            Router::addRoute(['GET'], '/list', 'App\Controller\Organization\IndexController@list'); //组织架构列表
            Router::addRoute(['POST'], '/add', 'App\Controller\Organization\IndexController@addOrganization'); //组织架构新增
            Router::addRoute(['POST'], '/edit', 'App\Controller\Organization\IndexController@editOrganization'); //组织架构编辑
            Router::addRoute(['POST'], '/del', 'App\Controller\Organization\IndexController@delOrganization'); //组织架构删除
            Router::addRoute(['POST'], '/sort', 'App\Controller\Organization\IndexController@sort'); //组织架构排序

            Router::addRoute(['POST'], '/duty', 'App\Controller\Organization\DutyController@index'); //职位
            Router::addRoute(['POST'], '/duty_list', 'App\Controller\Organization\DutyController@list'); //列表
            Router::addRoute(['POST'], '/duty_add', 'App\Controller\Organization\DutyController@addDuty'); //职位新增
            Router::addRoute(['POST'], '/duty_edit', 'App\Controller\Organization\DutyController@editDuty'); //职位编辑
            Router::addRoute(['POST'], '/duty_del', 'App\Controller\Organization\DutyController@delDuty'); //职位删除

            Router::addRoute(['POST'], '/role', 'App\Controller\Organization\RoleController@index'); //角色列表
            Router::addRoute(['POST'], '/role_component', 'App\Controller\Organization\RoleController@roleComponent'); //角色模块
            Router::addRoute(['POST'], '/role_add', 'App\Controller\Organization\RoleController@add'); //新增角色
            Router::addRoute(['POST'], '/role_edit', 'App\Controller\Organization\RoleController@edit'); //角色编辑
            Router::addRoute(['POST'], '/role_del', 'App\Controller\Organization\RoleController@del'); //角色删除
            Router::addRoute(['POST'], '/role_set', 'App\Controller\Organization\RoleController@set'); //角色配置

        });
    },
    [
        'middleware' => [
            App\Middleware\Auth\JwtTokenMiddleware::class,
//            App\Middleware\Auth\RoleMiddleware::class  // 权限验证
        ],
    ]
);

Router::addGroup('/auth', function () {
    // 不需要校验后台token
    Router::addRoute(['get'], '/login_verify', 'App\Controller\Rbac\LoginController@verify'); // 获取验证码
    Router::addRoute(['POST'], '/login', 'App\Controller\Rbac\LoginController@login'); //后台登录

    Router::addRoute(['POST'], '/organization/login', 'App\Controller\Organization\EmployeeController@login'); //后台登录
    Router::addRoute(['POST'], '/organization/refresh_token', 'App\Controller\Organization\EmployeeController@refreshToken'); //刷新token

    // 授权激活
    Router::addRoute(['POST'], '/auth_check', 'App\Controller\Rbac\LoginController@auth'); //授权码检查
    Router::addRoute(['POST'], '/new_info', 'App\Controller\Rbac\LoginController@account'); //授权后设置信息
});
