<?php

declare (strict_types = 1);

namespace App\Validate;

use App\Constants\Regex;

class LoginValidation extends BaseValidation
{
    // 调用案例 check支持自定义规则

    // make(TaskValidation::class)->check($this->request->all(),__FUNCTION__)

    //返回响应值的  ->response()

    //返回boole   ->get()

    //返回报错信息  ->getError()

    //验证规则
    protected $rule = [
        'account' => 'required|regex:/^\w{3,32}$/',
        'password' => 'required|regex:/^\w{6,32}$/',
        'company' => 'required',
        'phone' => 'required|regex:' . Regex::PHONE,
        'verification' => 'required',
        'key' => 'required',
        'content' => 'required',
        'code' => 'required'
    ];

    //自定义验证信息
    protected $message = [
        'account.required' => '账号不能为空',
        'account.regex' => '账号长度3,32位之间的数字与字母组合',
        'password.required' => '密码不能为空',
        'password.regex' => '密码长度6,32位之间',
        'company.required' => '企业名称不能为空',
        'phone.required' => '电话不能为空',
        'phone.regex' => Regex::PHONE_MESSAGE,
        'verification.required' => '验证文案不能为空',
        'key.required' => 'key不能为空',
        'content.required' => '授权内容不能为空',
        'code.required' => '验证码不能为空',
        'auth.required' => '授权码不能为空'
    ];

    //自定义场景
    protected $scene = [
        'account' => ['company','account','password','phone','verification','key','content'],
        'auth' => ['auth'],
        'login' => ['name','password','key','code'],
    ];
}
