<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

namespace App\Validate;

class MenuDepartValidation extends BaseValidation
{
    // 调用案例 check支持自定义规则

    // make(MenuDepartValidation::class)->check($this->request->all(),__FUNCTION__)

    //返回响应值的  ->response()

    //返回boole   ->get()

    //返回报错信息  ->getError()

    //验证规则
    protected $rule = [
        'id' => 'required',
        'modue_id' => 'required',
        'org_id' => 'required',
        'org_id' => 'required',
    ];

    //自定义验证信息
    protected $message = [
        'id.required' => '任务参数不对',
        'appkey.required' => '交互密匙必传',
        'url.required' => '交互地址必传',
    ];

    //自定义场景
    protected $scene = [
        'add' => 'appkey,url',
        'taskDetail' => 'id',
        'editTask' => 'id',
    ];
}
