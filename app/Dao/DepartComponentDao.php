<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Dao;

use App\Model\DepartComponent;

class DepartComponentDao
{
    //添加数据
    public function add(array $data)
    {
        return DepartComponent::query()->insertGetId($data);
    }

    //添加数据多条
    public function insert(array $data)
    {
        return DepartComponent::query()->insert($data);
    }

    //更新
    public function update($where, $data)
    {
        return DepartComponent::query()->where($where)->update($data);
    }

    //删除
    public function delete($where, $whereIn = [])
    {
        return DepartComponent::query()->where($where)->when($whereIn, function ($query, $whereIn) {
            return $query->whereIn('component_id', $whereIn);
        })->delete();
    }


    //通过条件搜索单一条
    public function searchFirst($where, $columns = ['*'])
    {
        return DepartComponent::query()->where($where)->first($columns);
    }

    //in 
    public function searchAll($where = [], $whereIn = [], $columns = ['*'])
    {
        return DepartComponent::query()->where($where)->when($whereIn, function ($query, $whereIn) {
            return $query->whereIn('component_id', $whereIn);
        })->get($columns)->toArray();
    }

    //找出部门id
    public function searchOriId($where = [], $whereIn = [])
    {
        return DepartComponent::query()->where($where)->when($whereIn, function ($query, $whereIn) {
            return $query->whereIn('component_id', $whereIn);
        })->pluck('org_id')->toArray();
    }

    //找出功能id
    public function searchComponentId($where = [])
    {
        return DepartComponent::query()->where($where)->pluck('component_id')->toArray();
    }

    
}
