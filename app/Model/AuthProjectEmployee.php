<?php

declare (strict_types = 1);
namespace App\Model;

/**
 * @property int $id
 * @property int $company_id 公司id
 * @property int $project_id 项目id
 * @property int $employee_id 员工id
 * @property int $type 关联类型：1查看 2参与
 * @property datetime:Y-m-d H:i:s $add_time
 * @property datetime:Y-m-d H:i:s $update_time
 * @property int $is_del 1正常、2删除
 */
class AuthProjectEmployee extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'auth_project_employee';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'company_id', 'project_id', 'employee_id', 'type', 'organization_id', 'add_time', 'update_time', 'is_del'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'int', 'project_id' => 'integer', 'employee_id' => 'integer', 'type' => 'integer', 'add_time' => 'datetime:Y-m-d H:i:s', 'entry_time' => 'datetime:Y-m-d H:i:s', 'update_time' => 'datetime:Y-m-d H:i:s', 'is_del' => 'integer', 'company_id' => 'integer'];
    const SHOW_TYPE = 1;
    //查看类型
    const JOIN_TYPE = 2;
    //参与类型
    const NOT_DEL = 1;
    //正常
    const DEL = 2;
    //删除
}
