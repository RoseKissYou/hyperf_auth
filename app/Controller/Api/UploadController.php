<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Constants\ErrorCode;
use App\Controller\AbstractController;
use App\Exception\BusinessException;
use Hyperf\Di\Annotation\Inject;
use App\Service\Api\UploadService;
use Hyperf\Utils\Context;

class UploadController extends AbstractController
{

    /**
     * @Inject
     * @var UploadService
     */
    protected $service;

    /**
     * 上传图片通用接口
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function upload()
    {
        if (!$this->request->hasFile('file')) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '文件不能为空');
        }
        $user = Context::get('jwt_token');
        $file = $this->request->file('file');
        $result = $this->service->upload($file, $user);
        return $this->success('上传成功', $result);
    }
}
