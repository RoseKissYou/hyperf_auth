<?php


namespace App\Utils;

use Hyperf\Redis\RedisFactory;
use Hyperf\Utils\ApplicationContext;

class Redis
{
    /**
     * @var
     */
    protected $redis;

    public function __construct($redis_db = 'default')
    {
        $container = ApplicationContext::getContainer();
        $this->redis = $container->get(RedisFactory::class)->get($redis_db);
    }

    /**
     * 获取某个key的值
     * @param $key
     * @return bool|mixed|string
     */
    public function get($key)
    {
        return $this->redis->get($key);
    }

    /**
     * 设置值
     * @param $key
     * @param $value
     * @param null $timeout
     * @return bool
     */
    public function set($key, $value, $option = [])
    {
        return $this->redis->set($key, $value, $option);
    }

    /**
     * 自增
     * @param $key
     * @param int $incr
     * @return int
     */
    public function incr($key, int $incr=1)
    {
        return intval($this->redis->incrBy($key, $incr));
    }

    /**
     * 自减
     * @param $key
     * @param int $decr
     * @return int
     */
    public function decr($key, int $decr=1)
    {
        return intval($this->redis->decrBy($key, $decr));
    }

    /**
     * 从左边写入队列
     * @param $key
     * @param $val
     * @return bool|int
     */
    public function lPush($key, $val)
    {
        return $this->redis->lPush($key, $val);
    }

    /**
     * 返回并删除列表的第一个元素
     * @param $key
     * @return bool|mixed
     */
    public function lPop($key)
    {
        return $this->redis->lPop($key);
    }

    /**
     * 以hash方式存储值，key存在返回false
     * @param $key
     * @param $hashKey
     * @param $value
     * @return bool|int
     */
    public function hSet($key, $hashKey, $value)
    {
        return $this->redis->hSet($key, $hashKey, $value);
    }

    /**
     * 获取hash中存储的值，hash表或密钥不存在返回false
     * @param $key
     * @param $hashKey
     * @return string
     */
    public function hGet($key, $hashKey)
    {
        return $this->redis->hGet($key, $hashKey);
    }

    /**
     * 无序集合
     * @param $key
     * @param mixed ...$val
     * @return bool|int
     */
    public function sAdd($key, ...$val)
    {
        return $this->redis->sAdd($key, ...$val);
    }

    /**
     * 获取集合数据
     * @param $key
     * @return array
     */
    public function sMembers($key)
    {
        return $this->redis->sMembers($key);
    }

    /**
     * 删除某个key
     * @param $key
     * @return int
     */
    public function del($key)
    {
        return $this->redis->del($key);
    }
}
