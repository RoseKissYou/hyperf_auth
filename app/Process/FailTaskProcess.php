<?php
declare(strict_types=1);

namespace App\Process;

use Hyperf\Process\AbstractProcess;
use Hyperf\Process\Annotation\Process;
use Hyperf\DbConnection\Db;
use App\Service\Api\TaskService;
use App\Dao\TaskFailDao;
use Hyperf\Utils\ApplicationContext;


/**
 * @Process(name="fail_task_process")
 */
class FailTaskProcess extends AbstractProcess
{
    public function handle(): void
    {
        $obj = ApplicationContext::getContainer()->get(TaskService::class);
        while (true) {
            try{
                $where = [];
                $where[] = ['num', '<', config('web.fail_num')];
                $task = make(TaskFailDao::class)->searchFirst($where);
                if(!empty($task)){
                    $obj->execTask($task->toArray());
                }else{
                    sleep(3);
                }
            }catch(\Throwable $throwable){
                sleep(3);
            }
            
        }
        
    }
}