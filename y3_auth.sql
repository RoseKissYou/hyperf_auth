/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : y3_auth

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 17/03/2021 16:49:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for y3_auth_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_admin_log`;
CREATE TABLE `y3_auth_admin_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `admin_id` int NOT NULL DEFAULT '0' COMMENT '管理员id',
  `operate` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '操作信息',
  `add_time` int NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of y3_auth_admin_log
-- ----------------------------
BEGIN;
INSERT INTO `y3_auth_admin_log` VALUES (1, 1, '开启了模块功能{\"id\":\"1\",\"appkey\":\"123\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602639333);
INSERT INTO `y3_auth_admin_log` VALUES (2, 1, '开启了模块功能{\"id\":\"2\",\"appkey\":\"123\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602639357);
INSERT INTO `y3_auth_admin_log` VALUES (3, 1, '添加了栏目{\"menu_name\":\"\\u4e2a\\u4eba\\u4e2d\\u5fc3\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602639649);
INSERT INTO `y3_auth_admin_log` VALUES (4, 1, '编辑了栏目{\"id\":\"4\",\"menu_name\":\"\\u4e2a\\u4eba\\u4e2d\\u5fc3\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602640300);
INSERT INTO `y3_auth_admin_log` VALUES (5, 1, '编辑了栏目{\"id\":\"4\",\"menu_name\":\"\\u4e2a\\u4eba\\u4e2d\\u5fc311\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602640306);
INSERT INTO `y3_auth_admin_log` VALUES (6, 1, '删除了栏目{\"id\":\"4\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602640499);
INSERT INTO `y3_auth_admin_log` VALUES (7, 1, '栏目添加了功能{\"component_id\":\"109,110\",\"menu_id\":\"1\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602641451);
INSERT INTO `y3_auth_admin_log` VALUES (8, 1, '栏目添加了功能{\"component_id\":\"109,111\",\"menu_id\":\"1\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602641913);
INSERT INTO `y3_auth_admin_log` VALUES (9, 1, '栏目添加了功能{\"component_id\":\"109,110\",\"menu_id\":\"1\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602641966);
INSERT INTO `y3_auth_admin_log` VALUES (10, 1, '栏目添加了功能{\"component_id\":\"109,111\",\"menu_id\":\"1\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602642033);
INSERT INTO `y3_auth_admin_log` VALUES (11, 1, '栏目添加了功能{\"component_id\":\"109,111\",\"menu_id\":\"1\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602642084);
INSERT INTO `y3_auth_admin_log` VALUES (12, 1, '栏目添加了功能{\"component_id\":\"109,111\",\"menu_id\":\"1\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602642349);
INSERT INTO `y3_auth_admin_log` VALUES (13, 1, '栏目添加了功能{\"component_id\":\"109,110\",\"menu_id\":\"1\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602642431);
INSERT INTO `y3_auth_admin_log` VALUES (14, 1, '栏目添加了功能{\"component_id\":\"111,110\",\"menu_id\":\"1\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602642446);
INSERT INTO `y3_auth_admin_log` VALUES (15, 1, '栏目添加了功能{\"component_id\":\"111,110\",\"menu_id\":\"1\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602643019);
INSERT INTO `y3_auth_admin_log` VALUES (16, 1, '栏目删除了模块{\"menu_id\":\"1\",\"component_id\":\"111\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602643626);
INSERT INTO `y3_auth_admin_log` VALUES (17, 1, '移动了栏目模块{\"old_menu_id\":\"1\",\"new_menu_id\":\"2\",\"component_id\":\"111\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602643806);
INSERT INTO `y3_auth_admin_log` VALUES (18, 1, '移动了栏目模块{\"old_menu_id\":\"1\",\"new_menu_id\":\"2\",\"component_id\":\"111\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602643884);
INSERT INTO `y3_auth_admin_log` VALUES (19, 1, '移动了栏目模块{\"old_menu_id\":\"1\",\"new_menu_id\":\"2\",\"component_id\":\"110\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602643896);
INSERT INTO `y3_auth_admin_log` VALUES (20, 1, '移动栏目{\"id\":\"1\",\"type\":1,\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602644014);
INSERT INTO `y3_auth_admin_log` VALUES (21, 1, '部门添加了模块{\"org_id\":\"1\",\"component_id\":\"109,110\",\"cover_next\":\"1\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602645152);
INSERT INTO `y3_auth_admin_log` VALUES (22, 1, '部门添加了模块{\"org_id\":\"1\",\"component_id\":\"109,110,111\",\"cover_next\":\"1\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602645202);
INSERT INTO `y3_auth_admin_log` VALUES (23, 1, '部门添加了模块{\"org_id\":\"1\",\"component_id\":\"109,110,111\",\"cover_next\":\"2\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602645221);
INSERT INTO `y3_auth_admin_log` VALUES (24, 1, '部门添加了模块{\"org_id\":\"1\",\"component_id\":\"109,110,111\",\"cover_next\":\"2\",\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1602725182);
INSERT INTO `y3_auth_admin_log` VALUES (25, 1, '编辑员工{\"employee_id\":\"60\",\"account\":\"xiongan60\",\"password\":\"123456\",\"name\":\"xiongan60\",\"phone\":\"11111111111\",\"identification\":\"441622199501230122\",\"duty_id\":\"1\",\"role_id\":\"1\",\"organization_id\":\"1\",\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":\"1\",\"entry_status\":\"1\",\"sex\":\"1\",\"company_id\":\"1\",\"join_project_id\":\"4\",\"show_project_id\":\"4,5\",\"id\":\"60\"}原数据 [{\"id\":1,\"company_id\":1,\"account\":\"admin\",\"password\":\"08f2a395c5ac177ffb03cc0b0975555f\",\"duty_id\":10,\"role_id\":0,\"organization_id\":3,\"name\":\"小北\",\"phone\":\"15666666661\",\"identification\":\"\",\"admin_status\":0,\"status\":1,\"partake_project\":2,\"check_project\":1,\"entry_status\":0,\"entry_time\":\"1601435574\",\"sex\":1,\"last_login_time\":1602489958,\"add_time\":\"1970-01-01 08:00:00\",\"update_time\":\"2020-10-12 16:05:58\"},{\"id\":60,\"company_id\":1,\"account\":\"xiongan60\",\"password\":\"123456\",\"duty_id\":1,\"role_id\":1,\"organization_id\":20,\"name\":\"xiongan60\",\"phone\":\"11111111111\",\"identification\":\"441622199501230122\",\"admin_status\":1,\"status\":1,\"partake_project\":1,\"check_project\":1,\"entry_status\":1,\"entry_time\":\"1\",\"sex\":1,\"last_login_time\":0,\"add_time\":\"2020-10-19 15:17:27\",\"update_time\":\"2020-10-19 15:49:05\"}]', 1603097079);
INSERT INTO `y3_auth_admin_log` VALUES (26, 1, '编辑项目员工{\"id\":\"4\",\"employee_list\":[{\"employee_id\":\"23\",\"organization_id\":\"0\"}],\"organization_ids\":\"15,13\",\"company_id\":\"1\"}', 1603098245);
INSERT INTO `y3_auth_admin_log` VALUES (27, 1, '新增岗位信息{\"pid\":\"0\",\"name\":\"总经办主管2\",\"company_id\":\"1\"}', 1603098381);
INSERT INTO `y3_auth_admin_log` VALUES (28, 1, '编辑项目员工{\"id\":\"4\",\"employee_list\":[{\"organization_id\":\"20\",\"employee_id\":\"31\"}],\"organization_ids\":\"15\",\"company_id\":\"1\"}', 1603098706);
INSERT INTO `y3_auth_admin_log` VALUES (29, 1, '编辑项目员工{\"id\":\"4\",\"employee_list\":[{\"organization_id\":\"0\",\"employee_id\":\"60\"},{\"organization_id\":\"0\",\"employee_id\":\"50\"},{\"organization_id\":\"19\",\"employee_id\":\"23\"}],\"organization_ids\":\"15\",\"company_id\":\"1\"}', 1603098787);
INSERT INTO `y3_auth_admin_log` VALUES (30, 1, '编辑项目员工{\"id\":\"4\",\"employee_list\":[{\"organization_id\":\"0\",\"employee_id\":\"46\"},{\"organization_id\":\"0\",\"employee_id\":\"50\"},{\"organization_id\":\"19\",\"employee_id\":\"23\"}],\"organization_ids\":\"15\",\"company_id\":\"1\"}', 1603098865);
INSERT INTO `y3_auth_admin_log` VALUES (31, 1, '编辑项目员工{\"id\":\"4\",\"employee_list\":[{\"organization_id\":\"0\",\"employee_id\":\"46\"},{\"organization_id\":\"0\",\"employee_id\":\"50\"},{\"organization_id\":\"19\",\"employee_id\":\"23\"}],\"organization_ids\":\"15\",\"company_id\":\"1\"}', 1603099052);
INSERT INTO `y3_auth_admin_log` VALUES (32, 1, '编辑项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"12,15\",\"id\":\"4\",\"company_id\":\"1\"}', 1603100798);
INSERT INTO `y3_auth_admin_log` VALUES (33, 1, '编辑项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"12\",\"id\":\"4\",\"company_id\":\"1\"}', 1603100818);
INSERT INTO `y3_auth_admin_log` VALUES (34, 1, '编辑项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"12,17,19\",\"id\":\"4\",\"company_id\":\"1\"}', 1603100889);
INSERT INTO `y3_auth_admin_log` VALUES (35, 1, '编辑项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"12\",\"id\":\"4\",\"company_id\":\"1\"}', 1603100901);
INSERT INTO `y3_auth_admin_log` VALUES (36, 1, '编辑项目员工{\"id\":\"4\",\"employee_list\":[{\"organization_id\":\"0\",\"employee_id\":\"46\"},{\"organization_id\":\"0\",\"employee_id\":\"50\"},{\"organization_id\":\"19\",\"employee_id\":\"23\"}],\"organization_ids\":\"15\",\"company_id\":\"1\"}', 1603100925);
INSERT INTO `y3_auth_admin_log` VALUES (37, 1, '编辑项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"12,17\",\"id\":\"4\",\"company_id\":\"1\"}', 1603100936);
INSERT INTO `y3_auth_admin_log` VALUES (38, 1, '编辑项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"12\",\"id\":\"4\",\"company_id\":\"1\"}', 1603100949);
INSERT INTO `y3_auth_admin_log` VALUES (39, 1, '添加项目{\"name\":\"\\u9879\\u76ee\\u56db88888\",\"organization_ids\":\"13,12\",\"company_id\":\"1\"}', 1603162862);
INSERT INTO `y3_auth_admin_log` VALUES (40, 1, '添加项目{\"name\":\"\\u9879\\u76ee\\u56db88888\",\"organization_ids\":\"13,12\",\"company_id\":\"1\"}', 1603162893);
INSERT INTO `y3_auth_admin_log` VALUES (41, 1, '添加项目{\"name\":\"\\u9879\\u76ee\\u56db88\",\"organization_ids\":\"13,17\",\"company_id\":\"1\"}', 1603163093);
INSERT INTO `y3_auth_admin_log` VALUES (42, 1, '添加项目{\"name\":\"\\u9879\\u76ee\\u56db88\",\"organization_ids\":\"13,17\",\"company_id\":\"1\"}', 1603163211);
INSERT INTO `y3_auth_admin_log` VALUES (43, 1, '添加项目{\"name\":\"\\u9879\\u76ee\\u56db88\",\"organization_ids\":\"13,17\",\"company_id\":\"1\"}', 1603163354);
INSERT INTO `y3_auth_admin_log` VALUES (44, 1, '添加项目{\"name\":\"\\u9879\\u76ee\\u56db88\",\"organization_ids\":\"13,17\",\"company_id\":\"1\"}', 1603163505);
INSERT INTO `y3_auth_admin_log` VALUES (45, 1, '添加项目{\"name\":\"\\u9879\\u76ee\\u56db88\",\"organization_ids\":\"13,17\",\"company_id\":\"1\"}', 1603163690);
INSERT INTO `y3_auth_admin_log` VALUES (46, 1, '添加项目{\"name\":\"\\u9879\\u76ee\\u56db88\",\"organization_ids\":\"13,17\",\"company_id\":\"1\"}', 1603163839);
INSERT INTO `y3_auth_admin_log` VALUES (47, 1, '添加项目{\"name\":\"\\u9879\\u76ee\\u56db88\",\"organization_ids\":\"13,17\",\"company_id\":\"1\"}', 1603164194);
INSERT INTO `y3_auth_admin_log` VALUES (48, 1, '添加项目{\"name\":\"\\u9879\\u76ee\\u56db88\",\"organization_ids\":\"13,17\",\"company_id\":\"1\"}', 1603165095);
INSERT INTO `y3_auth_admin_log` VALUES (49, 1, '添加项目{\"name\":\"\\u9879\\u76ee\\u56db88\",\"organization_ids\":\"13,17\",\"company_id\":\"1\"}', 1603166051);
INSERT INTO `y3_auth_admin_log` VALUES (50, 1, '添加项目{\"name\":\"\\u9879\\u76ee\\u56db88\",\"organization_ids\":\"13,17\",\"company_id\":\"1\"}', 1603166107);
INSERT INTO `y3_auth_admin_log` VALUES (51, 1, '用户修改员工状态id: 23内容 {\"employee_id\":\"23\"}', 1603271822);
INSERT INTO `y3_auth_admin_log` VALUES (52, 1, '用户修改员工状态id: 23内容 {\"employee_id\":\"23\"}', 1603271861);
INSERT INTO `y3_auth_admin_log` VALUES (53, 1, '用户修改员工状态id: 23内容 {\"employee_id\":\"23\"}', 1603271898);
INSERT INTO `y3_auth_admin_log` VALUES (54, 1, '用户修改员工状态id: 23内容 {\"employee_id\":\"23\"}', 1603271904);
INSERT INTO `y3_auth_admin_log` VALUES (55, 1, '用户修改员工状态id: 23内容 {\"employee_id\":\"23\",\"status\":\"2\"}', 1603271935);
INSERT INTO `y3_auth_admin_log` VALUES (56, 1, '用户修改员工状态id: 23内容 {\"employee_id\":\"23\",\"status\":\"2\",\"admin_status\":\"1\"}', 1603271946);
INSERT INTO `y3_auth_admin_log` VALUES (57, 1, '添加项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"13,17\",\"company_id\":\"1\"}', 1603336151);
INSERT INTO `y3_auth_admin_log` VALUES (58, 1, '编辑项目状态{\"type\":\"3\",\"id\":\"3\",\"company_id\":\"1\"}', 1603336756);
INSERT INTO `y3_auth_admin_log` VALUES (59, 1, '编辑项目状态{\"type\":\"2\",\"id\":\"3\",\"company_id\":\"1\"}', 1603336778);
INSERT INTO `y3_auth_admin_log` VALUES (60, 1, '编辑项目状态{\"type\":\"2\",\"id\":\"24\",\"company_id\":\"1\"}', 1603336790);
INSERT INTO `y3_auth_admin_log` VALUES (61, 1, '添加项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"2,17\",\"company_id\":\"1\"}', 1603337345);
INSERT INTO `y3_auth_admin_log` VALUES (62, 1, '添加项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"2,17\",\"company_id\":\"1\"}', 1603337438);
INSERT INTO `y3_auth_admin_log` VALUES (63, 1, '添加项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"15,17\",\"company_id\":\"1\"}', 1603337685);
INSERT INTO `y3_auth_admin_log` VALUES (64, 1, '添加项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"15,17\",\"company_id\":\"1\"}', 1603337983);
INSERT INTO `y3_auth_admin_log` VALUES (65, 1, '添加项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"15,17\",\"company_id\":\"1\"}', 1603338127);
INSERT INTO `y3_auth_admin_log` VALUES (66, 1, '添加项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"15,17\",\"company_id\":\"1\"}', 1603338258);
INSERT INTO `y3_auth_admin_log` VALUES (67, 1, '添加项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"15,17\",\"company_id\":\"1\"}', 1603338313);
INSERT INTO `y3_auth_admin_log` VALUES (68, 1, '添加项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"15,17\",\"company_id\":\"1\"}', 1603338500);
INSERT INTO `y3_auth_admin_log` VALUES (69, 1, '添加项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"15,17\",\"company_id\":\"1\"}', 1603338594);
INSERT INTO `y3_auth_admin_log` VALUES (70, 1, '添加项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"15,17\",\"company_id\":\"1\"}', 1603346602);
INSERT INTO `y3_auth_admin_log` VALUES (71, 1, '添加项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"15,17\",\"company_id\":\"1\"}', 1603346661);
INSERT INTO `y3_auth_admin_log` VALUES (72, 1, '添加项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"15,17\",\"company_id\":\"1\"}', 1603347515);
INSERT INTO `y3_auth_admin_log` VALUES (73, 1, '编辑项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"17\",\"id\":\"36\",\"company_id\":\"1\"}', 1603348012);
INSERT INTO `y3_auth_admin_log` VALUES (74, 1, '新增员工{\"account\":\"admin222343c\",\"job_number\":\"10\",\"password\":\"123456\",\"name\":\"xiongtest222343e\",\"phone\":\"11111111111\",\"identification\":\"441622199501230122\",\"duty_id\":\"1\",\"role_id\":\"1\",\"organization_id\":\"17\",\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"2\",\"check_project\":\"1\",\"entry_time\":\"1\",\"entry_status\":\"1\",\"sex\":\"1\",\"company_id\":\"1\",\"join_project_id\":\"4,5\",\"show_project_id\":\"3\",\"employee_id\":61}', 1603348622);
INSERT INTO `y3_auth_admin_log` VALUES (75, 1, '新增员工{\"account\":\"admin222343cc1\",\"job_number\":\"100\",\"password\":\"123456\",\"name\":\"xionst22343ea\",\"phone\":\"11111111111\",\"identification\":\"441622199501230122\",\"duty_id\":\"1\",\"role_id\":\"1\",\"organization_id\":\"17\",\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"2\",\"check_project\":\"1\",\"entry_time\":\"1\",\"entry_status\":\"1\",\"sex\":\"1\",\"company_id\":\"1\",\"join_project_id\":\"4,5\",\"show_project_id\":\"3\",\"employee_id\":62}', 1603348785);
INSERT INTO `y3_auth_admin_log` VALUES (76, 1, '新增员工{\"account\":\"xiongan110\",\"job_number\":\"1001\",\"password\":\"123456\",\"name\":\"xiongan110\",\"phone\":\"11111111111\",\"identification\":\"441622199501230122\",\"duty_id\":\"1\",\"role_id\":\"1\",\"organization_id\":\"20\",\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"2\",\"check_project\":\"1\",\"entry_time\":\"1\",\"entry_status\":\"1\",\"sex\":\"1\",\"company_id\":\"1\",\"join_project_id\":\"4,5\",\"show_project_id\":\"3\",\"employee_id\":63}', 1603348926);
INSERT INTO `y3_auth_admin_log` VALUES (77, 1, '新增员工{\"account\":\"xiongan1101\",\"job_number\":\"10011\",\"password\":\"123456\",\"name\":\"xiongan1101\",\"phone\":\"11111111111\",\"identification\":\"441622199501230122\",\"duty_id\":\"1\",\"role_id\":\"1\",\"organization_id\":\"20\",\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":\"1\",\"entry_status\":\"1\",\"sex\":\"1\",\"company_id\":\"1\",\"join_project_id\":\"4\",\"show_project_id\":\"3\",\"employee_id\":64}', 1603348985);
INSERT INTO `y3_auth_admin_log` VALUES (78, 1, '新增员工{\"account\":\"xiongan11012\",\"job_number\":\"100112\",\"password\":\"123456\",\"name\":\"xiongan11012\",\"phone\":\"11111111111\",\"identification\":\"441622199501230122\",\"duty_id\":\"1\",\"role_id\":\"1\",\"organization_id\":\"1\",\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":\"1\",\"entry_status\":\"1\",\"sex\":\"1\",\"company_id\":\"1\",\"join_project_id\":\"4\",\"show_project_id\":\"3\",\"employee_id\":65}', 1603349034);
INSERT INTO `y3_auth_admin_log` VALUES (79, 1, '新增员工{\"account\":\"xiongan11012\",\"job_number\":\"100112\",\"password\":\"123456\",\"name\":\"xiongan11012\",\"phone\":\"11111111111\",\"identification\":\"441622199501230122\",\"duty_id\":\"1\",\"role_id\":\"1\",\"organization_id\":\"20\",\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":\"1\",\"entry_status\":\"1\",\"sex\":\"1\",\"company_id\":\"1\",\"join_project_id\":\"4\",\"show_project_id\":\"3\",\"employee_id\":66}', 1603349368);
INSERT INTO `y3_auth_admin_log` VALUES (80, 1, '新增员工{\"account\":\"xiongan1\",\"job_number\":\"1\",\"password\":\"123456\",\"name\":\"xiongan1\",\"phone\":\"11111111111\",\"identification\":\"441622199501230122\",\"duty_id\":\"1\",\"role_id\":\"1\",\"organization_id\":\"1\",\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":\"1\",\"entry_status\":\"1\",\"sex\":\"1\",\"company_id\":\"1\",\"join_project_id\":\"4\",\"show_project_id\":\"3\",\"employee_id\":67}', 1603349397);
INSERT INTO `y3_auth_admin_log` VALUES (81, 1, '新增员工{\"account\":\"xiongan12\",\"job_number\":\"12\",\"password\":\"123456\",\"name\":\"xiongan12\",\"phone\":\"11111111111\",\"identification\":\"441622199501230122\",\"duty_id\":\"1\",\"role_id\":\"1\",\"organization_id\":\"1\",\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":\"1\",\"entry_status\":\"1\",\"sex\":\"1\",\"company_id\":\"1\",\"join_project_id\":\"4\",\"show_project_id\":\"3\",\"employee_id\":68}', 1603349577);
INSERT INTO `y3_auth_admin_log` VALUES (82, 1, '新增员工{\"account\":\"xiongan122\",\"job_number\":\"123\",\"password\":\"123456\",\"name\":\"xiongan122\",\"phone\":\"11111111111\",\"identification\":\"441622199501230122\",\"duty_id\":\"1\",\"role_id\":\"1\",\"organization_id\":\"1\",\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":\"1\",\"entry_status\":\"1\",\"sex\":\"1\",\"company_id\":\"1\",\"join_project_id\":\"4\",\"show_project_id\":\"3\",\"employee_id\":69}', 1603350223);
INSERT INTO `y3_auth_admin_log` VALUES (83, 1, '新增员工{\"account\":\"xiongan1221\",\"job_number\":\"1231\",\"password\":\"123456\",\"name\":\"xiongan1221\",\"phone\":\"11111111111\",\"identification\":\"441622199501230122\",\"duty_id\":\"1\",\"role_id\":\"1\",\"organization_id\":\"1\",\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":\"1\",\"entry_status\":\"1\",\"sex\":\"1\",\"company_id\":\"1\",\"join_project_id\":\"4\",\"show_project_id\":\"3\",\"employee_id\":70}', 1603350255);
INSERT INTO `y3_auth_admin_log` VALUES (84, 1, '新增员工{\"account\":\"xiongan12212\",\"job_number\":\"12312\",\"password\":\"123456\",\"name\":\"xiongan12212\",\"phone\":\"11111111111\",\"identification\":\"441622199501230122\",\"duty_id\":\"1\",\"role_id\":\"1\",\"organization_id\":\"1\",\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":\"1\",\"entry_status\":\"1\",\"sex\":\"1\",\"company_id\":\"1\",\"join_project_id\":\"4\",\"show_project_id\":\"3\",\"employee_id\":71}', 1603350291);
INSERT INTO `y3_auth_admin_log` VALUES (85, 1, '新增员工{\"account\":\"xa1\",\"job_number\":\"122\",\"password\":\"123456\",\"name\":\"xa1\",\"phone\":\"11111111111\",\"identification\":\"441622199501230122\",\"duty_id\":\"1\",\"role_id\":\"1\",\"organization_id\":\"1\",\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":\"1\",\"entry_status\":\"1\",\"sex\":\"1\",\"company_id\":\"1\",\"join_project_id\":\"4\",\"show_project_id\":\"3\",\"employee_id\":72}', 1603350544);
INSERT INTO `y3_auth_admin_log` VALUES (86, 1, '新增员工{\"account\":\"xa12\",\"job_number\":\"1222\",\"password\":\"123456\",\"name\":\"xa12\",\"phone\":\"11111111111\",\"identification\":\"441622199501230122\",\"duty_id\":\"1\",\"role_id\":\"1\",\"organization_id\":\"1\",\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":\"1\",\"entry_status\":\"1\",\"sex\":\"1\",\"company_id\":\"1\",\"join_project_id\":\"4\",\"show_project_id\":\"3\",\"employee_id\":73}', 1603350747);
INSERT INTO `y3_auth_admin_log` VALUES (87, 1, '新增员工{\"account\":\"xa122\",\"job_number\":\"12222\",\"password\":\"123456\",\"name\":\"xa122\",\"phone\":\"11111111111\",\"identification\":\"441622199501230122\",\"duty_id\":\"1\",\"role_id\":\"1\",\"organization_id\":\"1\",\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":\"1\",\"entry_status\":\"1\",\"sex\":\"1\",\"company_id\":\"1\",\"join_project_id\":\"4\",\"show_project_id\":\"3\",\"employee_id\":74}', 1603351038);
INSERT INTO `y3_auth_admin_log` VALUES (88, 1, '新增员工{\"account\":\"xa1223\",\"job_number\":\"122223\",\"password\":\"123456\",\"name\":\"xa1223\",\"phone\":\"11111111111\",\"identification\":\"441622199501230122\",\"duty_id\":\"1\",\"role_id\":\"1\",\"organization_id\":\"1\",\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":\"1\",\"entry_status\":\"1\",\"sex\":\"1\",\"company_id\":\"1\",\"join_project_id\":\"4\",\"show_project_id\":\"3\",\"employee_id\":75}', 1603351080);
INSERT INTO `y3_auth_admin_log` VALUES (89, 1, '新增员工{\"account\":\"xa12234\",\"job_number\":\"1222234\",\"password\":\"123456\",\"name\":\"xa12234\",\"phone\":\"11111111111\",\"identification\":\"441622199501230122\",\"duty_id\":\"1\",\"role_id\":\"1\",\"organization_id\":\"20\",\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":\"1\",\"entry_status\":\"1\",\"sex\":\"1\",\"company_id\":\"1\",\"join_project_id\":\"4\",\"show_project_id\":\"3\",\"employee_id\":76}', 1603351160);
INSERT INTO `y3_auth_admin_log` VALUES (90, 1, '新增员工{\"account\":\"xa122344\",\"job_number\":\"12222344\",\"password\":\"123456\",\"name\":\"xa122344\",\"phone\":\"11111111111\",\"identification\":\"441622199501230122\",\"duty_id\":\"1\",\"role_id\":\"1\",\"organization_id\":\"1\",\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":\"1\",\"entry_status\":\"1\",\"sex\":\"1\",\"company_id\":\"1\",\"join_project_id\":\"4\",\"show_project_id\":\"3\",\"employee_id\":77}', 1603351206);
INSERT INTO `y3_auth_admin_log` VALUES (91, 1, '新增了角色权限{\"role\":1,\"permission\":[\"2\",\"3\",\"4\"]}', 1603354722);
INSERT INTO `y3_auth_admin_log` VALUES (92, 1, '新增了角色权限{\"role\":2,\"permission\":[\"2\",\"3\",\"4\"]}', 1603354883);
INSERT INTO `y3_auth_admin_log` VALUES (93, 1, '新增员工{\"account\":\"admin11\",\"job_number\":\"11\",\"password\":\"123456\",\"name\":\"蓝蓝33\",\"phone\":\"13211111111\",\"identification\":\"11010119900307053X\",\"duty_id\":1,\"role_id\":1,\"organization_id\":15,\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"1\",\"check_project\":\"2\",\"entry_time\":1603497600,\"entry_status\":1,\"sex\":2,\"company_id\":\"1\",\"join_project_id\":\"4,5\",\"show_project_id\":\"4,5,6,7,8\",\"employee_id\":79}', 1603436541);
INSERT INTO `y3_auth_admin_log` VALUES (94, 1, '移动栏目{\"id\":\"3\",\"type\":1,\"login_usr\":{\"company_id\":1,\"employee_id\":1,\"uid\":1,\"account\":\"admin\"}}', 1603437672);
INSERT INTO `y3_auth_admin_log` VALUES (95, 1, '新增员工{\"account\":\"admin2\",\"job_number\":\"112\",\"password\":\"123456\",\"name\":\"蓝蓝\",\"phone\":\"13121111111\",\"identification\":\"11010119900307053X\",\"duty_id\":1,\"role_id\":1,\"organization_id\":15,\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"1\",\"check_project\":\"2\",\"entry_time\":1603497600,\"entry_status\":1,\"sex\":2,\"company_id\":\"1\",\"join_project_id\":\"4,5\",\"show_project_id\":\"4,5,6,7,8\",\"employee_id\":80}', 1603439737);
INSERT INTO `y3_auth_admin_log` VALUES (96, 1, '新增员工{\"account\":\"admin23\",\"job_number\":\"1123\",\"password\":\"123456\",\"name\":\"蓝蓝3\",\"phone\":\"13121111111\",\"identification\":\"11010119900307053X\",\"duty_id\":1,\"role_id\":1,\"organization_id\":15,\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"1\",\"check_project\":\"2\",\"entry_time\":1603497600,\"entry_status\":1,\"sex\":2,\"company_id\":\"1\",\"join_project_id\":\"4,5\",\"show_project_id\":\"4,5,6,7,8\",\"employee_id\":81}', 1603504779);
INSERT INTO `y3_auth_admin_log` VALUES (97, 1, '新增员工{\"account\":\"admin223\",\"job_number\":\"11232\",\"password\":\"Fsw0j6mIysH2EdwDckg0DA==\",\"name\":\"蓝蓝32\",\"phone\":\"13121111111\",\"identification\":\"11010119900307053X\",\"duty_id\":1,\"role_id\":1,\"organization_id\":15,\"admin_status\":\"1\",\"status\":\"1\",\"partake_project\":\"1\",\"check_project\":\"2\",\"entry_time\":1603497600,\"entry_status\":1,\"sex\":2,\"company_id\":\"1\",\"join_project_id\":\"4,5\",\"show_project_id\":\"4,5,6,7,8\",\"employee_id\":82}', 1603505780);
INSERT INTO `y3_auth_admin_log` VALUES (98, 1, '用户修改密码id: 1', 1603522235);
INSERT INTO `y3_auth_admin_log` VALUES (99, 1, '编辑项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"17,13\",\"id\":\"36\",\"company_id\":\"1\"}', 1603696477);
INSERT INTO `y3_auth_admin_log` VALUES (100, 1, '编辑公司信息{\"name\":\"广州搜游科技\",\"company_id\":\"1\"}', 1603957493);
INSERT INTO `y3_auth_admin_log` VALUES (101, 1, '编辑公司信息{\"name\":\"广州搜游科技\",\"company_id\":\"1\"}', 1603957558);
INSERT INTO `y3_auth_admin_log` VALUES (102, 0, '添加了模块组件{\"url\":\"unanswered\",\"module_id\":\"1\",\"content\":\"asc\",\"icon\":\"http:\\/\\/www.baidu.com\",\"is_web\":\"1\",\"parent_id\":\"0\",\"identity\":\"\\/api\\/we\\/d\",\"component_name\":\"\\u6d4b\\u8bd5\\u65b0\\u529f\\u80fd\",\"login_usr\":null}', 1606791264);
INSERT INTO `y3_auth_admin_log` VALUES (103, 0, '修改了模块组件{\"url\":\"unanswered\",\"module_id\":\"1\",\"content\":\"asc666\",\"icon\":\"http:\\/\\/www.baidu.com\",\"is_web\":\"1\",\"parent_id\":\"0\",\"identity\":\"\\/api\\/we\\/d\",\"component_name\":\"\\u6d4b\\u8bd5\\u65b0\\u529f\\u80fd888\",\"id\":\"114\",\"login_usr\":null}', 1606791548);
INSERT INTO `y3_auth_admin_log` VALUES (104, 0, '添加了模块组件{\"url\":\"unanswered\",\"module_id\":\"1\",\"content\":\"asc666\",\"icon\":\"http:\\/\\/www.baidu.com\",\"is_web\":\"1\",\"parent_id\":\"114\",\"identity\":\"\\/api\\/we\\/d\",\"component_name\":\"\\u6d4b\\u8bd5\\u5b50\\u529f\\u80fd\",\"login_usr\":null}', 1606791624);
INSERT INTO `y3_auth_admin_log` VALUES (105, 0, '添加了模块组件{\"url\":\"unanswered\",\"module_id\":\"1\",\"content\":\"asc666\",\"icon\":\"http:\\/\\/www.baidu.com\",\"is_web\":\"2\",\"parent_id\":\"114\",\"identity\":\"\\/api\\/we\\/d\",\"component_name\":\"\\u6dfb\\u52a0\\u529f\\u80fd\",\"login_usr\":null}', 1606791654);
INSERT INTO `y3_auth_admin_log` VALUES (106, 0, '添加了模块组件{\"url\":\"unanswered\",\"module_id\":\"1\",\"content\":\"asc666\",\"icon\":\"http:\\/\\/www.baidu.com\",\"is_web\":\"2\",\"parent_id\":\"114\",\"identity\":\"\\/api\\/we\\/d\",\"component_name\":\"\\u4fee\\u6539\\u529f\\u80fd\",\"login_usr\":null}', 1606791667);
INSERT INTO `y3_auth_admin_log` VALUES (107, 0, '栏目添加了功能{\"component_id\":\"114,115,116\",\"menu_id\":\"2\",\"login_usr\":{\"company_id\":1}}', 1606806822);
INSERT INTO `y3_auth_admin_log` VALUES (108, 0, '栏目添加了功能{\"component_id\":\"114,110,117\",\"menu_id\":\"1\",\"login_usr\":{\"company_id\":1}}', 1606806890);
INSERT INTO `y3_auth_admin_log` VALUES (109, 0, '栏目添加了功能{\"component_id\":\"114,110,117\",\"menu_id\":\"1\",\"login_usr\":{\"company_id\":1}}', 1606806908);
INSERT INTO `y3_auth_admin_log` VALUES (110, 0, '栏目添加了功能{\"component_id\":\"114,110,117\",\"menu_id\":\"1\",\"login_usr\":{\"company_id\":1}}', 1606807340);
INSERT INTO `y3_auth_admin_log` VALUES (111, 0, '栏目删除了模块{\"menu_id\":\"1\",\"component_id\":\"110\",\"login_usr\":null}', 1606807400);
INSERT INTO `y3_auth_admin_log` VALUES (112, 0, '栏目删除了模块{\"menu_id\":\"1\",\"component_id\":\"114\",\"login_usr\":null}', 1606807410);
INSERT INTO `y3_auth_admin_log` VALUES (113, 0, '栏目添加了功能{\"component_id\":\"114,110,117,118\",\"menu_id\":\"1\",\"login_usr\":{\"company_id\":1}}', 1606807448);
INSERT INTO `y3_auth_admin_log` VALUES (114, 1, '新增角色{\"name\":\"熊大角色\",\"company_id\":\"1\"}', 1606983685);
INSERT INTO `y3_auth_admin_log` VALUES (115, 1, '新增角色{\"name\":\"熊大角色\",\"company_id\":\"1\"}', 1606983728);
INSERT INTO `y3_auth_admin_log` VALUES (116, 1, '新增角色{\"name\":\"熊二角色\",\"company_id\":\"1\"}', 1606983741);
INSERT INTO `y3_auth_admin_log` VALUES (117, 1, '编辑了角色信息信息{\"role_id\":\"1\",\"name\":\"熊二角色2\",\"company_id\":\"1\",\"id\":\"1\"}', 1606983808);
INSERT INTO `y3_auth_admin_log` VALUES (118, 1, '删除模块角色{\"role_id\":\"1\",\"id\":\"1\",\"is_del\":2}', 1606983871);
INSERT INTO `y3_auth_admin_log` VALUES (119, 1, '编辑了模块权限{\"role_id\":\"1\",\"component_ids\":\"112,113\",\"company_id\":\"1\"}', 1606984485);
INSERT INTO `y3_auth_admin_log` VALUES (120, 1, '编辑了模块权限{\"role_id\":\"2\",\"component_ids\":\"112,113\",\"company_id\":\"1\"}', 1606984506);
INSERT INTO `y3_auth_admin_log` VALUES (121, 1, '编辑了模块权限{\"role_id\":\"2\",\"component_ids\":\"112,113,110\",\"company_id\":\"1\"}', 1606984517);
INSERT INTO `y3_auth_admin_log` VALUES (122, 1, '编辑了模块权限{\"role_id\":\"2\",\"component_ids\":\"112,113\",\"company_id\":\"1\"}', 1606984524);
INSERT INTO `y3_auth_admin_log` VALUES (123, 1, '删除模块角色{\"role_id\":\"1\",\"id\":\"1\",\"is_del\":2}', 1606987066);
INSERT INTO `y3_auth_admin_log` VALUES (124, 1, '编辑了模块权限{\"role_id\":\"2\",\"component_ids\":\"112,113\",\"company_id\":\"1\"}', 1607043396);
INSERT INTO `y3_auth_admin_log` VALUES (125, 1, '新增角色{\"name\":\"熊三角色\",\"company_id\":\"1\"}', 1607043412);
INSERT INTO `y3_auth_admin_log` VALUES (126, 1, '删除模块角色{\"role_id\":\"1\",\"id\":\"1\",\"is_del\":2}', 1607043717);
INSERT INTO `y3_auth_admin_log` VALUES (127, 1, '编辑员工{\"employee_id\":1,\"account\":\"quanguanqing1\",\"password\":\"rhLNWy5AI75lmmHwgiYqDQ==\",\"job_number\":\"0019\",\"name\":\"全冠清\",\"phone\":\"13800138020\",\"identification\":\"230119197801080000\",\"duty_id\":41,\"role_id\":2,\"organization_id\":61,\"admin_status\":1,\"status\":1,\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":1601856000,\"entry_status\":1,\"sex\":2,\"company_id\":\"1\",\"join_project_id\":\"43,58,44,50,45,46,47,51,55,60,57,56,59\",\"show_project_id\":\"34,35,37,39,42,43,44,45,46,47,50,51,55,56,57,58,59,60\",\"admin_role_id\":0,\"id\":1}原数据 {\"id\":1,\"job_number\":\"1050\",\"company_id\":1,\"account\":\"admin\",\"password\":\"Fsw0j6mIysH2EdwDckg0DA==\",\"duty_id\":10,\"admin_role_id\":0,\"role_id\":1,\"organization_id\":3,\"name\":\"小北\",\"phone\":\"15666666661\",\"identification\":\"\",\"admin_status\":1,\"status\":1,\"partake_project\":2,\"check_project\":1,\"entry_status\":1,\"entry_time\":1601435574,\"sex\":1,\"last_login_time\":1607043681,\"add_time\":\"1970-01-01 08:00:00\",\"update_time\":\"2020-12-04 09:01:21\"}', 1607044247);
INSERT INTO `y3_auth_admin_log` VALUES (128, 1, '编辑员工{\"employee_id\":1,\"account\":\"admin\",\"password\":\"rhLNWy5AI75lmmHwgiYqDQ==\",\"job_number\":\"0019\",\"name\":\"全冠清\",\"phone\":\"13800138020\",\"identification\":\"230119197801080000\",\"duty_id\":41,\"role_id\":2,\"organization_id\":61,\"admin_status\":1,\"status\":1,\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":1601856000,\"entry_status\":1,\"sex\":2,\"company_id\":\"1\",\"join_project_id\":\"43,58,44,50,45,46,47,51,55,60,57,56,59\",\"show_project_id\":\"34,35,37,39,42,43,44,45,46,47,50,51,55,56,57,58,59,60\",\"admin_role_id\":0,\"id\":1}原数据 {\"id\":1,\"job_number\":\"0019\",\"company_id\":1,\"account\":\"quanguanqing1\",\"password\":\"rhLNWy5AI75lmmHwgiYqDQ==\",\"duty_id\":41,\"admin_role_id\":0,\"role_id\":2,\"organization_id\":61,\"name\":\"全冠清\",\"phone\":\"13800138020\",\"identification\":\"230119197801080000\",\"admin_status\":1,\"status\":1,\"partake_project\":1,\"check_project\":1,\"entry_status\":1,\"entry_time\":1601856000,\"sex\":2,\"last_login_time\":1607043681,\"add_time\":\"1970-01-01 08:00:00\",\"update_time\":\"2020-12-04 09:10:47\"}', 1607044334);
INSERT INTO `y3_auth_admin_log` VALUES (129, 1, '编辑员工{\"employee_id\":1,\"account\":\"admin\",\"password\":\"rhLNWy5AI75lmmHwgiYqDQ==\",\"job_number\":\"0019\",\"name\":\"全冠清\",\"phone\":\"13800138020\",\"identification\":\"230119197801080000\",\"duty_id\":41,\"role_id\":2,\"organization_id\":61,\"admin_status\":1,\"status\":1,\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":1601856000,\"entry_status\":1,\"sex\":2,\"company_id\":\"1\",\"join_project_id\":\"43,58,44,50,45,46,47,51,55,60,57,56,59\",\"show_project_id\":\"34,35,37,39,42,43,44,45,46,47,50,51,55,56,57,58,59,60\",\"admin_role_id\":0,\"id\":1}原数据 {\"id\":1,\"job_number\":\"0019\",\"company_id\":1,\"account\":\"admin\",\"password\":\"rhLNWy5AI75lmmHwgiYqDQ==\",\"duty_id\":41,\"admin_role_id\":0,\"role_id\":2,\"organization_id\":61,\"name\":\"全冠清\",\"phone\":\"13800138020\",\"identification\":\"230119197801080000\",\"admin_status\":1,\"status\":1,\"partake_project\":1,\"check_project\":1,\"entry_status\":1,\"entry_time\":1601856000,\"sex\":2,\"last_login_time\":1607043681,\"add_time\":\"1970-01-01 08:00:00\",\"update_time\":\"2020-12-04 09:12:14\"}', 1607044341);
INSERT INTO `y3_auth_admin_log` VALUES (130, 1, '编辑员工{\"employee_id\":1,\"account\":\"admin\",\"password\":\"rhLNWy5AI75lmmHwgiYqDQ==\",\"job_number\":\"0019\",\"name\":\"全冠清\",\"phone\":\"13800138020\",\"identification\":\"230119197801080000\",\"duty_id\":41,\"role_id\":2,\"organization_id\":61,\"admin_status\":1,\"status\":1,\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":1601856000,\"entry_status\":1,\"sex\":2,\"company_id\":\"1\",\"join_project_id\":\"43,58,44,50,45,46,47,51,55,60,57,56,59\",\"show_project_id\":\"34,35,37,39,42,43,44,45,46,47,50,51,55,56,57,58,59,60\",\"admin_role_id\":1,\"id\":1}原数据 {\"id\":1,\"job_number\":\"0019\",\"company_id\":1,\"account\":\"admin\",\"password\":\"rhLNWy5AI75lmmHwgiYqDQ==\",\"duty_id\":41,\"admin_role_id\":0,\"role_id\":2,\"organization_id\":61,\"name\":\"全冠清\",\"phone\":\"13800138020\",\"identification\":\"230119197801080000\",\"admin_status\":1,\"status\":1,\"partake_project\":1,\"check_project\":1,\"entry_status\":1,\"entry_time\":1601856000,\"sex\":2,\"last_login_time\":1607043681,\"add_time\":\"1970-01-01 08:00:00\",\"update_time\":\"2020-12-04 09:12:14\"}', 1607044696);
INSERT INTO `y3_auth_admin_log` VALUES (131, 1, '编辑员工{\"employee_id\":1,\"account\":\"admin\",\"password\":\"rhLNWy5AI75lmmHwgiYqDQ==\",\"job_number\":\"0019\",\"name\":\"全冠清\",\"phone\":\"13800138020\",\"identification\":\"230119197801080000\",\"duty_id\":41,\"role_id\":2,\"organization_id\":61,\"admin_status\":1,\"status\":1,\"partake_project\":\"1\",\"check_project\":\"1\",\"entry_time\":1601856000,\"entry_status\":1,\"sex\":2,\"company_id\":\"1\",\"join_project_id\":\"43,58,44,50,45,46,47,51,55,60,57,56,59\",\"show_project_id\":\"34,35,37,39,42,43,44,45,46,47,50,51,55,56,57,58,59,60\",\"admin_role_id\":1,\"id\":1}原数据 {\"id\":1,\"job_number\":\"0019\",\"company_id\":1,\"account\":\"admin\",\"password\":\"rhLNWy5AI75lmmHwgiYqDQ==\",\"duty_id\":41,\"admin_role_id\":0,\"role_id\":2,\"organization_id\":61,\"name\":\"全冠清\",\"phone\":\"13800138020\",\"identification\":\"230119197801080000\",\"admin_status\":1,\"status\":1,\"partake_project\":1,\"check_project\":1,\"entry_status\":1,\"entry_time\":1601856000,\"sex\":2,\"last_login_time\":1607043681,\"add_time\":\"1970-01-01 08:00:00\",\"update_time\":\"2020-12-04 09:12:14\"}', 1607044836);
INSERT INTO `y3_auth_admin_log` VALUES (132, 1, '添加项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"15,17\",\"period_type\":\"1\",\"company_id\":\"1\"}', 1607069032);
INSERT INTO `y3_auth_admin_log` VALUES (133, 1, '添加项目{\"name\":\"\\u9879\\u76ee7777\",\"organization_ids\":\"15,17\",\"period_type\":\"1\",\"company_id\":\"1\"}', 1607069045);
INSERT INTO `y3_auth_admin_log` VALUES (134, 1, '添加项目{\"name\":\"\\u9879\\u76ee7777\",\"organization_ids\":\"15,17\",\"period_type\":\"1\",\"detail\":\"\\u554a\\u662f\\u5927\\u5bb6\\u624b\\u673a\\u5f85\\u673a\\u5c71\\u4e1c\\u57c3\\u53ca\\u8bf4\\u5230\\u5e95\\u5c31\\u662f\\u9a84\\u50b2i\\u4e0a\\u7684\\u9a84\\u50b2\\u7684\\u796d\\u626b\\u5c31\\u662f\",\"company_id\":\"1\"}', 1607069070);
INSERT INTO `y3_auth_admin_log` VALUES (135, 1, '添加项目{\"name\":\"\\u9879\\u76ee7778\",\"organization_ids\":\"15,17\",\"period_type\":\"2\",\"detail\":\"\\u554a\\u662f\\u5927\\u5bb6\\u624b\\u673a\\u5f85\\u673a\\u5c71\\u4e1c\\u57c3\\u53ca\\u8bf4\\u5230\\u5e95\\u5c31\\u662f\\u9a84\\u50b2i\\u4e0a\\u7684\\u9a84\\u50b2\\u7684\\u796d\\u626b\\u5c31\\u662f\",\"start_time\":\"1607069070\",\"end_time\":\"1607999070\",\"company_id\":\"1\"}', 1607069131);
INSERT INTO `y3_auth_admin_log` VALUES (136, 1, '添加项目{\"name\":\"\\u9879\\u76ee744454\",\"organization_ids\":\"15,17\",\"period_type\":\"2\",\"detail\":\"\\u554a\\u662f\\u5927\\u5bb6\\u624b\\u673a\\u5f85\\u673a\\u5c71\\u4e1c\\u57c3\\u53ca\\u8bf4\\u5230\\u5e95\\u5c31\\u662f\\u9a84\\u50b2i\\u4e0a\\u7684\\u9a84\\u50b2\\u7684\\u796d\\u626b\\u5c31\\u662f\",\"start_time\":\"1607069070\",\"end_time\":\"1607999070\",\"company_id\":\"1\"}', 1607130953);
INSERT INTO `y3_auth_admin_log` VALUES (137, 1, '编辑项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"17,13\",\"id\":\"41\",\"period_type\":\"1\",\"detail\":\"\\u554a\\u662f\\u5927\\u626b\\u5c31\\u662f\",\"start_time\":\"1607069000\",\"end_time\":\"1607999010\",\"company_id\":\"1\"}', 1607131112);
INSERT INTO `y3_auth_admin_log` VALUES (138, 1, '编辑项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"17,13\",\"id\":\"41\",\"period_type\":\"1\",\"start_time\":\"1607069000\",\"end_time\":\"1607999010\",\"company_id\":\"1\"}', 1607131367);
INSERT INTO `y3_auth_admin_log` VALUES (139, 1, '编辑项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"17,13\",\"id\":\"41\",\"period_type\":\"1\",\"start_time\":\"1607069000\",\"end_time\":\"1607999010\",\"company_id\":\"1\"}', 1607131544);
INSERT INTO `y3_auth_admin_log` VALUES (140, 1, '编辑项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"17,13\",\"id\":\"41\",\"period_type\":\"2\",\"start_time\":\"1607069000\",\"end_time\":\"1607999010\",\"company_id\":\"1\"}', 1607131557);
INSERT INTO `y3_auth_admin_log` VALUES (141, 1, '编辑项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"17,13\",\"id\":\"41\",\"period_type\":\"2\",\"detail\":\"\\u554a\",\"start_time\":\"1607069000\",\"end_time\":\"1607999010\",\"company_id\":\"1\"}', 1607131574);
INSERT INTO `y3_auth_admin_log` VALUES (142, 1, '编辑项目{\"name\":\"\\u9879\\u76ee\",\"organization_ids\":\"17,13\",\"id\":\"41\",\"period_type\":\"2\",\"start_time\":\"1607069000\",\"end_time\":\"1607999010\",\"company_id\":\"1\"}', 1607131588);
INSERT INTO `y3_auth_admin_log` VALUES (143, 0, '添加了模块组件{\"module_id\":\"1\",\"component_name\":\"\\u65b0\\u7248\",\"url\":\"en\",\"identity\":\"\\u6253\\u53d1\\u5927\\u591a\",\"is_web\":\"1\",\"pid\":\"119\",\"login_usr\":null}', 1607496794);
INSERT INTO `y3_auth_admin_log` VALUES (144, 0, '编辑了栏目{\"id\":\"1\",\"pid\":\"0\",\"menu_name\":\"\\u6d4b\\u8bd5\",\"login_usr\":null}', 1607498915);
INSERT INTO `y3_auth_admin_log` VALUES (145, 0, '删除了栏目{\"id\":\"5\",\"login_usr\":null}', 1607508807);
INSERT INTO `y3_auth_admin_log` VALUES (146, 0, '删除了栏目{\"id\":\"5\",\"login_usr\":null}', 1607509571);
INSERT INTO `y3_auth_admin_log` VALUES (147, 0, '修改了模块组件{\"id\":\"115\",\"component_name\":\"\\u6d4b\\u8bd511\",\"module_id\":\"1\",\"url\":\"111\",\"identity\":\"4455\",\"is_web\":\"1\",\"pid\":\"0\",\"login_usr\":null}', 1607594851);
INSERT INTO `y3_auth_admin_log` VALUES (148, 0, '修改了模块组件{\"id\":\"115\",\"component_name\":\"\\u6d4b\\u8bd511\",\"module_id\":\"1\",\"url\":\"111\",\"identity\":\"4455\",\"is_web\":\"1\",\"pid\":\"0\",\"login_usr\":null}', 1607594898);
INSERT INTO `y3_auth_admin_log` VALUES (149, 0, '移动栏目功能{\"old_menu_id\":\"8\",\"new_menu_id\":\"3\",\"login_usr\":null}', 1607659136);
INSERT INTO `y3_auth_admin_log` VALUES (150, 0, '移动栏目功能{\"old_menu_id\":\"8\",\"new_menu_id\":\"3\",\"login_usr\":null}', 1607659218);
INSERT INTO `y3_auth_admin_log` VALUES (151, 0, '栏目添加了功能{\"menu_id\":\"1\",\"component_id\":\"112,113\",\"login_usr\":{\"company_id\":1}}', 1607673364);
INSERT INTO `y3_auth_admin_log` VALUES (152, 0, '栏目添加了功能{\"menu_id\":\"2\",\"component_id\":\"115\",\"login_usr\":{\"company_id\":1}}', 1607673492);
INSERT INTO `y3_auth_admin_log` VALUES (153, 0, '栏目添加了功能{\"menu_id\":\"3\",\"component_id\":\"121\",\"login_usr\":{\"company_id\":1}}', 1607673531);
INSERT INTO `y3_auth_admin_log` VALUES (154, 0, '栏目添加了功能{\"menu_id\":\"8\",\"component_id\":\"122,123\",\"login_usr\":{\"company_id\":1}}', 1607673572);
INSERT INTO `y3_auth_admin_log` VALUES (155, 0, '栏目添加了功能{\"menu_id\":\"9\",\"component_id\":\"124\",\"login_usr\":{\"company_id\":1}}', 1607673600);
COMMIT;

-- ----------------------------
-- Table structure for y3_auth_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_admin_role`;
CREATE TABLE `y3_auth_admin_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `add_time` int unsigned NOT NULL DEFAULT '0',
  `update_time` int unsigned NOT NULL DEFAULT '0',
  `admin_id` int unsigned NOT NULL DEFAULT '0' COMMENT '管理员id',
  `admin_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '创建者名字',
  `is_del` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 正常 2 删除',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='角色表';

-- ----------------------------
-- Records of y3_auth_admin_role
-- ----------------------------
BEGIN;
INSERT INTO `y3_auth_admin_role` VALUES (1, '超级管理员', 1603354722, 0, 1, 'admin', 2);
INSERT INTO `y3_auth_admin_role` VALUES (2, '人事', 1603354883, 1607483639, 1, 'admin', 2);
INSERT INTO `y3_auth_admin_role` VALUES (3, '人事管理员', 1606959279, 1607134808, 0, '', 2);
INSERT INTO `y3_auth_admin_role` VALUES (4, '测试3', 1607134793, 0, 0, '', 1);
INSERT INTO `y3_auth_admin_role` VALUES (5, '测试3阿萨德', 1607483636, 0, 0, '', 1);
INSERT INTO `y3_auth_admin_role` VALUES (6, '一二三四五六七八九十', 1608112884, 0, 0, '', 1);
COMMIT;

-- ----------------------------
-- Table structure for y3_auth_admin_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_admin_role_permission`;
CREATE TABLE `y3_auth_admin_role_permission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `permission_id` int NOT NULL,
  `role_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `permission_id` (`permission_id`,`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COMMENT='权限角色表';

-- ----------------------------
-- Records of y3_auth_admin_role_permission
-- ----------------------------
BEGIN;
INSERT INTO `y3_auth_admin_role_permission` VALUES (64, 1, 5);
INSERT INTO `y3_auth_admin_role_permission` VALUES (84, 1, 6);
INSERT INTO `y3_auth_admin_role_permission` VALUES (35, 5, 4);
INSERT INTO `y3_auth_admin_role_permission` VALUES (61, 5, 5);
INSERT INTO `y3_auth_admin_role_permission` VALUES (81, 5, 6);
INSERT INTO `y3_auth_admin_role_permission` VALUES (37, 16, 4);
INSERT INTO `y3_auth_admin_role_permission` VALUES (63, 16, 5);
INSERT INTO `y3_auth_admin_role_permission` VALUES (83, 16, 6);
INSERT INTO `y3_auth_admin_role_permission` VALUES (36, 17, 4);
INSERT INTO `y3_auth_admin_role_permission` VALUES (62, 17, 5);
INSERT INTO `y3_auth_admin_role_permission` VALUES (82, 17, 6);
COMMIT;

-- ----------------------------
-- Table structure for y3_auth_company
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_company`;
CREATE TABLE `y3_auth_company` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `principal` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '负责人',
  `address` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '办公地址',
  `contacts` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '联系人',
  `service_phone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '前台电话',
  `contacts_phone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '联系电话',
  `verification` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '企业验证文案',
  `add_time` int unsigned NOT NULL DEFAULT '0' COMMENT '新增时间',
  `update_time` int unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `is_del` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '1正常 2删除',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `e_id` (`principal`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='组织公司详情信息表';

-- ----------------------------
-- Records of y3_auth_company
-- ----------------------------
BEGIN;
INSERT INTO `y3_auth_company` VALUES (1, '广州搜游科技', '', '天河区', '', '', '', '0', 0, 1603957493, 1);
INSERT INTO `y3_auth_company` VALUES (11, '优酷', '', '', '', '', '', 'youkus', 1603354477, 0, 1);
COMMIT;

-- ----------------------------
-- Table structure for y3_auth_company_module
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_company_module`;
CREATE TABLE `y3_auth_company_module` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int unsigned NOT NULL DEFAULT '0' COMMENT '公司id',
  `module_id` int unsigned NOT NULL DEFAULT '0' COMMENT '子模块id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of y3_auth_company_module
-- ----------------------------
BEGIN;
INSERT INTO `y3_auth_company_module` VALUES (6, 1, 1);
INSERT INTO `y3_auth_company_module` VALUES (7, 1, 2);
INSERT INTO `y3_auth_company_module` VALUES (8, 1, 3);
INSERT INTO `y3_auth_company_module` VALUES (9, 1, 4);
COMMIT;

-- ----------------------------
-- Table structure for y3_auth_component
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_component`;
CREATE TABLE `y3_auth_component` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `component_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '功能名字',
  `add_time` int unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `sort` int unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'web地址',
  `module_id` int unsigned NOT NULL DEFAULT '0' COMMENT '模块id',
  `update_time` int unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图标',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '内容',
  `pid` int unsigned NOT NULL DEFAULT '0' COMMENT '上级',
  `is_web` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '1 前端页面  2不是',
  `identity` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '接口url',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of y3_auth_component
-- ----------------------------
BEGIN;
INSERT INTO `y3_auth_component` VALUES (109, '任务', 1602583842, 0, '22', 1, 0, '', '', 0, 1, '');
INSERT INTO `y3_auth_component` VALUES (110, '项目', 1602583866, 0, '22', 1, 0, '', '', 109, 1, '');
INSERT INTO `y3_auth_component` VALUES (112, '进度', 1602583930, 0, '666', 1, 1602584338, '', '', 110, 1, '');
INSERT INTO `y3_auth_component` VALUES (113, 'test', 1603439726, 0, '222', 3, 0, '123', '1111', 0, 1, '');
INSERT INTO `y3_auth_component` VALUES (115, '测试11', 1606791264, 0, '111', 1, 1607594898, '', '', 0, 1, '4455');
INSERT INTO `y3_auth_component` VALUES (121, '测试999', 1606791264, 0, '111', 1, 1607594898, '', '', 0, 1, '4455');
INSERT INTO `y3_auth_component` VALUES (122, '测试88', 1606791264, 0, '111', 1, 1607594898, '', '', 0, 1, '4455');
INSERT INTO `y3_auth_component` VALUES (123, '测试66', 1606791264, 0, '111', 1, 1607594898, '', '', 0, 1, '4455');
INSERT INTO `y3_auth_component` VALUES (124, '测试777', 1606791264, 0, '', 1, 1607594898, '', '', 0, 1, '4455');
COMMIT;

-- ----------------------------
-- Table structure for y3_auth_depart_component
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_depart_component`;
CREATE TABLE `y3_auth_depart_component` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `org_id` int unsigned NOT NULL DEFAULT '0' COMMENT '部门id',
  `component_id` int unsigned NOT NULL DEFAULT '0' COMMENT '组件id',
  `company_id` int unsigned NOT NULL DEFAULT '0' COMMENT '公司id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of y3_auth_depart_component
-- ----------------------------
BEGIN;
INSERT INTO `y3_auth_depart_component` VALUES (49, 1, 109, 1);
INSERT INTO `y3_auth_depart_component` VALUES (51, 1, 111, 1);
COMMIT;

-- ----------------------------
-- Table structure for y3_auth_duty
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_duty`;
CREATE TABLE `y3_auth_duty` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int unsigned NOT NULL DEFAULT '0' COMMENT '公司id',
  `pid` int unsigned NOT NULL DEFAULT '0' COMMENT '上级关联id：0顶级岗位',
  `name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '组织名称',
  `grade` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '级别',
  `add_time` int unsigned NOT NULL DEFAULT '0' COMMENT '新增时间',
  `update_time` int unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `is_del` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '1正常 2删除 ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='职位信息表';

-- ----------------------------
-- Records of y3_auth_duty
-- ----------------------------
BEGIN;
INSERT INTO `y3_auth_duty` VALUES (1, 1, 0, '总经办主管', 1, 1599814013, 1599899547, 1);
INSERT INTO `y3_auth_duty` VALUES (2, 1, 1, '财务部', 1, 1599894994, 1599899577, 1);
INSERT INTO `y3_auth_duty` VALUES (3, 1, 2, '人事部', 1, 1599899067, 1599899547, 1);
INSERT INTO `y3_auth_duty` VALUES (4, 1, 1, '海外财务', 1, 0, 0, 1);
INSERT INTO `y3_auth_duty` VALUES (5, 1, 0, '管理层', 1, 0, 0, 1);
INSERT INTO `y3_auth_duty` VALUES (6, 0, 5, '微信事业部', 1, 0, 0, 1);
INSERT INTO `y3_auth_duty` VALUES (7, 0, 5, '企业发展事业', 1, 0, 0, 1);
INSERT INTO `y3_auth_duty` VALUES (8, 0, 5, '互动娱乐事业', 1, 0, 0, 1);
INSERT INTO `y3_auth_duty` VALUES (9, 0, 5, '移动互联网事业', 1, 0, 0, 1);
INSERT INTO `y3_auth_duty` VALUES (10, 0, 5, '网络媒体事业', 1, 0, 0, 1);
INSERT INTO `y3_auth_duty` VALUES (11, 0, 5, '社交网络事业', 1, 0, 0, 1);
INSERT INTO `y3_auth_duty` VALUES (12, 0, 0, '总', 1, 0, 0, 1);
INSERT INTO `y3_auth_duty` VALUES (13, 0, 12, '人事部', 1, 0, 0, 1);
INSERT INTO `y3_auth_duty` VALUES (14, 0, 6, '微信支付', 1, 0, 0, 1);
INSERT INTO `y3_auth_duty` VALUES (15, 0, 0, '熊安大文娱上市版', 1, 1600335400, 1600335803, 2);
INSERT INTO `y3_auth_duty` VALUES (16, 1, 2, '人事总监', 2, 1600335400, 1600335803, 2);
INSERT INTO `y3_auth_duty` VALUES (17, 1, 0, '熊安指定事业编岗位', 1, 1602294163, 1602294739, 1);
INSERT INTO `y3_auth_duty` VALUES (18, 1, 0, '总经办主管2', 1, 1603098381, 1603098381, 1);
COMMIT;

-- ----------------------------
-- Table structure for y3_auth_employee
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_employee`;
CREATE TABLE `y3_auth_employee` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `job_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '工号',
  `company_id` int unsigned NOT NULL DEFAULT '0' COMMENT '公司id',
  `account` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '登陆账号',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '登陆密码',
  `duty_id` int unsigned NOT NULL DEFAULT '0' COMMENT '职位id',
  `admin_role_id` int unsigned NOT NULL DEFAULT '0' COMMENT '后台角色id',
  `role_id` int unsigned NOT NULL DEFAULT '0' COMMENT '角色id ',
  `organization_id` int unsigned NOT NULL DEFAULT '0' COMMENT '组织id',
  `name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `phone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '联系电话',
  `identification` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '身份证',
  `admin_status` tinyint unsigned NOT NULL DEFAULT '2' COMMENT '后台登陆权限：1正常 2停用',
  `status` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '登陆状态: 1正常 2停用',
  `partake_project` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '参与项目: 1选择 2全部',
  `check_project` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '查看项目: 1 选择 2全部',
  `entry_status` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '在职状态：1在职 2离职',
  `entry_time` int unsigned NOT NULL DEFAULT '0' COMMENT '入职时间',
  `sex` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '性别:1 男 2女',
  `last_login_time` int unsigned NOT NULL DEFAULT '0' COMMENT 'grpc最近登陆时间',
  `add_time` int unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `update_time` int unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `is_del` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '状态：1正常 2删除',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `duty_id` (`duty_id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `organization_id` (`organization_id`) USING BTREE,
  KEY `account` (`account`) USING BTREE,
  KEY `company_id` (`company_id`) USING BTREE,
  KEY `admin_role_id` (`admin_role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of y3_auth_employee
-- ----------------------------
BEGIN;
INSERT INTO `y3_auth_employee` VALUES (1, '0019', 1, 'admin', 'rhLNWy5AI75lmmHwgiYqDQ==', 41, 1, 1, 61, '全冠清', '13800138020', '230119197801080000', 1, 1, 1, 1, 1, 1601856000, 2, 1607071088, 0, 1613619202, 1);
INSERT INTO `y3_auth_employee` VALUES (23, '1002', 1, '2222', 'Fsw0j6mIysH2EdwDckg0DA==', 11, 2, 0, 13, '叶', '13265328888', '', 1, 2, 1, 1, 1, 1601435574, 1, 0, 0, 1607481239, 1);
INSERT INTO `y3_auth_employee` VALUES (24, '', 1, '22228', 'Fsw0j6mIysH2EdwDckg0DA==', 12, 0, 0, 12, '222', '13265328888', '', 0, 0, 1, 1, 1, 1601435574, 1, 0, 0, 0, 1);
INSERT INTO `y3_auth_employee` VALUES (25, '', 1, '5878', 'Fsw0j6mIysH2EdwDckg0DA==', 13, 0, 0, 15, '222', '13265328888', '', 0, 0, 1, 1, 1, 1601435574, 1, 0, 0, 0, 1);
INSERT INTO `y3_auth_employee` VALUES (26, '', 1, '', 'Fsw0j6mIysH2EdwDckg0DA==', 0, 0, 0, 17, '', '', '', 2, 1, 1, 1, 1, 1, 1, 0, 0, 1602301929, 1);
INSERT INTO `y3_auth_employee` VALUES (27, '', 0, '123xioongan', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 1, 'xiongan', '11111111111', '441622199501230122', 1, 1, 1, 1, 1, 1, 1, 0, 1602313958, 1602313958, 1);
INSERT INTO `y3_auth_employee` VALUES (28, '', 0, '123xioongan1', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 1, 'xiongan', '11111111111', '441622199501230122', 1, 1, 1, 2, 1, 1, 1, 0, 1602314112, 1602314112, 1);
INSERT INTO `y3_auth_employee` VALUES (29, '', 1, '123xioongan12', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 1, 'xiongan', '11111111111', '441622199501230122', 1, 1, 1, 2, 1, 1, 1, 0, 1602314161, 1602314161, 1);
INSERT INTO `y3_auth_employee` VALUES (30, '', 1, '123xioongan123', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 1, 'xiongantet', '11111111111', '441622199501230122', 1, 1, 1, 2, 1, 1, 1, 0, 1602314255, 1602314255, 1);
INSERT INTO `y3_auth_employee` VALUES (31, '', 1, '123xioongan12323', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 1, 'xiongantet3', '11111111111', '441622199501230122', 1, 1, 2, 1, 1, 1, 1, 0, 1602314327, 1602496138, 1);
INSERT INTO `y3_auth_employee` VALUES (45, '', 1, '123xioongan12322', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 17, 'xiongantet32', '11111111111', '441622199501230122', 1, 1, 1, 1, 1, 1, 1, 0, 1602491391, 1602491391, 1);
INSERT INTO `y3_auth_employee` VALUES (46, '', 1, '123xioongan123222', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 2, 'xiongantet322', '11111111111', '441622199501230122', 1, 1, 2, 1, 1, 1, 1, 0, 1602491565, 1602491565, 1);
INSERT INTO `y3_auth_employee` VALUES (47, '', 1, '123xioongan123222q', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 18, 'xiongantet322q', '11111111111', '441622199501230122', 1, 1, 1, 1, 1, 1, 1, 0, 1602491731, 1602491731, 1);
INSERT INTO `y3_auth_employee` VALUES (48, '', 1, '123xioongan123222qd', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 19, 'xiongantet322qd', '11111111111', '441622199501230122', 1, 1, 1, 1, 1, 1, 1, 0, 1602491776, 1602491776, 1);
INSERT INTO `y3_auth_employee` VALUES (49, '', 1, '123xioongan123222qd2', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 19, 'xiongantet322qd2', '11111111111', '441622199501230122', 1, 1, 1, 1, 1, 1, 1, 0, 1602491913, 1602491913, 1);
INSERT INTO `y3_auth_employee` VALUES (66, '100112', 1, 'xiongan11012', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 20, 'xiongan11012', '11111111111', '441622199501230122', 1, 1, 1, 1, 1, 1, 1, 0, 1603349368, 1603349368, 1);
INSERT INTO `y3_auth_employee` VALUES (67, '1', 1, 'xiongan1', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 1, 'xiongan1', '11111111111', '441622199501230122', 1, 1, 1, 1, 1, 1, 1, 0, 1603349397, 1603349397, 1);
INSERT INTO `y3_auth_employee` VALUES (68, '12', 1, 'xiongan12', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 1, 'xiongan12', '11111111111', '441622199501230122', 1, 1, 1, 1, 1, 1, 1, 0, 1603349577, 1603349577, 1);
INSERT INTO `y3_auth_employee` VALUES (69, '123', 1, 'xiongan122', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 1, 'xiongan122', '11111111111', '441622199501230122', 1, 1, 1, 1, 1, 1, 1, 0, 1603350223, 1603350223, 1);
INSERT INTO `y3_auth_employee` VALUES (70, '1231', 1, 'xiongan1221', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 1, 'xiongan1221', '11111111111', '441622199501230122', 1, 1, 1, 1, 1, 1, 1, 0, 1603350255, 1603350255, 1);
INSERT INTO `y3_auth_employee` VALUES (71, '12312', 1, 'xiongan12212', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 1, 'xiongan12212', '11111111111', '441622199501230122', 1, 1, 1, 1, 1, 1, 1, 0, 1603350291, 1603350291, 1);
INSERT INTO `y3_auth_employee` VALUES (72, '122', 1, 'xa1', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 1, 'xa1', '11111111111', '441622199501230122', 1, 1, 1, 1, 1, 1, 1, 0, 1603350543, 1603350543, 1);
INSERT INTO `y3_auth_employee` VALUES (73, '1222', 1, 'xa12', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 1, 'xa12', '11111111111', '441622199501230122', 1, 1, 1, 1, 1, 1, 1, 0, 1603350747, 1603350747, 1);
INSERT INTO `y3_auth_employee` VALUES (74, '12222', 1, 'xa122', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 1, 'xa122', '11111111111', '441622199501230122', 1, 1, 1, 1, 1, 1, 1, 0, 1603351037, 1603351037, 1);
INSERT INTO `y3_auth_employee` VALUES (75, '122223', 1, 'xa1223', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 1, 'xa1223', '11111111111', '441622199501230122', 1, 1, 1, 1, 1, 1, 1, 0, 1603351080, 1603351080, 1);
INSERT INTO `y3_auth_employee` VALUES (76, '1222234', 1, 'xa12234', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 20, 'xa12234', '11111111111', '441622199501230122', 1, 1, 1, 1, 1, 1, 1, 0, 1603351160, 1603351160, 1);
INSERT INTO `y3_auth_employee` VALUES (77, '12222344', 1, 'xa122344', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 1, 'xa122344', '11111111111', '441622199501230122', 1, 1, 1, 1, 1, 1, 1, 0, 1603351206, 1603351206, 1);
INSERT INTO `y3_auth_employee` VALUES (78, 'youku', 0, 'youku', 'Fsw0j6mIysH2EdwDckg0DA==', 0, 0, 0, 0, '', '13112341234', '', 2, 1, 1, 1, 1, 0, 1, 0, 1603354477, 1603354477, 1);
INSERT INTO `y3_auth_employee` VALUES (79, '11', 1, 'admin11', 'Fsw0j6mIysH2EdwDckg0DA==', 2, 0, 1, 15, '蓝蓝33', '13211111111', '11010119900307053X', 1, 1, 1, 2, 1, 1603497600, 2, 0, 1603436541, 1603436541, 1);
INSERT INTO `y3_auth_employee` VALUES (80, '112', 1, 'admin2', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 15, '蓝蓝', '13121111111', '11010119900307053X', 1, 1, 1, 2, 1, 1603497600, 2, 0, 1603439737, 1603439737, 1);
INSERT INTO `y3_auth_employee` VALUES (81, '1123', 1, 'admin23', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 15, '蓝蓝3', '13121111111', '11010119900307053X', 1, 1, 1, 2, 1, 1603497600, 2, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_employee` VALUES (82, '11232', 1, 'admin223', 'Fsw0j6mIysH2EdwDckg0DA==', 1, 0, 1, 15, '蓝蓝32', '13121111111', '11010119900307053X', 1, 1, 1, 2, 1, 1603497600, 2, 0, 1603505780, 1603505780, 1);
COMMIT;

-- ----------------------------
-- Table structure for y3_auth_menu
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_menu`;
CREATE TABLE `y3_auth_menu` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '栏目名字',
  `sort` int NOT NULL DEFAULT '0' COMMENT '排序',
  `add_time` int unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `update_time` int unsigned NOT NULL DEFAULT '0',
  `company_id` int unsigned NOT NULL DEFAULT '0' COMMENT '公司id',
  `pid` int unsigned NOT NULL DEFAULT '0' COMMENT '上级id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of y3_auth_menu
-- ----------------------------
BEGIN;
INSERT INTO `y3_auth_menu` VALUES (1, '运营', 1, 1601178957, 1607498915, 1, 0);
INSERT INTO `y3_auth_menu` VALUES (2, '人员管理', 1, 1601186823, 0, 1, 0);
INSERT INTO `y3_auth_menu` VALUES (3, '财务', 2, 1601186837, 1601198010, 1, 0);
INSERT INTO `y3_auth_menu` VALUES (8, '测试', 0, 0, 0, 1, 3);
INSERT INTO `y3_auth_menu` VALUES (9, '技术', 0, 0, 0, 1, 8);
COMMIT;

-- ----------------------------
-- Table structure for y3_auth_menu_component
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_menu_component`;
CREATE TABLE `y3_auth_menu_component` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int unsigned NOT NULL DEFAULT '0' COMMENT '栏目id',
  `component_id` int unsigned DEFAULT '0' COMMENT '功能id',
  `company_id` int unsigned NOT NULL DEFAULT '0' COMMENT '公司id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of y3_auth_menu_component
-- ----------------------------
BEGIN;
INSERT INTO `y3_auth_menu_component` VALUES (75, 1, 112, 1);
INSERT INTO `y3_auth_menu_component` VALUES (76, 1, 113, 1);
INSERT INTO `y3_auth_menu_component` VALUES (77, 2, 115, 1);
INSERT INTO `y3_auth_menu_component` VALUES (78, 3, 121, 1);
INSERT INTO `y3_auth_menu_component` VALUES (79, 8, 122, 1);
INSERT INTO `y3_auth_menu_component` VALUES (80, 8, 123, 1);
INSERT INTO `y3_auth_menu_component` VALUES (81, 9, 124, 1);
COMMIT;

-- ----------------------------
-- Table structure for y3_auth_module
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_module`;
CREATE TABLE `y3_auth_module` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '模块名',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '交互地址',
  `appkey` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '密匙',
  `aes_iv` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'aes加密向量',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图标',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of y3_auth_module
-- ----------------------------
BEGIN;
INSERT INTO `y3_auth_module` VALUES (1, 'OA模块', '192.168.0.57:9501', '123', '', '');
INSERT INTO `y3_auth_module` VALUES (2, 'ERP模块', '192.168.0.57:9501', '123', '', '');
INSERT INTO `y3_auth_module` VALUES (3, '库存模块', '192.168.0.57:9501', '', '', '');
COMMIT;

-- ----------------------------
-- Table structure for y3_auth_organization
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_organization`;
CREATE TABLE `y3_auth_organization` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int unsigned NOT NULL DEFAULT '0' COMMENT '公司id',
  `employee_id` int unsigned NOT NULL DEFAULT '0' COMMENT '部门负责人id',
  `pid` int unsigned NOT NULL DEFAULT '0' COMMENT '上级关联id：0顶级',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '组织名称',
  `sort` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '排序',
  `grade` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '级别',
  `prefix_name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门面包屑前缀',
  `add_time` int unsigned NOT NULL DEFAULT '0' COMMENT '新增时间',
  `update_time` int unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `is_del` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '1正常 2删除 ',
  `cover_next` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '是否覆盖下级(部门权限用) 1不覆盖  2覆盖',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `company_id` (`company_id`) USING BTREE,
  KEY `employee_id` (`employee_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='组织架构表';

-- ----------------------------
-- Records of y3_auth_organization
-- ----------------------------
BEGIN;
INSERT INTO `y3_auth_organization` VALUES (1, 1, 1, 0, '广州搜游科技', 0, 1, '广州搜游科技-', 1599814013, 1603957493, 1, 2);
INSERT INTO `y3_auth_organization` VALUES (2, 1, 1, 1, '财务部', 0, 1, '财务部-', 1599894994, 1599899577, 1, 1);
INSERT INTO `y3_auth_organization` VALUES (3, 1, 1, 1, '人事部', 0, 1, '人事部-', 1599899067, 1602297473, 1, 2);
INSERT INTO `y3_auth_organization` VALUES (4, 0, 0, 1, '海外财务', 0, 1, '', 0, 0, 1, 1);
INSERT INTO `y3_auth_organization` VALUES (5, 0, 0, 0, '管理层', 0, 1, '', 0, 0, 1, 1);
INSERT INTO `y3_auth_organization` VALUES (6, 0, 0, 5, '微信事业部', 0, 1, '', 0, 0, 1, 1);
INSERT INTO `y3_auth_organization` VALUES (7, 0, 0, 5, '企业发展事业', 0, 1, '', 0, 0, 1, 1);
INSERT INTO `y3_auth_organization` VALUES (8, 0, 0, 5, '互动娱乐事业', 0, 1, '', 0, 0, 1, 1);
INSERT INTO `y3_auth_organization` VALUES (9, 0, 0, 5, '移动互联网事业', 0, 1, '总经办-财务部-', 0, 0, 1, 1);
INSERT INTO `y3_auth_organization` VALUES (10, 0, 0, 5, '网络媒体事业', 0, 1, '总经办-财务部-', 0, 0, 1, 1);
INSERT INTO `y3_auth_organization` VALUES (11, 0, 0, 5, '社交网络事业', 0, 1, '总经办-财务部-', 0, 0, 1, 1);
INSERT INTO `y3_auth_organization` VALUES (12, 1, 24, 2, '总经办', 4, 4, '广州搜游科技-财务部-', 0, 1603957493, 1, 1);
INSERT INTO `y3_auth_organization` VALUES (13, 1, 25, 12, '人事部', 5, 5, '广州搜游科技-财务部-', 0, 1603957493, 1, 1);
INSERT INTO `y3_auth_organization` VALUES (14, 1, 23, 6, '微信支付', 0, 1, '广州搜游科技-财务部-', 0, 1603957493, 1, 1);
INSERT INTO `y3_auth_organization` VALUES (15, 1, 23, 13, '熊安大文娱上市版', 6, 6, '广州搜游科技-财务部-', 1600335400, 1603957493, 1, 1);
INSERT INTO `y3_auth_organization` VALUES (17, 1, 23, 1, '新增部门2321', 4, 2, '广州搜游科技-新增部门2321-', 1602299149, 1603957493, 1, 1);
INSERT INTO `y3_auth_organization` VALUES (18, 1, 1, 17, '新增部门2', 1, 1, '广州搜游科技-新增部门2321-新增部门2-', 1602299175, 1603957493, 1, 1);
INSERT INTO `y3_auth_organization` VALUES (19, 1, 23, 17, '新增部门23', 2, 2, '广州搜游科技-新增部门2321-新增部门23-', 1602299237, 1603957493, 1, 1);
INSERT INTO `y3_auth_organization` VALUES (20, 1, 31, 17, '新增部门232', 3, 2, '广州搜游科技-新增部门2321-新增部门232-', 1602299400, 1603957493, 1, 1);
INSERT INTO `y3_auth_organization` VALUES (21, 1, 1, 20, '编辑一下部门', 1, 3, '广州搜游科技-新增部门2321-新增部门232-编辑一下部门-', 1603093313, 1603957493, 1, 1);
INSERT INTO `y3_auth_organization` VALUES (22, 1, 0, 0, '总经办', 1, 1, '', 1603354477, 1603354477, 1, 1);
COMMIT;

-- ----------------------------
-- Table structure for y3_auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_permission`;
CREATE TABLE `y3_auth_permission` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `identity` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '导航标识',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '级别递归名称',
  `sort_order` int unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `parent_id` int NOT NULL DEFAULT '0',
  `add_time` int unsigned NOT NULL DEFAULT '0',
  `update_time` int unsigned NOT NULL DEFAULT '0',
  `url` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'url地址',
  `is_web` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1web页面  2不是',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `url` (`url`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COMMENT='授权权限表';

-- ----------------------------
-- Records of y3_auth_permission
-- ----------------------------
BEGIN;
INSERT INTO `y3_auth_permission` VALUES (2, 'user_add', '用户添加', 1, 0, 1606958743, 0, 'user_add', 1);
INSERT INTO `y3_auth_permission` VALUES (3, 'user_1edit_save1', '用户信息修改', 3, 0, 1606958787, 1607481717, 'user_1edit_save', 1);
INSERT INTO `y3_auth_permission` VALUES (4, 'user_edit', '用户编辑', 1, 0, 1606989040, 0, 'user_edit', 1);
INSERT INTO `y3_auth_permission` VALUES (5, 'user_edit1', '用户编辑1', 1, 0, 1607064434, 0, 'user_edit1', 1);
INSERT INTO `y3_auth_permission` VALUES (6, 'auth/permission_add', '添加权限', 30, 38, 1607065155, 1607502832, 'auth/permission_add', 2);
INSERT INTO `y3_auth_permission` VALUES (23, 'CompanyInfo', '公司信息', 10, 0, 1603951657, 0, 'CompanyInfo', 1);
INSERT INTO `y3_auth_permission` VALUES (24, 'CompanyStructure', '公司组织架构', 20, 0, 1603951687, 0, 'CompanyStructure', 1);
INSERT INTO `y3_auth_permission` VALUES (25, 'EmployeeList', '员工列表', 10, 24, 1603951719, 0, 'EmployeeList', 1);
INSERT INTO `y3_auth_permission` VALUES (26, 'Structure', '组织架构', 20, 24, 1603951739, 0, 'Structure', 1);
INSERT INTO `y3_auth_permission` VALUES (27, 'DepartmentManager', '部门管理', 30, 24, 1603951769, 0, 'DepartmentManager', 1);
INSERT INTO `y3_auth_permission` VALUES (28, 'PositionManage', '职位管理', 40, 24, 1603951786, 0, 'PositionManage', 1);
INSERT INTO `y3_auth_permission` VALUES (29, 'ProjectManager', '项目管理', 30, 0, 1603951816, 0, 'ProjectManager', 1);
INSERT INTO `y3_auth_permission` VALUES (30, 'ProjectList', '项目列表', 10, 29, 1603951845, 0, 'ProjectList', 1);
INSERT INTO `y3_auth_permission` VALUES (31, 'FunctionManager', '功能管理', 40, 0, 1603951890, 0, 'FunctionManager', 1);
INSERT INTO `y3_auth_permission` VALUES (32, 'Function', '功能管理', 10, 31, 1603951921, 0, 'Function', 1);
INSERT INTO `y3_auth_permission` VALUES (33, 'FunctionSetting', '功能配置', 20, 31, 1603951954, 0, 'FunctionSetting', 1);
INSERT INTO `y3_auth_permission` VALUES (34, 'StaffColumnManage', '员工栏目管理', 30, 31, 1603951987, 0, 'StaffColumnManage', 1);
INSERT INTO `y3_auth_permission` VALUES (35, 'FunctionAuthManage', '功能权限管理', 40, 31, 1603952017, 0, 'FunctionAuthManage', 1);
INSERT INTO `y3_auth_permission` VALUES (36, 'AuthorityManager', '权限管理', 50, 0, 1603952069, 0, 'AuthorityManager', 1);
INSERT INTO `y3_auth_permission` VALUES (37, 'RoleManage', '角色管理', 10, 36, 1603952088, 0, 'RoleManage', 1);
INSERT INTO `y3_auth_permission` VALUES (38, '/auth/menu', '权限管理', 20, 36, 1603952110, 0, 'RightsManage', 1);
INSERT INTO `y3_auth_permission` VALUES (39, 'auth/permission_add11', '添加权限1', 1, 0, 1607481249, 0, 'auth/permission_add1', 1);
INSERT INTO `y3_auth_permission` VALUES (40, 'auth/permission_del', '删除权限2225dfd', 3, 0, 1607499962, 1607500039, 'auth/permission_del', 1);
INSERT INTO `y3_auth_permission` VALUES (41, 'auth/permission_add9', '添加权限', 1, 36, 1607502683, 0, 'auth/permission_add', 1);
INSERT INTO `y3_auth_permission` VALUES (42, 'auth/permission_addsdfsd', '添加权限', 1, 38, 1607503266, 0, 'auth/permission_addfadsfas', 2);
INSERT INTO `y3_auth_permission` VALUES (43, 'function_sub', '功能管理子集', 19, 35, 1607505538, 0, 'auth/permission_addfadsfas', 2);
COMMIT;

-- ----------------------------
-- Table structure for y3_auth_project
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_project`;
CREATE TABLE `y3_auth_project` (
  `id` int NOT NULL AUTO_INCREMENT,
  `company_id` int NOT NULL DEFAULT '0' COMMENT '公司id',
  `name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '项目名称',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：1开启、2关闭',
  `detail` varchar(10000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '项目描述',
  `period_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '周期类型 1长期项目 2指定周期项目',
  `start_time` int NOT NULL DEFAULT '0' COMMENT '项目开始时间 （period_type为2时才用此字段）',
  `end_time` int NOT NULL DEFAULT '0' COMMENT '项目结束时间 （period_type为2时才用此字段）',
  `add_time` int NOT NULL DEFAULT '0',
  `is_del` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1正常、2删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='项目表';

-- ----------------------------
-- Records of y3_auth_project
-- ----------------------------
BEGIN;
INSERT INTO `y3_auth_project` VALUES (3, 1, '项目3', 2, '', 1, 0, 0, 1601435574, 2);
INSERT INTO `y3_auth_project` VALUES (4, 1, '项目', 1, '', 1, 0, 0, 1601435633, 1);
INSERT INTO `y3_auth_project` VALUES (5, 1, '项目三', 1, '', 1, 0, 0, 1601435638, 1);
INSERT INTO `y3_auth_project` VALUES (6, 1, '项目四', 1, '', 1, 0, 0, 1601435643, 1);
INSERT INTO `y3_auth_project` VALUES (7, 1, '项目四', 1, '', 1, 0, 0, 1601435722, 1);
INSERT INTO `y3_auth_project` VALUES (8, 1, '项目四', 1, '', 1, 0, 0, 1601436425, 1);
INSERT INTO `y3_auth_project` VALUES (12, 1, '项目四88888', 1, '', 1, 0, 0, 1603162861, 1);
INSERT INTO `y3_auth_project` VALUES (13, 1, '项目四88888', 1, '', 1, 0, 0, 1603162893, 1);
INSERT INTO `y3_auth_project` VALUES (14, 1, '项目四88', 1, '', 1, 0, 0, 1603163093, 1);
INSERT INTO `y3_auth_project` VALUES (15, 1, '项目四88', 1, '', 1, 0, 0, 1603163211, 1);
INSERT INTO `y3_auth_project` VALUES (16, 1, '项目四88', 1, '', 1, 0, 0, 1603163354, 1);
INSERT INTO `y3_auth_project` VALUES (17, 1, '项目四88', 1, '', 1, 0, 0, 1603163505, 1);
INSERT INTO `y3_auth_project` VALUES (18, 1, '项目四88', 1, '', 1, 0, 0, 1603163689, 1);
INSERT INTO `y3_auth_project` VALUES (19, 1, '项目四88', 1, '', 1, 0, 0, 1603163839, 1);
INSERT INTO `y3_auth_project` VALUES (20, 1, '项目四88', 1, '', 1, 0, 0, 1603164194, 1);
INSERT INTO `y3_auth_project` VALUES (21, 1, '项目四88', 1, '', 1, 0, 0, 1603165095, 1);
INSERT INTO `y3_auth_project` VALUES (22, 1, '项目四88', 1, '', 1, 0, 0, 1603166051, 1);
INSERT INTO `y3_auth_project` VALUES (23, 1, '项目四88', 1, '', 1, 0, 0, 1603166107, 1);
INSERT INTO `y3_auth_project` VALUES (24, 1, '项目', 2, '', 1, 0, 0, 1603336151, 1);
INSERT INTO `y3_auth_project` VALUES (25, 1, '项目', 1, '', 1, 0, 0, 1603337345, 1);
INSERT INTO `y3_auth_project` VALUES (26, 1, '项目', 1, '', 1, 0, 0, 1603337438, 1);
INSERT INTO `y3_auth_project` VALUES (27, 1, '项目', 1, '', 1, 0, 0, 1603337685, 1);
INSERT INTO `y3_auth_project` VALUES (28, 1, '项目', 1, '', 1, 0, 0, 1603337983, 1);
INSERT INTO `y3_auth_project` VALUES (29, 1, '项目', 1, '', 1, 0, 0, 1603338126, 1);
INSERT INTO `y3_auth_project` VALUES (30, 1, '项目', 1, '', 1, 0, 0, 1603338258, 1);
INSERT INTO `y3_auth_project` VALUES (31, 1, '项目', 1, '', 1, 0, 0, 1603338313, 1);
INSERT INTO `y3_auth_project` VALUES (32, 1, '项目', 1, '', 1, 0, 0, 1603338500, 1);
INSERT INTO `y3_auth_project` VALUES (33, 1, '项目', 1, '', 1, 0, 0, 1603338594, 1);
INSERT INTO `y3_auth_project` VALUES (34, 1, '项目', 1, '', 1, 0, 0, 1603346602, 1);
INSERT INTO `y3_auth_project` VALUES (35, 1, '项目', 1, '', 1, 0, 0, 1603346661, 1);
INSERT INTO `y3_auth_project` VALUES (36, 1, '项目', 1, '', 1, 0, 0, 1603347515, 1);
INSERT INTO `y3_auth_project` VALUES (37, 1, '项目', 1, '', 1, 0, 0, 1607069032, 1);
INSERT INTO `y3_auth_project` VALUES (38, 1, '项目7777', 1, '', 1, 0, 0, 1607069045, 1);
INSERT INTO `y3_auth_project` VALUES (39, 1, '项目7777', 1, '啊是大家手机待机山东埃及说到底就是骄傲i上的骄傲的祭扫就是', 1, 0, 0, 1607069070, 1);
INSERT INTO `y3_auth_project` VALUES (40, 1, '项目7778', 1, '啊是大家手机待机山东埃及说到底就是骄傲i上的骄傲的祭扫就是', 2, 1607069070, 1607999070, 1607069131, 1);
INSERT INTO `y3_auth_project` VALUES (41, 1, '项目', 1, '', 2, 1607069000, 1607999010, 1607130953, 1);
COMMIT;

-- ----------------------------
-- Table structure for y3_auth_project_employee
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_project_employee`;
CREATE TABLE `y3_auth_project_employee` (
  `id` int NOT NULL AUTO_INCREMENT,
  `company_id` int NOT NULL DEFAULT '0' COMMENT '公司id',
  `project_id` int unsigned NOT NULL DEFAULT '0' COMMENT '项目id',
  `employee_id` int unsigned NOT NULL DEFAULT '0' COMMENT '员工id',
  `type` tinyint unsigned NOT NULL DEFAULT '1' COMMENT '关联类型：1查看 2参与',
  `organization_id` int NOT NULL DEFAULT '0' COMMENT '来源部门id',
  `add_time` int NOT NULL DEFAULT '0',
  `update_time` int NOT NULL DEFAULT '0',
  `is_del` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1正常、2删除',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `project_id` (`project_id`) USING BTREE,
  KEY `employee_id` (`employee_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='员工项目关联表';

-- ----------------------------
-- Records of y3_auth_project_employee
-- ----------------------------
BEGIN;
INSERT INTO `y3_auth_project_employee` VALUES (1, 1, 4, 76, 2, 0, 1603351160, 1603351160, 1);
INSERT INTO `y3_auth_project_employee` VALUES (2, 1, 4, 31, 2, 20, 1603351160, 1603351160, 1);
INSERT INTO `y3_auth_project_employee` VALUES (3, 1, 3, 76, 1, 0, 1603351160, 1603351160, 1);
INSERT INTO `y3_auth_project_employee` VALUES (4, 1, 4, 77, 2, 0, 1603351206, 1603351206, 1);
INSERT INTO `y3_auth_project_employee` VALUES (5, 1, 4, 23, 2, 17, 1603351206, 1603351206, 1);
INSERT INTO `y3_auth_project_employee` VALUES (6, 1, 4, 1, 2, 1, 1603351206, 1603351206, 1);
INSERT INTO `y3_auth_project_employee` VALUES (7, 1, 3, 77, 1, 0, 1603351206, 1603351206, 1);
INSERT INTO `y3_auth_project_employee` VALUES (8, 1, 4, 79, 2, 0, 1603436541, 1603436541, 1);
INSERT INTO `y3_auth_project_employee` VALUES (9, 1, 4, 23, 2, 15, 1603436541, 1603436541, 1);
INSERT INTO `y3_auth_project_employee` VALUES (10, 1, 4, 25, 2, 13, 1603436541, 1603436541, 1);
INSERT INTO `y3_auth_project_employee` VALUES (11, 1, 4, 24, 2, 12, 1603436541, 1603436541, 1);
INSERT INTO `y3_auth_project_employee` VALUES (12, 1, 4, 1, 2, 2, 1603436541, 1603436541, 1);
INSERT INTO `y3_auth_project_employee` VALUES (13, 1, 5, 79, 2, 0, 1603436541, 1603436541, 1);
INSERT INTO `y3_auth_project_employee` VALUES (14, 1, 5, 23, 2, 15, 1603436541, 1603436541, 1);
INSERT INTO `y3_auth_project_employee` VALUES (15, 1, 4, 80, 2, 0, 1603439737, 1603439737, 1);
INSERT INTO `y3_auth_project_employee` VALUES (16, 1, 5, 80, 2, 0, 1603439737, 1603439737, 1);
INSERT INTO `y3_auth_project_employee` VALUES (17, 1, 4, 81, 2, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (18, 1, 5, 81, 2, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (19, 1, 4, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (20, 1, 5, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (21, 1, 6, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (22, 1, 7, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (23, 1, 8, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (24, 1, 12, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (25, 1, 13, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (26, 1, 14, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (27, 1, 15, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (28, 1, 16, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (29, 1, 17, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (30, 1, 18, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (31, 1, 19, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (32, 1, 20, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (33, 1, 21, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (34, 1, 22, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (35, 1, 23, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (36, 1, 25, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (37, 1, 26, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (38, 1, 27, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (39, 1, 28, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (40, 1, 29, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (41, 1, 30, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (42, 1, 31, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (43, 1, 32, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (44, 1, 33, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (45, 1, 34, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (46, 1, 35, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (47, 1, 36, 81, 1, 0, 1603504779, 1603504779, 1);
INSERT INTO `y3_auth_project_employee` VALUES (48, 1, 4, 82, 2, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (49, 1, 5, 82, 2, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (50, 1, 4, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (51, 1, 5, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (52, 1, 6, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (53, 1, 7, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (54, 1, 8, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (55, 1, 12, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (56, 1, 13, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (57, 1, 14, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (58, 1, 15, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (59, 1, 16, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (60, 1, 17, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (61, 1, 18, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (62, 1, 19, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (63, 1, 20, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (64, 1, 21, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (65, 1, 22, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (66, 1, 23, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (67, 1, 25, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (68, 1, 26, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (69, 1, 27, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (70, 1, 28, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (71, 1, 29, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (72, 1, 30, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (73, 1, 31, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (74, 1, 32, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (75, 1, 33, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (76, 1, 34, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (77, 1, 35, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (78, 1, 36, 82, 1, 0, 1603505780, 1603505780, 1);
INSERT INTO `y3_auth_project_employee` VALUES (79, 1, 36, 25, 2, 13, 1603696477, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (80, 1, 36, 23, 2, 17, 1603696477, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (81, 1, 4, 1, 2, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (82, 1, 43, 1, 2, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (83, 1, 58, 1, 2, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (84, 1, 44, 1, 2, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (85, 1, 50, 1, 2, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (86, 1, 45, 1, 2, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (87, 1, 46, 1, 2, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (88, 1, 47, 1, 2, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (89, 1, 51, 1, 2, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (90, 1, 55, 1, 2, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (91, 1, 60, 1, 2, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (92, 1, 57, 1, 2, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (93, 1, 56, 1, 2, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (94, 1, 59, 1, 2, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (95, 1, 34, 1, 1, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (96, 1, 35, 1, 1, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (97, 1, 37, 1, 1, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (98, 1, 39, 1, 1, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (99, 1, 42, 1, 1, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (100, 1, 43, 1, 1, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (101, 1, 44, 1, 1, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (102, 1, 45, 1, 1, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (103, 1, 46, 1, 1, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (104, 1, 47, 1, 1, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (105, 1, 50, 1, 1, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (106, 1, 51, 1, 1, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (107, 1, 55, 1, 1, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (108, 1, 56, 1, 1, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (109, 1, 57, 1, 1, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (110, 1, 58, 1, 1, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (111, 1, 59, 1, 1, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (112, 1, 60, 1, 1, 0, 1607044247, 1607044247, 1);
INSERT INTO `y3_auth_project_employee` VALUES (113, 1, 37, 1, 2, 1, 1607069032, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (114, 1, 37, 1, 2, 2, 1607069032, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (115, 1, 37, 23, 2, 15, 1607069032, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (116, 1, 37, 23, 2, 17, 1607069032, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (117, 1, 37, 31, 2, 0, 1607069032, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (118, 1, 37, 46, 2, 0, 1607069032, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (119, 1, 37, 29, 1, 0, 1607069032, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (120, 1, 37, 30, 1, 0, 1607069032, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (121, 1, 37, 79, 1, 0, 1607069032, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (122, 1, 37, 80, 1, 0, 1607069032, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (123, 1, 37, 81, 1, 0, 1607069032, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (124, 1, 37, 82, 1, 0, 1607069032, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (125, 1, 38, 1, 2, 1, 1607069045, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (126, 1, 38, 1, 2, 2, 1607069045, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (127, 1, 38, 23, 2, 15, 1607069045, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (128, 1, 38, 23, 2, 17, 1607069045, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (129, 1, 38, 31, 2, 0, 1607069045, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (130, 1, 38, 46, 2, 0, 1607069045, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (131, 1, 38, 29, 1, 0, 1607069045, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (132, 1, 38, 30, 1, 0, 1607069045, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (133, 1, 38, 79, 1, 0, 1607069045, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (134, 1, 38, 80, 1, 0, 1607069045, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (135, 1, 38, 81, 1, 0, 1607069045, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (136, 1, 38, 82, 1, 0, 1607069045, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (137, 1, 39, 1, 2, 1, 1607069070, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (138, 1, 39, 1, 2, 2, 1607069070, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (139, 1, 39, 23, 2, 15, 1607069070, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (140, 1, 39, 23, 2, 17, 1607069070, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (141, 1, 39, 31, 2, 0, 1607069070, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (142, 1, 39, 46, 2, 0, 1607069070, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (143, 1, 39, 29, 1, 0, 1607069070, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (144, 1, 39, 30, 1, 0, 1607069070, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (145, 1, 39, 79, 1, 0, 1607069070, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (146, 1, 39, 80, 1, 0, 1607069070, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (147, 1, 39, 81, 1, 0, 1607069070, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (148, 1, 39, 82, 1, 0, 1607069070, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (149, 1, 40, 1, 2, 1, 1607069131, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (150, 1, 40, 1, 2, 2, 1607069131, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (151, 1, 40, 23, 2, 15, 1607069131, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (152, 1, 40, 23, 2, 17, 1607069131, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (153, 1, 40, 31, 2, 0, 1607069131, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (154, 1, 40, 46, 2, 0, 1607069131, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (155, 1, 40, 29, 1, 0, 1607069131, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (156, 1, 40, 30, 1, 0, 1607069131, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (157, 1, 40, 79, 1, 0, 1607069131, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (158, 1, 40, 80, 1, 0, 1607069131, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (159, 1, 40, 81, 1, 0, 1607069131, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (160, 1, 40, 82, 1, 0, 1607069131, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (161, 1, 41, 1, 2, 1, 1607130953, 1607131112, 2);
INSERT INTO `y3_auth_project_employee` VALUES (162, 1, 41, 1, 2, 2, 1607130953, 1607131112, 2);
INSERT INTO `y3_auth_project_employee` VALUES (163, 1, 41, 23, 2, 15, 1607130953, 1607131112, 2);
INSERT INTO `y3_auth_project_employee` VALUES (164, 1, 41, 23, 2, 17, 1607130953, 1607131112, 2);
INSERT INTO `y3_auth_project_employee` VALUES (165, 1, 41, 31, 2, 0, 1607130953, 1607131112, 2);
INSERT INTO `y3_auth_project_employee` VALUES (166, 1, 41, 46, 2, 0, 1607130953, 1607131112, 2);
INSERT INTO `y3_auth_project_employee` VALUES (167, 1, 41, 29, 1, 0, 1607130953, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (168, 1, 41, 30, 1, 0, 1607130953, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (169, 1, 41, 79, 1, 0, 1607130953, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (170, 1, 41, 80, 1, 0, 1607130953, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (171, 1, 41, 81, 1, 0, 1607130953, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (172, 1, 41, 82, 1, 0, 1607130953, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (173, 1, 41, 25, 2, 13, 1607131112, 0, 1);
INSERT INTO `y3_auth_project_employee` VALUES (174, 1, 41, 23, 2, 17, 1607131112, 0, 1);
COMMIT;

-- ----------------------------
-- Table structure for y3_auth_project_organization
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_project_organization`;
CREATE TABLE `y3_auth_project_organization` (
  `id` int NOT NULL AUTO_INCREMENT,
  `company_id` int NOT NULL DEFAULT '0' COMMENT '公司id',
  `project_id` int NOT NULL DEFAULT '0' COMMENT '项目id',
  `organization_id` int NOT NULL DEFAULT '0' COMMENT '部门id',
  `add_time` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='项目-部门表';

-- ----------------------------
-- Records of y3_auth_project_organization
-- ----------------------------
BEGIN;
INSERT INTO `y3_auth_project_organization` VALUES (2, 1, 4, 1, 1603351206);
INSERT INTO `y3_auth_project_organization` VALUES (3, 1, 5, 15, 1603436541);
INSERT INTO `y3_auth_project_organization` VALUES (4, 1, 36, 17, 1603696477);
INSERT INTO `y3_auth_project_organization` VALUES (5, 1, 36, 13, 1603696477);
INSERT INTO `y3_auth_project_organization` VALUES (6, 1, 4, 61, 1607044247);
INSERT INTO `y3_auth_project_organization` VALUES (7, 1, 43, 61, 1607044247);
INSERT INTO `y3_auth_project_organization` VALUES (8, 1, 58, 61, 1607044247);
INSERT INTO `y3_auth_project_organization` VALUES (9, 1, 44, 61, 1607044247);
INSERT INTO `y3_auth_project_organization` VALUES (10, 1, 50, 61, 1607044247);
INSERT INTO `y3_auth_project_organization` VALUES (11, 1, 45, 61, 1607044247);
INSERT INTO `y3_auth_project_organization` VALUES (12, 1, 46, 61, 1607044247);
INSERT INTO `y3_auth_project_organization` VALUES (13, 1, 47, 61, 1607044247);
INSERT INTO `y3_auth_project_organization` VALUES (14, 1, 51, 61, 1607044247);
INSERT INTO `y3_auth_project_organization` VALUES (15, 1, 55, 61, 1607044247);
INSERT INTO `y3_auth_project_organization` VALUES (16, 1, 60, 61, 1607044247);
INSERT INTO `y3_auth_project_organization` VALUES (17, 1, 57, 61, 1607044247);
INSERT INTO `y3_auth_project_organization` VALUES (18, 1, 56, 61, 1607044247);
INSERT INTO `y3_auth_project_organization` VALUES (19, 1, 59, 61, 1607044247);
INSERT INTO `y3_auth_project_organization` VALUES (20, 1, 37, 1, 1607069032);
INSERT INTO `y3_auth_project_organization` VALUES (21, 1, 38, 1, 1607069045);
INSERT INTO `y3_auth_project_organization` VALUES (22, 1, 39, 1, 1607069070);
INSERT INTO `y3_auth_project_organization` VALUES (23, 1, 40, 1, 1607069131);
INSERT INTO `y3_auth_project_organization` VALUES (25, 1, 41, 17, 1607131112);
INSERT INTO `y3_auth_project_organization` VALUES (26, 1, 41, 13, 1607131112);
COMMIT;

-- ----------------------------
-- Table structure for y3_auth_role
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_role`;
CREATE TABLE `y3_auth_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `company_id` int unsigned NOT NULL DEFAULT '0' COMMENT '公司id',
  `add_time` int unsigned NOT NULL DEFAULT '0',
  `update_time` int unsigned NOT NULL DEFAULT '0',
  `is_del` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 正常 2 删除',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `company_id` (`company_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Table structure for y3_auth_role_component
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_role_component`;
CREATE TABLE `y3_auth_role_component` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `component_id` int unsigned NOT NULL DEFAULT '0' COMMENT '组件id',
  `company_id` int unsigned NOT NULL DEFAULT '0' COMMENT '公司id',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `component_id` (`component_id`) USING BTREE,
  KEY `company_id` (`company_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for y3_auth_upload
-- ----------------------------
DROP TABLE IF EXISTS `y3_auth_upload`;
CREATE TABLE `y3_auth_upload` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `md5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '资源文件的md5',
  `content_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'image/jpeg' COMMENT '数据类型',
  `chunk_size` int unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `origin_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '源文件名称',
  `add_time` int unsigned NOT NULL DEFAULT '0' COMMENT '上传时间',
  `file_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '资源内容路径',
  `user_id` int unsigned NOT NULL DEFAULT '0' COMMENT '上传者',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '上传者',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `md5` (`md5`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='资源文件md5';

-- ----------------------------
-- Records of y3_auth_upload
-- ----------------------------
BEGIN;
INSERT INTO `y3_auth_upload` VALUES (6, '674c0d72f58c628dfc9a3ab57f3b2bda', 'image/png', 2885, 'connections_active.png', 1603330971, 'images/20201022/5f90e39b5eb4f.png', 1, '1');
INSERT INTO `y3_auth_upload` VALUES (7, '850d4353cd7b65e0d8dcb57f49537174', 'image/png', 1188, 'more_active.png', 1603334027, 'images/20201022/5f90ef8bf1c85.png', 1, '1');
INSERT INTO `y3_auth_upload` VALUES (8, '92c1e879b157bde3a61436bbc2860c73', 'image/jpeg', 167469, '广东.jpg', 1603335140, 'images/20201022/5f90f3e489174.jpg', 1, '小北');
INSERT INTO `y3_auth_upload` VALUES (9, 'cdd56e3d6dfe0aa701624c5aa393dbd1', 'image/jpeg', 1314453, '2020-10-1 1929 1.jpg', 1603335539, 'images/20201022/5f90f5732dc6b.jpg', 1, '小北');
INSERT INTO `y3_auth_upload` VALUES (10, '7100fd496ceeec0a7033035dace07bea', 'image/png', 1191, 'more.png', 1603335858, 'images/20201022/5f90f6b27e35a.png', 1, '1');
INSERT INTO `y3_auth_upload` VALUES (11, 'e5a1bcef429e05675dba780562d4acdc', 'image/png', 1732, 'index.png', 1603336104, 'images/20201022/5f90f7a83e65c.png', 1, '1');
INSERT INTO `y3_auth_upload` VALUES (13, 'adc74b3e7e8f501592903bbc887ab008', 'image/png', 1730, 'index_active.png', 1603336895, 'images/20201022/5f90fabf4ef17.png', 1, '1');
INSERT INTO `y3_auth_upload` VALUES (14, '54c7eef86d21606fc23ee62972301be0', 'image/png', 387212, '上海名媛.png', 1603338796, 'images/20201022/5f91022c50dc4.png', 1, '222');
INSERT INTO `y3_auth_upload` VALUES (15, '848fdb9cf9abbc21b2ceb31049013a6c', 'image/png', 199136, '给心.png', 1603338992, 'images/20201022/5f9102f01697a.png', 1, '222');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
