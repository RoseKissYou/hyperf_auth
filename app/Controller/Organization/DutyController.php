<?php

declare (strict_types = 1);

namespace App\Controller\Organization;

use App\Controller\AbstractController;
use App\Service\Organziation\DutyService;
use App\Validate\DutyValidation;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Context;

class DutyController extends AbstractController
{

    /**
     * @Inject
     * @var DutyService
     */
    protected $service;

    public function index()
    {
        $params = $this->request->all();
        $params['page'] = $this->request->input('page', 1);
        $params['pageSize'] = $this->request->input('pageSize', 15);
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $result = $this->service->list($params);
        return $this->success($result['msg'], $result['data'], $result['totalItem']);
    }

    function list() {
        $params = make(DutyValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $result = $this->service->list($params);
        return $this->success($result['msg'], $result['data']);
    }

    public function addDuty()
    {
        $params = make(DutyValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $result = $this->service->addDuty($params);
        return $this->success($result['msg']);
    }

    public function editDuty()
    {
        $params = make(DutyValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $result = $this->service->editDuty($params);
        return $this->success($result['msg']);
    }

    public function delDuty()
    {
        $params = make(DutyValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        $result = $this->service->delDuty($params);
        return $this->success($result['msg']);
    }
}
