<?php


namespace App\Constants;

/**
 * 默认值
 * Class DefaultValue
 * @package App\Constants
 */
class RedisKey
{
    /**
     * 图形验证码
     * （eg：verify_code_img:$key）
     */
    const VERIFY_CODE_IMG = 'verify_code_img:';

    /*
     * 授权码验证
     */
    const AUTH_CODE_KEY = 'auth_code_key';

    /*
     * 用户权限redis_key
     */
    const USER_AUTH_KEY = 'user_auth_key_';
}
