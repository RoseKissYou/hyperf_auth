<?php

declare (strict_types = 1);

namespace App\Controller\Organization;

use App\Controller\AbstractController;
use App\Service\Organziation\OrganziationService;
use App\Validate\OrganizationValidation;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Context;

class IndexController extends AbstractController
{

    /**
     * @Inject
     * @var OrganziationService
     */
    protected $service;

    public function index()
    {

        $params['company_id'] = Context::get('jwt_token')['company_id'] ?? 1;
        $result = $this->service->list($params);
        return $this->success($result['msg'], $result['data']);
    }

    function list() {
        $params = $this->request->all();
        $params['company_id'] = Context::get('jwt_token')['company_id'] ?? 1;
        $params['list_type'] = 1;
        $result = $this->service->list($params);
        return $this->success($result['msg'], $result['data']);
    }

    public function employee()
    {
        $params['company_id'] = Context::get('jwt_token')['company_id'] ?? 1;
        $params['list_type'] = 2;
        $result = $this->service->list($params);
        return $this->success($result['msg'], $result['data']);
    }

    public function addOrganization()
    {
        $params = make(OrganizationValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $params['employee_id'] = $this->request->input('employee_id', 0);
        $result = $this->service->addOrganization($params);
        return $this->success($result['msg']);
    }

    public function editOrganization()
    {
        $params = make(OrganizationValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        $params['company_id'] = Context::get('jwt_token')['company_id'];
        $params['employee_id'] = $this->request->input('employee_id', 0);
        $result = $this->service->editOrganization($params);
        return $this->success($result['msg']);
    }

    public function delOrganization()
    {
        $params = make(OrganizationValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        $result = $this->service->delOrganization($params);
        return $this->success($result['msg']);
    }

    public function sort()
    {
        $params = make(OrganizationValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        $result = $this->service->sort($params);
        return $this->success($result['msg']);
    }

    public function treeList()
    {
        $params['company_id'] = (int) Context::get('jwt_token')['company_id'];
        $result = $this->service->treeList($params);
        return $this->success($result['msg'], $result['data']);
    }

    // public function project()
    // {

    //     // $params = make(OrganizationValidation::class)->check($this->request->all(), __FUNCTION__)->response();
    //     $params['company_id'] = Context::get('jwt_token')['company_id'];
    //     $result = $this->service->project($params);
    //     return $this->success($result['msg'], $result['data']);
    // }

}
