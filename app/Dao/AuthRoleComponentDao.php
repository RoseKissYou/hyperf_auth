<?php

declare (strict_types = 1);

namespace App\Dao;

class AuthRoleComponentDao
{
    use DaoTrait;

    public function __construct()
    {
        $this->driver = __CLASS__;
    }

}
