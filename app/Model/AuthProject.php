<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id
 * @property int $company_id 公司id
 * @property string $name 项目名称
 * @property int $status 状态：1开启、2关闭
 * @property datetime:Y-m-d H:i:s $add_time
 * @property int $is_del 1正常、2删除
 */
class AuthProject extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'auth_project';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'company_id', 'name', 'status', 'add_time', 'is_del'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'int', 'status' => 'integer', 'add_time' => 'datetime:Y-m-d H:i:s', 'start_time' => 'datetime:Y-m-d H:i:s', 'end_time' => 'datetime:Y-m-d H:i:s', 'is_del' => 'integer', 'company_id' => 'integer'];
    const IS_STATUS = 1;
    //开启
    const NOT_STATUS = 2;
    //关闭
    const NOT_DEL = 1;
    //正常
    const DEL = 2;
    //删除

    const LONG_PERIOD_TYPE = 1;//长期项目

    const SHORT_PERIOD_TYPE = 2;//周期项目
}