<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

namespace App\Validate;

class MenuValidation extends BaseValidation
{
    // 调用案例 check支持自定义规则

    // make(ModuleValidation::class)->check($this->request->all(),__FUNCTION__)

    //返回响应值的  ->response()

    //返回boole   ->get()

    //返回报错信息  ->getError()

    //验证规则
    protected $rule = [
        'id' => 'required',
        'menu_name' => 'required',
        'component_id' => 'required',
        'menu_id' => 'required',
        'old_menu_id' => 'required',
        'new_menu_id' => 'required',
        'pid' => 'required',
    ];

    //自定义验证信息
    protected $message = [
        'id.required' => '参数不对',
        'menu_name.required' => '栏目名字必填',
        'component_id.required' => '请选择功能',
        'menu_id.required' => '请选择栏目',
        'new_menu_id.required' => '请选择新栏目',
        'old_menu_id.required' => '请选择旧栏目',
        'pid.required' => '上级id必填',
    ];

    //自定义场景
    protected $scene = [
        'add' => 'menu_name,pid',
        'edit' => 'id,menu_name,pid',
        'del' => 'id',
        'menuAddComponent' => 'menu_id,component_id',
        'moveMenuComponent' => 'old_menu_id,new_menu_id',
        'menuMoveComponent' => 'old_menu_id,new_menu_id,component_id'
    ];
}
