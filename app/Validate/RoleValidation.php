<?php

declare (strict_types = 1);

namespace App\Validate;

class RoleValidation extends BaseValidation
{
    // 调用案例 check支持自定义规则

    // make(TaskValidation::class)->check($this->request->all(),__FUNCTION__)

    //返回响应值的  ->response()

    //返回boole   ->get()

    //返回报错信息  ->getError()

    //验证规则
    protected $rule = [
        'name' => 'required|max:10',
        'id' => 'required|integer',
        'component_ids' => 'required',
        'role_id' => 'required|integer',
        'permission_id' => 'required',
    ];

    //自定义验证信息
    protected $message = [
        'name.required' => '名称不能为空',
        'name.max' => '角色名称不能超过10个字符',
        'id.required' => 'ID不能为空',
        'permission_id.required' => '权限id不正确',
        'component_id.required' => '权限模块id不正确',
        'role_id.required' => '角色id 不能为空',

    ];

    //自定义场景
    protected $scene = [
        'roleAdd' => ['name', 'permission_id'],
        'roleShow' => ['id'],
        'roleEdit' => ['id', 'name', 'permission_id'],
        'add' => ['name'],
        'edit' => ['role_id', 'name'],
        'del' => ['role_id'],
        'set' => ['role_id', 'component_ids'],
        'roleComponent' => ['role_id'],
    ];
}
