<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Controller;

use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;
use Hyperf\Utils\Codec\Json;
use Psr\Container\ContainerInterface;
use App\Utils\ApiUtils;
use App\Constants\ErrorCode;

abstract class AbstractController
{
    /**
     * @Inject
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @Inject
     * @var RequestInterface
     */
    protected $request;

    /**
     * @Inject
     * @var ResponseInterface
     */
    protected $response;

    public function success($msg = '获取成功', $data = [], $total = 1)
    {
        return $this->response->json(ApiUtils::send(ErrorCode::SUCCESS, $msg, $data, $total));
    }

    public function error($code, $msg)
    {
        return $this->response->json(ApiUtils::send($code, $msg));
    }

    public function reply($code, $msg = '获取成功', $data = "", $total = 1)
    {
        $data = $data ? Json::decode($data) : [];
        return $this->response->json(ApiUtils::send($code, $msg, $data, $total));
    }
}
