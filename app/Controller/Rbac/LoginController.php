<?php

declare(strict_types=1);

namespace App\Controller\Rbac;

use App\Controller\AbstractController;
use App\Service\Rbac\LoginService;
use App\Validate\LoginValidation;
use Hyperf\Di\Annotation\Inject;

class LoginController extends AbstractController
{
    /**
     * @Inject
     * @var LoginService
     */
    protected $loginService;

    /*
     * 设置账号密码
     */
    public function account()
    {
        $data = $this->request->inputs(['company','account','password','phone','verification','key','content']);
        make(LoginValidation::class)->check($data, __FUNCTION__)->response();
        return $this->success('ok',$this->loginService->account($data));
    }
    /*
     * 校验授权码
     */
    public function auth()
    {
        $data = $this->request->input('auth');
        make(LoginValidation::class)->check($this->request->all(), __FUNCTION__)->response();
        return $this->success('ok',$this->loginService->auth($data));
    }

    /*
     * 登录
     */
    public function login()
    {
        $data = $this->request->inputs(['name','password','key','code']);
        var_dump('登录',$data);
        make(LoginValidation::class)->check($data, __FUNCTION__)->response();
        return $this->success('ok',$this->loginService->login($data));
    }

    public function verify()
    {
        return $this->success('ok',$this->loginService->verify());
    }
}