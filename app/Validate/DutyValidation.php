<?php

declare (strict_types = 1);

namespace App\Validate;

class DutyValidation extends BaseValidation
{
    // 调用案例 check支持自定义规则

    // make(TaskValidation::class)->check($this->request->all(),__FUNCTION__)

    //返回响应值的  ->response()

    //返回boole   ->get()

    //返回报错信息  ->getError()

    //验证规则
    protected $rule = [
        'name' => 'required',
        'pid' => 'required|integer',
        'duty_id' => 'required|integer',
        'list_type' => 'required|in:1,2,3',
        'operation_confirm' => 'required|in:1,2',

    ];

    //自定义验证信息
    protected $message = [
        'pid.required' => '上级id不能为空',
        'name.required' => '名称不能为空',
        'duty_id.required' => '岗位id不能为空',
        'list_type.required' => '取值范围设置不能为空',
        'list_type.in' => '取值范围错误',
        'operation_confirm.required' => '确认 必须',
        'operation_confirm.in' => '确认 取值错误',
    ];

    //自定义场景
    protected $scene = [
        'list' => ['list_type'],
        'addDuty' => ['pid', 'name'],
        'editDuty' => ['pid', 'duty_id', 'name'],
        'delDuty' => ['duty_id', 'operation_confirm'],
    ];
}
