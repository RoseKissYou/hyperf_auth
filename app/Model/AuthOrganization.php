<?php

declare (strict_types = 1);
namespace App\Model;

use Hyperf\Database\Model\Builder;
use Hyperf\DbConnection\Model\Model;

/**
 * 组织信息表
 *
 * @Author czm
 * @DateTime 2020-09-09
 */
class AuthOrganization extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'auth_organization';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'company_id', 'employee_id', 'grade', 'sort', 'pid', 'add_time', 'update_time', 'is_del', 'cover_next', 'prefix_name'];
    protected $guarded = ['id'];
    protected $hidden = ['is_del'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'add_time' => 'datetime:Y-m-d H:i:s',
        'update_time' => 'datetime:Y-m-d H:i:s',
    ];

    const CREATED_AT = 'add_time';
    const UPDATED_AT = 'update_time';
    protected $dateFormat = 'U';

    protected function boot(): void
    {
        $this->bootTraits();
        //model 软删除过滤 避免每次查询都添加is_del条件 去除条件添加withoutGlobalScope
        $this->addGlobalScope('is_del', function (Builder $builder) {
            $builder->where($this->table . '.is_del', 1);
        });
    }

    const ORGANIZATION_RANK_LIMIT = 100; //最高部门限制
    const ORGANIZATION_INIT_GRADE = 1; //初始等级

    const COVER_NO = 1; //不覆盖下级
    const COVER_YES = 2; //覆盖下级

    public function hasEmployee()
    {
        return $this->hasMany(AuthEmployee::class, 'organization_id', 'id');
    }

    public function hasEmployeeOne()
    {
        return $this->hasOne(AuthEmployee::class, 'id', 'employee_id');
    }

    public function hasPrincipalProject()
    {
        return $this->belongsToMany(AuthProject::class, 'auth_project_organization', 'organization_id', 'project_id');
    }
}
