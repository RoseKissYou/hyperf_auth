<?php

declare (strict_types = 1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Controller;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Lib\Niutrans\Xunfei;
use App\Service\Auth\ModuleService;
use App\Utils\GRPCClient;
use Hyperf\Utils\Codec\Json;
use Pb\Params;
use App\Utils\ApiUtils;

class IndexController extends AbstractController
{
    public function index()
    {
        $params = $this->request->inputs(['name','pwd','pwd2']);
        $pwd = ApiUtils::aesEnPassword($params['pwd']);
        $pwd2 = ApiUtils::aesDePassword($params['pwd2']);
        var_dump($pwd,$pwd2);
        return [
            'data'=>$params
        ];
        // $arrToken = \App\Lib\JwtToken::authorizations(
        //     [
        //         "uid" => 1, //记录的userid的信息
        //         "id" => 1, //记录的userid的信息
        //         'account' => 'admin', // 用户账号
        //         'name' => '小bei',
        //         'company_id' => '1',
        //     ],
        //     config('web.admin_jwt_key')
        // );
        // return [
        //     'token' => $arrToken,
        // ];
        $client = new GRPCClient();
        $request = new Params();
        $request->setRequest(Json::encode(['to_module_name' => 'auth','from_module_name'=>'auth','name'=>'123','task_no'=>'fjlsdj534534','content'=>['class_name'=>'\\App\\Service\\Api\\TaskService','method'=>'test','data'=>['test'=>'123']]]));
        $request->setController(\App\Service\Api\TaskService::class);
        $request->setMethod('addTask');

        list($reply, $status) = $client->curdClient($request);
        if ($reply->getErrCode() == 0) {
            $data = $reply->getData();
            return $data;
        } else {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, $reply->getMsg());
        }
        return 11;
        $arrToken = \App\Lib\JwtToken::authorizations(
            [
                "uid" => 1, //记录的userid的信息
                "id" => 1, //记录的userid的信息
                'account' => 'admin', // 用户账号
                'name' => '小bei',
                'company_id' => '1',
            ],
            config('web.admin_jwt_key')
        );
        return [
            'token' => $arrToken,
        ];
        $obj = new Xunfei();
        return $obj->xfyun(['text' => '中华人民共和国于1949年成立', 'to' => 'en']);

    }
}
