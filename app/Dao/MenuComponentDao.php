<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Dao;

use App\Model\MenuComponent;

class MenuComponentDao
{
    //添加数据
    public function add(array $data)
    {
        return MenuComponent::query()->insertGetId($data);
    }

    //添加数据多条
    public function insert(array $data)
    {
        return MenuComponent::query()->insert($data);
    }

    //更新
    public function update($where, $data)
    {
        return MenuComponent::query()->where($where)->update($data);
    }

    //删除
    public function delete($where, $whereIn = [], $key = 'component_id')
    {
        return MenuComponent::query()->where($where)->when($whereIn, function ($query, $whereIn) use ($key){
            return $query->whereIn($key, $whereIn);
        })->delete();
    }


    //通过条件搜索单一条
    public function searchFirst($where, $columns = ['*'])
    {
        return MenuComponent::query()->where($where)->first($columns);
    }

    public function searchAll($where = [], $whereIn = [], $columns = ['*'])
    {
        return MenuComponent::query()->where($where)->when($whereIn, function ($query, $whereIn) {
            return $query->whereIn('component_id', $whereIn);
        })->get($columns)->toArray();
    }
    //找出功能id
    public function searchMoudleId($where = [], $whereIn = [])
    {
        return MenuComponent::query()->where($where)->when($whereIn, function ($query, $whereIn) {
            return $query->whereIn('menu_id', $whereIn);
        })->pluck('component_id')->toArray();
    }

    //通过功能id找出功能id
    public function searchMoudleIdByModule($where = [], $whereIn = [])
    {
        return MenuComponent::query()->where($where)->when($whereIn, function ($query, $whereIn) {
            return $query->whereIn('component_id', $whereIn);
        })->pluck('component_id')->toArray();
    }

    //通过功能id找出栏目id
    public function searchMenuIdByModule($where = [], $whereIn = [])
    {
        return MenuComponent::query()->where($where)->when($whereIn, function ($query, $whereIn) {
            return $query->whereIn('component_id', $whereIn);
        })->pluck('menu_id')->toArray();
    }
}
