<?php

declare(strict_types=1);

namespace App\Model;

use Hyperf\DbConnection\Model\Model;

class TaskFail extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'auth_task_fail';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['to_module_name','from_module_name','task_id','content','add_time','task_no','num'];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'add_time' => 'datetime:Y-m-d H:i:s',
    ];

    public $timestamps = false;

    
}
