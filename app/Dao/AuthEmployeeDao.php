<?php

declare (strict_types = 1);

namespace App\Dao;

use App\Constants\ErrorCode;
use App\Exception\BusinessException;
use App\Model\AuthAdminRolePermission;
use App\Model\AuthEmployee;
use App\Model\AuthOrganization;
use App\Model\AuthPermission;
use App\Utils\ApiUtils;

class AuthEmployeeDao
{
    use DaoTrait;
    public function __construct()
    {
        $this->driver = __CLASS__;
    }

    public function changeDriver(\Hyperf\Database\Model\Model $model)
    {
        $this->driver = $model;
        return $this;
    }

    public function resetDriver()
    {
        $this->driver = __CLASS__;
        return $this;
    }

    public function searchByOrganizationIds($where, $organizationIds, $columns = ['*'])
    {
        return AuthEmployee::query()->where($where)->whereIn('organization_id', $organizationIds)->get($columns)->toArray();
    }
    /**
     * 根据条件获取员工列表信息
     *
     * @Author czm
     * @DateTime 2020-10-21
     * @param [type] $map
     * @return void
     */
    public function getWithRselevance($params, $page, $pageSize, $columns = ['*'])
    {
        $map[] = [
            'company_id', "=", $params['company_id'],
        ];
        if (isset($params['name']) && $params['name'] !== "") {
            $map[] = [
                'name', "like", "%{$params['name']}%",
            ];
        }

        if (isset($params['phone']) && $params['phone'] !== "") {
            $map[] = [
                'phone', "=", $params['phone'],
            ];
        }

        if (isset($params['account']) && $params['account'] !== "") {
            $map[] = [
                'account', "=", $params['account'],
            ];
        }

        if (isset($params['job_number']) && $params['job_number'] !== "") {
            $map[] = [
                'job_number', "=", $params['job_number'],
            ];
        }

        if (isset($params['entry_time_start']) && $params['entry_time_start'] !== "") {
            $map[] = [
                'entry_time', ">=", $params['entry_time_start'],
            ];
        }

        if (isset($params['entry_time_end']) && $params['entry_time_end'] !== "") {
            $map[] = [
                'entry_time', "<=", $params['entry_time_end'],
            ];
        }
        $sql = AuthEmployee::where($map)
            ->with([
                'hasOrganization' => function ($query) {
                    $query->select('auth_organization.id', 'auth_organization.name', 'auth_organization.prefix_name');
                },
                'hasRole' => function ($query) {
                    $query->select('auth_role.id', 'auth_role.name');
                },
                'hasDuty' => function ($query) {
                    $query->select('auth_duty.id', 'auth_duty.name');
                },
                // 'belongsToProject' => function ($query) {
                //     $query->select('auth_project.id', 'auth_project.name', 'auth_project_employee.type')
                //         ->where('auth_project_employee.is_del', 1)
                //         ->where('auth_project.status', AuthEmployee::STATUS_NORMAL)
                //         ->where('auth_project_employee.organization_id', 0);
                // },
            ]);

        if (isset($params['organization_id']) && $params['organization_id'] !== "") {
            $sql = $sql->whereIn('organization_id', explode(",", $params['organization_id']));
        }

        if (isset($params['duty_id']) && $params['duty_id'] !== "") {
            $sql = $sql->whereIn('duty_id', explode(",", $params['duty_id']));

        }

        if (isset($params['role_id']) && $params['role_id'] !== "") {
            $sql = $sql->whereIn('role_id', explode(",", $params['role_id']));

        }

        if (isset($params['entry_status']) && $params['entry_status'] !== "") {
            $sql = $sql->whereIn('entry_status', explode(",", $params['entry_status']));

        }

        if (isset($params['admin_status']) && $params['admin_status'] !== "") {
            $sql = $sql->whereIn('admin_status', explode(",", $params['admin_status']));

        }

        if (isset($params['status']) && $params['status'] !== "") {
            $sql = $sql->whereIn('status', explode(",", $params['status']));
        }

        // if (isset($params['join_project_id']) && $params['join_project_id'] !== "") {
        //     $join_project_id = explode(",", $params['join_project_id']);
        //     $sql = $sql->whereHas(
        //         'HasManyProject', function ($query) use ($join_project_id) {
        //         $query
        //             ->where('auth_project_employee.type', AuthProjectEmployee::JOIN_TYPE)
        //             ->where('auth_project_employee.is_del', 1)
        //             ->where('auth_project_employee.organization_id', 0)
        //             ->whereIn('auth_project_employee.project_id', $join_project_id);
        //     }
        //     );
        //     if (in_array(-1, $join_project_id)) {
        //         $sql->OrWhere("partake_project", AuthEmployee::PARTAKE_PROJECT_ALL);
        //     }
        // }
        $count = $sql->count();
        $sql = $sql->orderBy('id', 'DESC');
        if ($pageSize == -1) {
            $sql = $sql->get();
        } else {
            $sql = $sql->paginate(intval($pageSize), $columns, 'page', intval($page));
        }

        $data = $sql->each(function ($item) use ($params) {
            $organization_name = "";
            $role_name = "";
            // $project_name = "";
            $duty_name = "";

            if ($item->hasOrganization) {
                $organization_name = substr($item->hasOrganization->prefix_name, 0, -1);
            }
            if ($item->hasRole) {
                $role_name = $item->hasRole->name;
            }
            if ($item->hasDuty) {
                $duty_name = $item->hasDuty->name;
            }
            // if ($item->belongsToProject) {
            //     $project_name = $item->partake_project == AuthEmployee::PARTAKE_PROJECT_ALL ? "全部" : implode("、", $item->belongsToProject->where('type', AuthProjectEmployee::JOIN_TYPE)->pluck('name')->toArray());
            //     $join_project_id = implode(",", $item->belongsToProject->where('type', AuthProjectEmployee::JOIN_TYPE)->pluck('id')->toArray());
            //     $show_project_id = implode(",", $item->belongsToProject->where('type', AuthProjectEmployee::SHOW_TYPE)->pluck('id')->toArray());
            // }

            $item->forceFill([
                'organization_name' => $organization_name,
                'role_name' => $role_name,
                // 'project_name' => $project_name,
                'duty_name' => $duty_name,
                // 'join_project_id' => $join_project_id ?? "",
                // 'show_project_id' => $show_project_id ?? "",
                'password' => ApiUtils::aesDePassword((string) $item->password),
            ])->makeHidden(['hasOrganization', 'hasRole', 'hasDuty']);

            if (isset($params['is_hide_password']) && $params['is_hide_password']) {
                $item->makeHidden(['password']);
            }

        })->toArray();
        return [$data, $count];
    }

    public function searchByWhereIn($where, $key, $whereIn, $columns = ['*'])
    {
        return AuthEmployee::query()->where($where)->whereIn($key, $whereIn)->get($columns)->toArray();

    }

    /**
     * 获取员工负责人信息
     *
     * @Author czm
     * @DateTime 2020-11-13
     * @param integer $employee_id
     * @return void
     */
    public function findPrincipal(int $employee_id, $organization_id = 0)
    {
        !$organization_id && $employee = AuthEmployee::find($employee_id);
        if (!$organization_id && !$employee) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '员工信息错误');
        }
        $organization_id = $organization_id ?: $employee->organization_id;
        $organization = AuthOrganization::find($organization_id);

        if (!$organization || !$organization->employee_id) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '部门信息错误');
        }

        $principal = $organization->hasEmployeeOne;
        if (!$principal) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '负责人不存在');
        }
        $principal = $principal->only(['id', 'name']);
        $principal['organization_id'] = $organization_id;
        if ($principal['id'] == $employee_id && $organization->pid != 0) {
            $principal = $this->findPrincipal($employee_id, $organization->pid);
        }
        // $principal->put('organization_id', $organization_id);
        return $principal;
    }

    // 根据用户id获取后台权限
    public function getPermissionById(int $user_id)
    {
        $admin_role_id = AuthEmployee::query()->find($user_id, ['admin_role_id'])->toArray();
        $permission_ids = AuthAdminRolePermission::query()
            ->where('role_id', '=', $admin_role_id['admin_role_id'])
            ->select(['permission_id'])
            ->get()->toArray();
        $permission_ids = array_column($permission_ids, 'permission_id');
        $permission_array = AuthPermission::query()
            ->whereIn('id', $permission_ids)
            ->select(['identity'])
            ->get()->toArray();
        return array_column($permission_array, 'identity');
    }

}
