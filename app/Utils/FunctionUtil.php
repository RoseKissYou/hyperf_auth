<?php
declare(strict_types=1);

namespace App\Utils;


/**
 * @Description 函数方法
 * @author wuxian
 * @since 2020-04-15
 */
class FunctionUtil
{
    //递归实现侧边栏(层级从属关系)
    public static function getTree($data, $pid = 0, $level = 1)
    {
        $list = [];
        foreach ($data as $k => $v) {
            if ($v['pid'] == $pid) {
                $v['level'] = $level;
                $v['son'] = static::getTree($data, $v['id'], $level + 1);
                $list[] = $v;
            }
        }
        return $list;
    }

    //页面按钮层级
    public static function webBut($data)
    {
        $list = [];
        foreach ($data as $k => $v) {
            if ($v['is_web'] == 1) {
                $v['son'] = [];
                $list[$v['id']] = $v;
                unset($data[$k]);
            }
        }
        if(!empty($data)){
            foreach ($data as $k => $v) {
                $list[$v['pid']]['son'][] = $v;
            } 
        }
        return \array_values($list);
    }
}