<?php

declare (strict_types = 1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Dao;

use App\Model\AuthProject;

class AuthProjectDao
{
    //添加数据
    public function add(array $data)
    {
        $data['add_time'] = time();
        return AuthProject::query()->insertGetId($data);

    }

    //通过条件搜索单一条
    public function searchFirst($where, $columns = ['*'])
    {
        return AuthProject::query()->where($where)->first($columns);

    }

    public function searchAll($where, $columns = ['*'])
    {
        return AuthProject::query()->where($where)->get($columns)->toArray();

    }

    //带条件的分页
    public function pageByWhere($where, $page, $pageSize, $columns = ['*'])
    {
        return AuthProject::query()->where($where)->paginate(intval($pageSize), $columns, 'page', intval($page))->toArray();

    }

    public function updateByWhere($where, $data)
    {
        return AuthProject::query()->where($where)->update($data);

    }

    public function getListPage($where = [], $page = 1, $pageSize = 15, $columns = ['*'])
    {
        return AuthProject::query()->where($where)->orderBy('id', 'desc')->paginate(intval($pageSize), $columns, 'page', intval($page))->toArray();

    }

    public function getByWhereIn($where, $whereIn, $columns = ['*'])
    {
        return AuthProject::query()->where($where)
            ->when($whereIn, function ($query) use ($whereIn) {
                $query->whereIn('id', $whereIn);
            })
            ->get($columns)->toArray();
    }

}
