<?php

declare (strict_types = 1);

namespace App\Dao;

use Hyperf\Utils\Context;
use App\Model\AuthAdminLog;

class AuthAdminLogDao
{
    use DaoTrait;
    public function __construct()
    {
        $this->driver = __CLASS__;
    }

    public function getListPage($where = [], $adminIds = [], $page = null, $pageSize = 15, $columns = ['admin_log.id', 'admin_log.admin_id', 'admin_log.operate', 'admin_log.add_time', 'admin.name', 'admin.remark'])
    {
        if (empty($adminIds)) {//没有指定管理员id
            $data = AuthAdminLog::query()->leftJoin('admin', 'admin_log.admin_id', '=', 'admin.id')
                ->where($where)->orderBy('admin_log.id', 'dosc')
                ->paginate(intval($pageSize), $columns, 'page', $page)
                ->toArray();
        }else{
            $data = AuthAdminLog::query()->leftJoin('admin', 'admin_log.admin_id', '=', 'admin.id')
                ->where($where)->whereIn('admin_log.admin_id', $adminIds)->orderBy('admin_log.id', 'dosc')
                ->paginate(intval($pageSize), $columns, 'page', $page)
                ->toArray();
        }
        return ['list'=>$data['data'],'total'=>$data['total']];
    }

    public function addLog(string $operate,int $adminId =0)
    {
        if(empty($adminId)){
            $user = Context::get('jwt_token');
            $adminId = $user['uid'] ?? 0;
        }
        return $this->insertGetIdTrait([
            'admin_id' => $adminId,
            'operate' => $operate,
            'add_time' => time(),
        ]);
    }
}