<?php

declare (strict_types = 1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Controller\Auth;

use App\Constants\ErrorCode;
use App\Controller\AbstractController;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Context;
use App\Validate\ModuleValidation;
use App\Service\Auth\ModuleService;
use Hyperf\Utils\Codec\Json;

class ModuleController extends AbstractController
{
    /**
     * @Inject
     * @var ModuleService
     */
    protected $service;

    public function addComponent()
    {
        $params = $this->request->all();
        make(ModuleValidation::class)->check($params, 'addComponent')->response();
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->addComponent($params);
        return $this->success($result['msg'],$result['data']);
        
    }

    public function editComponent()
    {
        $params = $this->request->all();
        make(ModuleValidation::class)->check($params, 'editComponent')->response();
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->editComponent($params);
        return $this->success($result['msg'],$result['data']);
        
    }

    public function delComponent()
    {
        $params = $this->request->all();
        make(ModuleValidation::class)->check($params, 'delComponent')->response();
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->delComponent($params);
        return $this->success($result['msg'],$result['data']);
        
    }

    public function componentInfo()
    {
        $params = $this->request->all();
        make(ModuleValidation::class)->check($params, 'delComponent')->response();
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->componentInfo($params);
        return $this->success($result['msg'],$result['data']);
        
    }

    public function EnableModule()
    {
        $params = $this->request->all();
        make(ModuleValidation::class)->check($params, 'EnableModule')->response();
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->EnableModule($params);
        return $this->success($result['msg'],$result['data']);
        
    }

    public function componentCate()
    {
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->componentCate($params);
        return $this->success($result['msg'],$result['data']);
        
    }

    public function componentConfigList()
    {
        $params['login_usr'] = Context::get('jwt_token');
        $params['pageSize'] = $this->request->input('pageSize', 15);
        $result = $this->service->componentConfigList($params);
        return $this->success($result['msg'],$result['data'],$result['totalItem']);

    }

    public function companyModuleList()
    {
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->companyModuleList($params);
        return $this->success($result['msg'],$result['data']);

    }

    public function moduleList()
    {
        $params['login_usr'] = Context::get('jwt_token');
        $params['module_id'] = $this->request->input('module_id', 0);
        $result = $this->service->moduleList($params);
        return $this->success($result['msg'],$result['data']);

    }

    public function topModuleList()
    {
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->topModuleList($params);
        return $this->success($result['msg'],$result['data']);

    }

    public function departAddComponent()
    {
        $params = $this->request->all();
        make(ModuleValidation::class)->check($params, 'departAddComponent')->response();
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->departAddComponent($params);
        return $this->success($result['msg'],$result['data']);

    }

    public function departHasComponent()
    {
        $params['org_id'] = $this->request->input('org_id');
        make(ModuleValidation::class)->check($params, 'departHasComponent')->response();
        $params['login_usr'] = Context::get('jwt_token');
        $result = $this->service->departHasComponent($params);
        return $this->success($result['msg'],$result['data']);

    }

    public function userHasModule()
    {
        $params = Context::get('jwt_token');
        $result = $this->service->userHasModule($params);
        return $this->success($result['msg'],$result['data']);

    }

   
    
}
