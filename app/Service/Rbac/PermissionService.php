<?php
declare(strict_types=1);

namespace App\Service\Rbac;

use App\Constants\ErrorCode;
use App\Dao\{AuthPermissionDao,AuthAdminLogDao};
use App\Exception\BusinessException;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;

class PermissionService
{
    /**
     * @Inject
     * @var AuthPermissionDao
     */
    protected $authPermissinDao;

    public function menu(int $user_id)
    {
        return $this->authPermissinDao->searchPerByWhereId($user_id);
    }

    public function addPermssion(array $data)
    {
        $where[] = ['identity', '=', $data['identity']];
        $info = $this->authPermissinDao->firstByWhereTrait($where);
        if ($info) throw new BusinessException(ErrorCode::ERR_BUESSUS, '权限标识已被占用');
        $data['add_time'] = time();
        Db::beginTransaction();
        try {
            $this->authPermissinDao->createTrait($data);
            make(AuthAdminLogDao::class)->addLog('添加了权限,信息为' . json_encode($data));
            Db::commit();
            return true;
        }catch (\Throwable $t) {
            Db::rollBack();
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '网络繁忙,请重试@#新增权限失败');
        }
    }

    public function editPermissinGet(int $id)
    {
        $obj = $this->authPermissinDao->findTrait($id);
        if ($obj) return $obj->toArray();
        throw new BusinessException(ErrorCode::ERR_BUESSUS, '权限不存在');
    }

    public function editPermissionPost(array $data)
    {
        $obj = $this->authPermissinDao->findTrait($data['id']);
        if (!$obj) throw new BusinessException(ErrorCode::ERR_BUESSUS, '权限不存在');
        $ori = json_encode($obj->toArray());
        $info = $this->authPermissinDao->firstByWhereTrait(['identity'=>$data['identity']]);
        if ($info && $info->id != $data['id']) {
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '权限标识已被占用');
        }
        Db::beginTransaction();
        try {
            $data['update_time']=time();
            $this->authPermissinDao->updateByWhereTrait(['id'=>$data['id']],$data);
            make(AuthAdminLogDao::class)->addLog('编辑了权限,信息由原值' . $ori. '修改成' .json_encode($data));
            Db::commit();
            return true;
        }catch (\Throwable $t) {
            Db::rollBack();
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '网络繁忙,请重试@#编辑权限失败');
        }
    }

    public function delPermission(int $id)
    {
        $ids = $all = [$id];
        while (($ids = $this->authPermissinDao->serchaByIds($ids))){
            $all = array_merge($all,$ids);
        }
        $arr = json_encode($this->authPermissinDao->serchaByIds($all));
        Db::beginTransaction();
        try {
            $this->authPermissinDao->delPermissionByIds($all);
            make(AuthAdminLogDao::class)->addLog('删除了权限' . $arr);
            Db::commit();
            return true;
        }catch (\Throwable $t) {
            Db::rollBack();
            throw new BusinessException(ErrorCode::ERR_BUESSUS, '网络繁忙,请重试@#删除权限失败');
        }
    }
}