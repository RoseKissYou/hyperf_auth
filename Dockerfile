FROM 192.168.8.35/hyperf/hyperf:v7.4

WORKDIR /opt/www

# Composer Cache
# COPY ./composer.* /opt/www/
# RUN composer install --no-dev --no-scripts

COPY . /opt/www
RUN composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/ \
    && composer install --no-dev -o \
    && php bin/hyperf.php


EXPOSE 9501 9503

ENTRYPOINT ["php", "/opt/www/bin/hyperf.php", "start"]
