<?php

declare (strict_types = 1);
namespace App\Model;

use Hyperf\Database\Model\Builder;
use Hyperf\DbConnection\Model\Model;

/**
 * 职位信息表
 *
 * @Author czm
 * @DateTime 2020-09-09
 */
class AuthDuty extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'auth_duty';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'company_id', 'grade', 'pid', 'add_time', 'update_time', 'is_del'];
    protected $guarded = ['id'];
    protected $hidden = ['is_del'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'add_time' => 'datetime:Y-m-d H:i:s',
        'update_time' => 'datetime:Y-m-d H:i:s',
    ];

    const CREATED_AT = 'add_time';
    const UPDATED_AT = 'update_time';
    protected $dateFormat = 'U';

    protected function boot(): void
    {
        $this->bootTraits();
        //model 软删除过滤 避免每次查询都添加is_del条件 去除条件添加withoutGlobalScope
        $this->addGlobalScope('is_del', function (Builder $builder) {
            $builder->where($this->table . '.is_del', 1);
        });
    }

    const TOP_DUTY_ID = 0; //最高部门限制
    const TOP_DUTY_NAME = "顶级岗位"; //初始等级
}
