<?php

declare (strict_types = 1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Dao;

use App\Model\AuthOrganization;
use App\Model\AuthProjectEmployee;
use App\Model\AuthProjectOrganization;

class AuthProjectOrganizationDao
{
    //添加数据
    public function add(array $data)
    {
        $data['add_time'] = time();
        return AuthProjectOrganization::query()->insertGetId($data);

    }

    //通过条件搜索单一条
    public function searchFirst($where, $columns = ['*'])
    {
        return AuthProjectOrganization::query()->where($where)->first($columns);

    }

    public function searchAll($where, $columns = ['*'])
    {
        return AuthProjectOrganization::query()->where($where)->get($columns)->toArray();

    }

    //带条件的分页
    public function pageByWhere($where, $page, $pageSize, $columns = ['*'])
    {
        return AuthProjectOrganization::query()->where($where)->paginate(intval($pageSize), $columns, 'page', intval($page))->toArray();

    }

    /**
     * 更新
     * @param $where
     * @param $data
     * @return int
     */
    public function updateByWhere($where, $data)
    {
        return AuthProjectOrganization::query()->where($where)->update($data);

    }

    /**
     * in删除
     * @param $where
     * @param $whereIn
     * @return int|mixed
     */
    public function delByWhereIn($where, $whereIn)
    {
        return AuthProjectOrganization::query()->where($where)->whereIn('organization_id', $whereIn)->delete();
    }

    /**
     * in部门查询
     * @param $where
     * @param $whereIn
     * @return int|mixed
     */
    public function getByWhereIn($where, $whereIn)
    {
        return AuthProjectOrganization::query()->where($where)->whereIn('organization_id', $whereIn)->get();
    }

    /**
     * 获取全部项目部门信息
     * @param $where
     * @param $projectIds
     * @param string[] $columns
     * @return array
     */
    public function searchProjectsOrganizationAll($where, $projectIds, $columns = ['*'])
    {
        return AuthProjectOrganization::query()
            ->leftJoin('auth_organization', 'auth_project_organization.organization_id', '=', 'auth_organization.id')
            ->where($where)
            ->whereIn('auth_project_organization.project_id', $projectIds)
            ->get($columns)
            ->toArray();

    }

    /**
     * 获取指定项目部门信息
     * @param $where
     * @param $projectIds
     * @param string[] $columns
     * @return array
     */
    public function searchProjectsOrganization($where, $columns = ['*'])
    {
        return AuthProjectOrganization::query()
            ->leftJoin('auth_organization', 'auth_project_organization.organization_id', '=', 'auth_organization.id')
            ->where($where)
            ->get($columns)
            ->toArray();

    }

    public function firstOrCreate($data, $map = [])
    {
        if (empty($map)) {
            return AuthProjectOrganization::query()->firstOrCreate($data);
        } else {
            return AuthProjectOrganization::query()->firstOrCreate($map, $data);
        }
    }

    /**
     * 根据部门id与项目id去除子部门的最高项目关联
     *
     * @Author czm
     * @DateTime 2020-10-16
     * @param [type] $organization_id
     * @param [type] $projec_ids
     * @return void
     */
    public function delChildProject($organization_id, $project_id, $path = [], $node = [])
    {
        static $organization_ids = [];
        static $path = [];
        static $node = [];

        $organization_ids[] = $organization_id;
        $organizations = AuthOrganization::select('id', 'pid', 'employee_id', 'company_id')->where('pid', $organization_id)->orderBy('sort')->get()->toArray();
        foreach ($organizations as $val) {
            //信息记录
            $fd = isset($path[$organization_id]['fd']) ? $path[$organization_id]['fd'] . "-" . $val['id'] : $organization_id . "-" . $val['id'];
            $path[$val['id']] = $val;
            $path[$val['id']]['fd'] = $fd;
            $info = AuthProjectOrganization::where([
                ['organization_id', '=', $val['id']],
                ['project_id', '=', $project_id],
            ])->first();

            if ($info) {
                //当前节点下记录途中路径
                $info->delete();
                $fd = explode("-", $fd);
                array_shift($fd);
                array_pop($fd);
                if (!empty($fd)) {
                    $node = array_merge($node, $fd);
                }
            }
            $this->delChildProject($val['id'], $project_id, $path, $node);

        }
        // var_dump($organization_ids[0]);
        if ($organization_id == $organization_ids[0]) {
            $this->BindPrincipal($path, $node, $project_id);
            $organization_ids = [];
            $path = [];
            $node = [];
        }
        return true;
    }

    /**
     * 删除负责人时候进行链路之间负责人绑定
     *
     * @Author czm
     * @DateTime 2020-10-22
     * @param array $path
     * @param array $node
     * @param [type] $project_id
     * @return void
     */
    public function BindPrincipal(array $path, array $node, $project_id)
    {
        // var_dump($path, $node, $project_id);
        foreach ($path as $key => $val) {
            if (in_array($key, $node)) {
                //链路上负责人绑定
                $val['employee_id'] && make(AuthProjectEmployeeDao::class)->firstOrCreate([
                    'employee_id' => $val['employee_id'],
                    'project_id' => $project_id,
                    'company_id' => $val['company_id'],
                    'type' => AuthProjectEmployee::JOIN_TYPE,
                    'organization_id' => $key,
                    'add_time' => time(),
                    'update_time' => time(),
                    'is_del' => 1,
                ], [
                    'employee_id' => $val['employee_id'],
                    'project_id' => $project_id,
                    'company_id' => $val['company_id'],
                    'type' => AuthProjectEmployee::JOIN_TYPE,
                    'organization_id' => $key,
                    'is_del' => 1,
                ]);
            }
        }
    }
}
