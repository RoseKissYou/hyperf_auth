<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

namespace App\Controller;

use Hyperf\DbConnection\Db;
use Hyperf\Nats\Driver\DriverFactory;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\Redis\Redis;

class HealthController extends AbstractController
{
    public function health()
    {
        $res = '';
        try {
            Db::table('auth_admin')->count();
        } catch (\Throwable $t) {
            $res = $res.$t->getMessage();
        }

        try {
            $this->container->get(Redis::class)->set('health', '11');
        } catch (\Throwable $t) {
            $res = $res.'redis no connect '.$t->getMessage();
        }

        if ($res) {
            return $this->response->withStatus(500)->withBody(new SwooleStream($res));
        } else {
            return 'ok';
        }
    }
}
