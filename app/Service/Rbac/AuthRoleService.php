<?php

declare (strict_types = 1);

namespace App\Service\Rbac;

use App\Constants\ErrorCode;
use App\Dao\AuthRoleDao;
use App\Utils\ApiUtils;
use Hyperf\Utils\Context;

class AuthRoleService
{
    // 单独获取角色和名称列表
    public function rpcRole($params): array
    {
        $data = make(AuthRoleDao::class)->getByWhereTrait(['company_id'=>$params['company_id']], ['id', 'name'])->toArray();
        return ApiUtils::send(ErrorCode::SUCCESS, '操作成功', $data, count($data));
    }
}
