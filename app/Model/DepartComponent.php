<?php

declare(strict_types=1);

namespace App\Model;

use Hyperf\DbConnection\Model\Model;

class DepartComponent extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'auth_depart_component';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['org_id','component_id','company_id'];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    public $timestamps = false;

    public function components()
    {
        return $this->hasMany(Component::class, 'id', 'component_id');
    }

   
}
