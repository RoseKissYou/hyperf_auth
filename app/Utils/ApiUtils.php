<?php
declare(strict_types=1);

namespace App\Utils;

/**
 * @Description 对接前端对应的方法封装类
 * @author wuxian
 * @since 2020-04-15
 */
class ApiUtils
{
    /**
     * @param integer $code  返回前端的code码
     * @param string $msg   返回给前端的提示语
     * @param [type] $data  返回给前端的数据
     * @param integer $totalItem  分页的总页数
     * @return array
     * @Description  返回客户端的数据格式
     * @author wuxian
     * @since 2020-04-16
     */
    public static function send(int $code = 0, string $msg = '', $data = [], int $totalItem = 1): array
    {
        return ['msg' => $msg, 'errCode' => $code, 'data' => $data, 'totalItem'=> $totalItem];
    }

    /**
     * @param [type] $password  要加密的密码
     * @return string
     * @Description 加密用户密码
     * @author wuxian
     * @since 2020-04-16
     */
    public static function encryption($password): string
    {
        return md5(config('web.slot') . $password);
    }

    /**
     * @param [type] $password 明文密码
     * @param [type] $encr_pwd  加密后的密码
     * @return string
     * @Description  检查用户密码是否正确
     * @author wuxian
     * @since 2020-04-16
     */
    public static function decryption($password, $encr_pwd): bool
    {
        return self::encryption($password) == $encr_pwd;
    }

    /** AES加密
     * @param string $password
     * @return string
     */
    public static function aesEnPassword(string $password) : string
    {
        $data = openssl_encrypt($password, 'AES-128-ECB', config('web.slot'), OPENSSL_RAW_DATA);
        return base64_encode($data);
    }

    /** AES解密
     * @param string $password
     * @return string
     */
    public static function aesDePassword(string $password) : string
    {
        $decrypted = openssl_decrypt(base64_decode($password), 'AES-128-ECB', config('web.slot'), OPENSSL_RAW_DATA);
        return $decrypted;
    }
}
